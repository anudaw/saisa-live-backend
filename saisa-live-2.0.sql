-- MySQL dump 10.13  Distrib 5.7.21, for osx10.9 (x86_64)
--
-- Host: localhost    Database: saisa_live
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accesscodes`
--

DROP TABLE IF EXISTS `accesscodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accesscodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(100) NOT NULL,
  `access_code` varchar(10) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accesscodes`
--

LOCK TABLES `accesscodes` WRITE;
/*!40000 ALTER TABLE `accesscodes` DISABLE KEYS */;
INSERT INTO `accesscodes` VALUES (1,'OSC3','lololololo',1),(2,'OSC3','geckos',1),(3,'test','testing123',1),(4,'lol','lol',1),(5,'test','test',1);
/*!40000 ALTER TABLE `accesscodes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `tournament_id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES (1,'aweerasinghe20@osc.lk','7HDhnssem8eUZi#%pLzPDKuCOrhFKm',0,1,1,'2020-02-08 15:05:59'),(2,'stturner@osc.lk','D0unu*zBvL*$y^FmhEc2GemO$t2^Pl',5,1,0,'2020-02-18 13:27:26'),(3,'boysBasketballJordan2020','VlLS1wDDB2#PU4yUz8RBAS8mb5MUvY',4,1,0,'2020-02-18 18:34:36'),(4,'trackChennai2020','aeZP6&Al9zX9axReJ^cC4XHSI$6TTa',6,1,0,'2020-02-18 18:40:21'),(5,'admin','admin',0,1,1,'2020-05-11 07:53:53'),(6,'test','test',1,1,1,'2020-05-11 08:24:39'),(7,'lol','lol',4,5,1,'2020-05-11 08:49:02'),(8,'ibAdmin','Admin123',0,1,1,'2020-05-27 08:15:07');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `followers`
--

DROP TABLE IF EXISTS `followers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `followers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `followers`
--

LOCK TABLES `followers` WRITE;
/*!40000 ALTER TABLE `followers` DISABLE KEYS */;
INSERT INTO `followers` VALUES (1,1,1),(2,1,7),(3,2,1),(4,3,1),(5,3,3),(6,3,8),(7,4,1),(8,5,1),(9,5,8);
/*!40000 ALTER TABLE `followers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `games`
--

DROP TABLE IF EXISTS `games`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `games` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team1` int(11) NOT NULL,
  `team2` int(11) NOT NULL,
  `team1_score` varchar(100) DEFAULT '0',
  `team2_score` varchar(100) DEFAULT '0',
  `result` int(11) DEFAULT '0',
  `active_status` int(11) NOT NULL,
  `livestream_id` int(11) DEFAULT '0',
  `tournament_id` int(11) NOT NULL,
  `location` varchar(100) NOT NULL,
  `game_description` varchar(100) NOT NULL,
  `start_time` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `games`
--

LOCK TABLES `games` WRITE;
/*!40000 ALTER TABLE `games` DISABLE KEYS */;
INSERT INTO `games` VALUES (1,7,8,'6, 15','25, 25',2,2,2,1,'Court A, The Overseas School of Colombo','Round Robin','1540436400'),(2,6,5,'25, 25','19, 17',1,2,3,1,'Court B, The Overseas School of Colombo','Round Robin','1540436400'),(3,2,4,'19, 25,  8','25, 10,  15',2,2,2,1,'Court A, The Overseas School of Colombo','Round Robin','1540440600'),(4,3,1,'25, 25','22, 17',1,2,3,1,'Court B, The Overseas School of Colombo','Round Robin','1540440600'),(5,6,8,'22, 25','25, 27',2,2,2,1,'Court A, The Overseas School of Colombo','Round Robin','1540444800'),(6,5,9,'25, 25','11, 11',1,2,3,1,'Court B, The Overseas School of Colombo','Round Robin','1540444800'),(7,1,2,'25, 25','17, 16',1,2,2,1,'Court A, The Overseas School of Colombo','Round Robin','1540449000'),(8,4,7,'25, 25','12, 17',1,2,3,1,'Court B, The Overseas School of Colombo','Round Robin','1540449000'),(9,3,8,'25, 25','10, 16',1,2,2,1,'Court A, The Overseas School of Colombo','Round Robin','1540453200'),(10,9,6,'19, 16','25, 25',2,2,3,1,'Court B, The Overseas School of Colombo','Round Robin','1540453200'),(11,7,5,'12, 23','25, 25',2,2,2,1,'Court A, The Overseas School of Colombo','Round Robin','1540457400'),(12,1,4,'25, 22,  17','22, 25,  19',2,2,3,1,'Court B, The Overseas School of Colombo','Round Robin','1540457400'),(13,9,3,'10, 24','25, 26',2,2,2,1,'Court A, The Overseas School of Colombo','Round Robin','1540461600'),(14,8,2,'20, 20','25, 25',2,2,3,1,'Court B, The Overseas School of Colombo','Round Robin','1540461600'),(15,6,7,'25, 25','11, 17',1,2,2,1,'Court A, The Overseas School of Colombo','Round Robin','1540465800'),(16,1,5,'19, 25,  15','25, 23,  11',1,2,3,1,'Court B, The Overseas School of Colombo','Round Robin','1540465800'),(17,4,3,'25, 11,  15','19, 25,  9',1,2,2,1,'Court A, The Overseas School of Colombo','Round Robin','1540470000'),(18,2,9,'25, 25','12, 21',1,2,3,1,'Court B, The Overseas School of Colombo','Round Robin','1540470000'),(19,2,5,'25, 23,  13','23, 25,  15',2,2,4,1,'Court A, The Overseas School of Colombo','Round Robin','1540521000'),(20,7,9,'25, 25','9, 12',1,2,5,1,'Court B, The Overseas School of Colombo','Round Robin','1540521000'),(21,6,4,'25, 25','21, 14',1,2,4,1,'Court A, The Overseas School of Colombo','Round Robin','1540525200'),(22,8,1,'25, 25,','18, 21',1,2,5,1,'Court B, The Overseas School of Colombo','Round Robin','1540525200'),(23,5,3,'26, 21,  15','24, 25,  11',1,2,4,1,'Court A, The Overseas School of Colombo','Round Robin','1540529400'),(24,7,2,'21, 25,  15','25, 22,  10',1,2,5,1,'Court B, The Overseas School of Colombo','Round Robin','1540529400'),(25,9,1,'6, 16','25, 25,  0',2,2,4,1,'Court A, The Overseas School of Colombo','Round Robin','1540533600'),(26,4,8,'23, 31,  15','25, 29,  7',1,2,5,1,'Court B, The Overseas School of Colombo','Round Robin','1540533600'),(27,7,3,'23, 26,  13','25, 24,  15',2,2,4,1,'Court A, The Overseas School of Colombo','Round Robin','1540537800'),(28,2,6,'25, 25','20, 19',1,2,5,1,'Court B, The Overseas School of Colombo','Round Robin','1540537800'),(29,4,9,'25, 26','13, 24',1,2,4,1,'Court A, The Overseas School of Colombo','Round Robin','1540542000'),(30,8,5,'13, 16','25, 25',2,2,5,1,'Court B, The Overseas School of Colombo','Round Robin','1540542000'),(31,1,6,'25, 25','23, 17',1,2,4,1,'Court A, The Overseas School of Colombo','Round Robin','1540546200'),(32,2,3,'27, 25','25, 21',1,2,5,1,'Court B, The Overseas School of Colombo','Round Robin','1540546200'),(33,4,5,'26, 25','24, 17',1,2,4,1,'Court A, The Overseas School of Colombo','Round Robin','1540550400'),(34,9,8,'14, 17','25, 25',2,2,5,1,'Court B, The Overseas School of Colombo','Round Robin','1540550400'),(35,7,1,'18, 24','25, 26',2,2,4,1,'Court A, The Overseas School of Colombo','Round Robin','1540554600'),(36,6,3,'23, 22','25, 25',2,2,5,1,'Court B, The Overseas School of Colombo','Round Robin','1540554600'),(37,7,9,'25, 25','6, 15',1,2,6,1,'Court A, The Overseas School of Colombo','Playoffs','1540607400'),(38,3,6,'25, 25','20, 11',1,2,7,1,'Court B, The Overseas School of Colombo','Playoffs','1540607400'),(39,5,8,'25, 25','18, 18',1,2,6,1,'Court A, The Overseas School of Colombo','Playoffs','1540611600'),(40,1,2,'25, 19,  16','19, 25,  18',2,2,7,1,'Court B, The Overseas School of Colombo','Playoffs','1540611600'),(41,4,7,'25, 25','23, 18',1,2,6,1,'Court A, The Overseas School of Colombo','Playoffs','1540615800'),(42,3,5,'25, 21,  15','19, 25,  5',1,2,6,1,'Court A, The Overseas School of Colombo','Semi Final 1','1540620000'),(43,6,8,'25, 25','15, 15',1,2,7,1,'Court B, The Overseas School of Colombo','Playoffs','1540620000'),(44,1,7,'23, 15','25, 25',2,2,6,1,'Court A, The Overseas School of Colombo','Playoffs','1540624200'),(45,2,4,'21, 12','25, 25',2,2,7,1,'Court B, The Overseas School of Colombo','Semi Final 2','1540624200'),(46,8,1,'17, 25,  15','25, 18,  6',1,2,6,1,'Court A, The Overseas School of Colombo','7th Place Game','1540628400'),(47,6,7,'25, 23,  15','17, 25,  13',1,2,7,1,'Court B, The Overseas School of Colombo','5th Place Game','1540628400'),(48,5,2,'21, 18,  21','25, 25,  25',2,2,6,1,'Court A, The Overseas School of Colombo','3rd Place Game','1540632600'),(49,3,4,'21, 25, 25, 25','25, 9, 18, 17',1,2,6,1,'Court A, The Overseas School of Colombo','Championship Game','1540639800'),(50,28,29,'28','49',2,2,34,4,'American Community School, Amman','Pool A - Round Robin','1582182000'),(51,30,31,'44','14',1,2,34,4,'American Community School, Amman','Pool A - Round Robin','1582186800'),(52,32,33,'15','40',2,2,34,4,'American Community School, Amman','Pool B - Round Robin','1582191600'),(53,34,35,'44','34',1,2,34,4,'American Community School, Amman','Pool B - Round Robin','1582196400'),(54,28,30,'52','59',2,2,34,4,'American Community School, Amman','Pool A - Round Robin','1582201200'),(55,29,31,'38','18',1,2,34,4,'American Community School, Amman','Pool A - Round Robin','1582206000'),(56,32,34,'32','44',2,2,34,4,'American Community School, Amman','Pool B - Round Robin','1582214400'),(57,33,35,'54','43',1,2,34,4,'American Community School, Amman','Pool B - Round Robin','1582219200'),(58,28,31,'38','15',1,2,38,4,'American Community School, Amman','Pool A - Round Robin','1582264800'),(59,29,30,'54','28',1,2,38,4,'American Community School, Amman','Pool A - Round Robin','1582269600'),(60,32,35,'43','45',2,2,38,4,'American Community School, Amman','Pool B - Round Robin','1582274400'),(61,33,34,'66','51',1,2,38,4,'American Community School, Amman','Pool B - Round Robin','1582279200'),(62,42,41,'1','0',1,2,17,5,'Field 1, The American International School in Muscat, Oman','Pool B - Round Robin','1582176600'),(63,40,43,'2','4',2,2,18,5,'Field 2, The American International School in Muscat, Oman','Pool B - Round Robin','1582176600'),(64,39,36,'0','1',2,2,19,5,'Field 1, The American International School in Muscat, Oman','Pool A - Round Robin','1582183800'),(65,38,37,'0','1',2,2,20,5,'Field 2, The American International School in Muscat, Oman','Pool A - Round Robin','1582183800'),(66,40,41,'0','5',2,2,21,5,'Field 1, The American International School in Muscat, Oman','Pool B - Round Robin','1582191000'),(67,42,43,'1','1',0,2,22,5,'Field 2, The American International School in Muscat, Oman','Pool B - Round Robin','1582191000'),(68,38,36,'1','0',1,2,23,5,'Field 1, The American International School in Muscat, Oman','Pool A - Round Robin','1582198200'),(69,39,37,'1','0',1,2,24,5,'Field 2, The American International School in Muscat, Oman','Pool A - Round Robin','1582198200'),(70,36,37,'0','0',0,2,25,5,'Field 1, The American International School in Muscat, Oman','Pool A - Round Robin','1582263000'),(71,38,39,'0','3',2,2,26,5,'Field 2, The American International School in Muscat, Oman','Pool A - Round Robin','1582263000'),(72,42,40,'3','0',1,2,27,5,'Field 1, The American International School in Muscat, Oman','Pool B - Round Robin','1582270200'),(73,43,41,'1','2',2,2,28,5,'Field 2, The American International School in Muscat, Oman','Pool B - Round Robin','1582270200'),(74,44,45,'','',0,3,29,6,'AISC','1500 Meter Finals','1582162200'),(75,46,47,'','',0,3,30,6,'AISC','100 Meter Prelims','1582170600'),(76,48,49,'','',0,3,31,6,'AISC Field','Long Jump, High Jump, Shot Put, Discus','1582182600'),(77,45,50,'','',0,3,29,6,'AISC Field','100 Meter Finals','1582188600'),(78,45,46,'','',0,3,30,6,'AISC','Long Jump, High Jump, Shot Put, Discus','1582192800'),(79,45,48,'','',0,3,31,6,'AISC','4 X 400 Meter Finals','1582198200'),(80,45,44,'','',0,3,35,6,'AISC Field','800 Meter Finals','1582250400'),(81,46,47,'','',0,3,36,6,'AISC Track','200 Meter Prelims','1582254600'),(82,48,49,'','',0,3,37,6,'AISC Field','Long Jump, High Jump, Shot Put, Discus','1582261200'),(83,50,45,'','',0,3,35,6,'AISC Field','Long Jump, High Jump, Shot Put, Discus','1582272000'),(84,45,44,'','',0,3,36,6,'AISC Track','200 Meter Finals','1582279200'),(85,36,43,'0','1',2,2,39,5,'TAISM','Quarter Final 1','1582277400'),(86,41,37,'3','0',1,2,40,5,'taism','Quarter Final 2','1582277400'),(87,39,40,'3','1',1,2,41,5,'taism','Quarter Final 3','1582284600'),(88,42,38,'2','1',1,2,42,5,'taism','Quarter Final 4','1582284600'),(89,30,35,'64','54',1,2,38,4,'ACS','Game 1 -  Quarter Finals','1582284000'),(90,37,40,'2','0',1,2,43,5,'taism','Loser Game 14 vs Loser Game 15','1582349400'),(91,36,38,'2','3',2,2,44,5,'taism','Loser Game 13 vs Loser Game 16','1582349400'),(92,41,39,'1','0',1,2,45,5,'taism','Semi Final 1','1582354800'),(93,43,42,'0','2',2,2,46,5,'taism','Semi Final 2','1582354800'),(94,34,28,'','',0,3,38,4,'acs','Game 2 - Quarter Finals','1582378800'),(95,29,32,'','',0,3,38,4,'acs','Game 3 - Quarter Finals','1582383600'),(96,33,31,'43','12',1,2,38,4,'acs','Game 4 - Quarter Finals','1582302000'),(97,45,44,'','',0,3,47,6,'AISC Track','3000 Mts Final','1582335000'),(98,46,47,'','',0,3,47,6,'AISC Track','400 Mts Timed Finals','1582341000'),(99,48,49,'','',0,3,48,6,'Track','Long Jump, High Jump, Shot Put, Discus','1582347600'),(100,50,45,'','',0,3,49,6,'AISC Field','Long Jump, High Jump, Shot Put, Discus','1582358400'),(101,44,45,'','',0,3,47,6,'AISC Track','4x100 Mts Relay','1582367400'),(102,34,28,'46','31',1,2,38,4,'ACS','Game 2 -  Quarter Finals','1582293600'),(103,29,32,'39','19',1,2,38,4,'ACS','Game 3 - Quarter Finals','1582298400'),(104,35,32,'','',0,3,50,4,'acs','Loser QF1 vs Loser QF 3','1582351200'),(105,28,32,'40','30',1,2,50,4,'ACS','Loser QF2 vs Loser QF3','1582356000'),(106,29,34,'57','44',1,2,50,4,'acs','Semi Final 1','1582360800'),(107,30,33,'51','42',1,2,50,4,'acs','Semi Final 2','1582365600'),(108,35,31,'35','28',1,2,50,4,'Court 1','Loser QF1 vs Loser QF 4','1582351200'),(109,40,36,'FORFEIT','',2,2,51,5,'taism','7th Place Playoff Match','1582360200'),(110,37,38,'0','1',2,2,52,5,'taism','5th Place Playoff Match','1582360200'),(111,31,32,'8','30',2,2,50,4,'acs','7th Place Playoff Match','1582370400'),(112,35,28,'21','35',2,2,50,4,'acs','5th Place Playoff Match','1582375200'),(113,39,43,'3 (1/4)','3 (3/5)',2,2,53,5,'taism','3rd Place Playoff Match','1582365600'),(114,41,42,'2','3',2,2,54,5,'taism','Championship Match','1582371000'),(115,34,33,'46','44',1,2,50,4,'acs','3rd Place Playoff Match','1582380000'),(116,29,30,'60','41',1,2,50,4,'acs','Championship Match','1582384800');
/*!40000 ALTER TABLE `games` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `livestreams`
--

DROP TABLE IF EXISTS `livestreams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `livestreams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tournament_id` int(11) NOT NULL,
  `url` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `is_live` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `livestreams`
--

LOCK TABLES `livestreams` WRITE;
/*!40000 ALTER TABLE `livestreams` DISABLE KEYS */;
INSERT INTO `livestreams` VALUES (1,1,'https://youtu.be/sUwu2P2FLFY','Opening Ceremony',1,0),(2,1,'https://youtu.be/aSujN1fKouk','Day 1 - Court A',1,0),(3,1,'https://youtu.be/Es-wbYo-q2k','Day 1 - Court B',1,0),(4,1,'https://youtu.be/p1F2hp39y_Y','Day 2 - Court A',1,0),(5,1,'https://youtu.be/l89RfVieB8g','Day 2 - Court B',1,0),(6,1,'https://youtu.be/stqd2A3sw_A','Day 3 - Court A',1,0),(7,1,'https://youtu.be/1viuMBDm33A','Day 3 - Court B',1,0),(8,1,'https://youtu.be/-M54CN9hlXE','Closing Ceremony',1,0),(17,5,'https://youtu.be/_UMIajYPqEM','Game 1 - TAISM vs OSC',1,0),(18,5,'https://youtu.be/0e0NicAVP7A','Game 2 - KAS vs ACS',0,0),(19,5,'https://youtu.be/sB_H3wEgI4Y','Game 3 - AISD vs LS',1,0),(20,5,'https://youtu.be/UhZu4_9TGbE','Game 4 - AISC vs ASB',1,0),(21,5,'https://youtu.be/ForVCTvvItE','Game 5 - KAS vs OSC',1,0),(22,5,'https://youtu.be/FNc7Rz5cpDw','Game 6 - TAISM vs ACS',1,0),(23,5,'https://youtu.be/Z6ywtGiC3KU','Game 7 - AISC vs LS',1,0),(24,5,'https://youtu.be/9daPISqcYq0','Game 8 - AISD vs ASB',1,0),(25,5,'https://youtu.be/Ip9tuep6qAM','Game 9 - LS vs ASB',1,0),(26,5,'https://youtu.be/0dGH4IjPy6k','Game 10 - AISC vs AISD',1,0),(27,5,'https://youtu.be/HVqFtilMJ8U','Game 11 - TAISM vs KAS',1,0),(28,5,'https://youtu.be/PyZJvOhePUM','Game 12 - ACS vs OSC',1,0),(29,6,'https://youtu.be/iPHuZ_2FdRw','Camera 1 - Day 1',1,0),(30,6,'https://youtu.be/3H3IZRa1uZY','Camera 2 - Day 1',1,0),(31,6,'https://youtu.be/B-ErPVEVTHI','Camera 3 - Day 1',1,0),(32,6,'','',0,0),(33,6,'','',0,0),(34,4,'https://youtu.be/D5giwc4SNNk','Day 1',1,0),(35,6,'https://youtu.be/3Jj5nLuuTpc','Camera 1 - Day  2',1,0),(36,6,'https://youtu.be/EfKJT9aRwqo','Camera 2 - Day 2',1,0),(37,6,'https://youtu.be/T7cHLcMlxnY','Camera 3 - Day 2',1,0),(38,4,'https://youtu.be/6kgSscM9w48','Day 2',1,0),(39,5,'https://youtu.be/of48RF_yUtE','Game 13 - LS vs ACS',1,0),(40,5,'https://youtu.be/JsnUjQ6dy2Q','Game 14 - OSC vs ASB',1,0),(41,5,'https://youtu.be/_nkjHI8HlVs','Game 15 - AISD vs KAS',1,0),(42,5,'https://youtu.be/4GhoMVlUmmQ','Game 16 - TAISM vs AISC',1,0),(43,5,'https://youtu.be/O3kLF6-3Z0w','Game 17 - ASB vs KAS',1,0),(44,5,'https://youtu.be/rWDEgBGbG9M','Game 18 - LS vs AISC',1,0),(45,5,'https://youtu.be/lJViE7r2QJw','Semi Final 1 - OSC vs AISD',1,0),(46,5,'https://youtu.be/sWlJbbTQwMI','Semi Final 2 - ACS vs TAISM',1,0),(47,6,'https://youtu.be/qPDG8gzHcPo','Camera 1- Day 3',1,0),(48,6,'https://youtu.be/jXsjPLSKADQ','Camera 2 - Day 3',1,0),(49,6,'https://youtu.be/vJ5B2XR9Nl4','Camera 3 - Day 3',1,0),(50,4,'https://youtu.be/SmzOSUM6-zo','Day 3',1,0),(51,5,'https://youtu.be/w2jklRlbeIM','7th Place Playoff - KAS vs LS',0,0),(52,5,'https://youtu.be/4CFg1jDv_fY','5th Place Playoff - ASB vs AISC',1,0),(53,5,'https://youtu.be/-q6Pfdg16lI','3rd Place Playoff Match - AISD vs ACS',1,0),(54,5,'https://youtu.be/NnoOyvjNEds','Championship Match - OSC vs TAISM',1,0),(55,4,'https://youtu.be/j66iuPNB4Jg','Closing Ceremony',1,0);
/*!40000 ALTER TABLE `livestreams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tournament_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `cover_img` varchar(200) NOT NULL,
  `content_url` varchar(200) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL,
  `timestamp` varchar(200) NOT NULL,
  `text` varchar(10000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media`
--

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
INSERT INTO `media` VALUES (1,6,2,'SAISA Track and Field 2020 - Opening Ceremony','https://www.aischennai.org/wp-content/uploads/2020/02/SAISA-Track-Field-Banner-01.jpg','https://www.youtube.com/watch?v=oyVJFvs-9Ko',1,'1582177880',NULL),(2,1,1,'SAISA Girls Volleyball 2018','https://res.cloudinary.com/dr4uncu8z/image/upload/v1542877113/SAISA%20Girls%20Volleyball%20Website/49-min.jpg','https://drive.google.com/drive/folders/1pVTX2Uon1vNrS6m1FmuQuZSMsFuEfbZh?usp=sharing',0,'1582186475',NULL),(3,4,1,'Opening Ceremony - SAISA Boys Basketball 2020','https://resources.finalsite.net/images/f_auto,q_auto,t_image_size_4/v1582265859/acsammanedujo/fw5j6gpnneuux326y8jz/SAISABoysBB32.jpg','https://www.acsamman.edu.jo/student-life/athletics/welcome-to-saisa/saisa-boys-basketball-2020#photos',1,'1582282443',NULL),(4,5,1,'Day 1 - SAISA Girls Football 2020','http://142.93.212.170/covers/unnamed.jpg','https://saisa.taism.com/2019-20-saisa-girls-soccer/photos-girls-soccer/photo-girls-soccer-day-1',1,'1582293752',NULL),(5,5,1,'Day 2 - SAISA Girls Football 2020','http://142.93.212.170/covers/unnamed%20(1).jpg','https://saisa.taism.com/2019-20-saisa-girls-soccer/photos-girls-soccer/photo-girls-soccer-day-2',1,'1582293805',NULL),(6,5,3,'AISD, OSC, ACS and TAISM Qualify for Girls Football Semi Finals','http://142.93.212.170/covers/unnamed%20(1).jpg',NULL,1,'1589356062','After some thrilling games in the pool stages, and the quarter finals on Thursday and Friday, The American International School in Dhaka, The Overseas School of Colombo, The American Community School of Amman and The American International School of Muscat have qualified for the Semi Finals of SAISA Girls Football 2020 set to take place on Saturday. The fixtures for these semi finals are as follows:\n\n\nSemi Final 1 - 11:00AM GST, Saturday 22nd of February 2020 - OSC vs AISD\n\nSemi Final 2 - 11:00AM GST, Saturday 22nd of February 2020 - ACS vs TAISM\n\n3rd Place Game - 02:00PM GST, Saturday 22nd of February 2020 - Loser of Semi Final 1 vs Loser of Semi Final 2\n\nChampionship Game - 03:30PM GST, Saturday 22nd of February 2020 - Winner of Semi Final 1 vs Winer of Semi Final 2\n\n\nKeep up with live scores, live streams and media from SAISA Girls Football 2020 hosted in Muscat, Oman via the SAISA Live App!\n\n*GST = +04:00 GMT'),(7,4,3,'AISD, AISC, ACS and TAISM Qualify for Boys Basketball Semi Finals','https://resources.finalsite.net/images/f_auto,q_auto,t_image_size_4/v1582265845/acsammanedujo/lalo8i73wkcpgdtga4ha/SAISABoysBB21.jpg',NULL,1,'1582306921','After some thrilling games in the pool stages, and the quarter finals on Thursday and Friday, The American International School in Dhaka, The American International School in Chennai, The American Community School of Amman and The American International School of Muscat have qualified for the Semi Finals of SAISA Boys Basketball 2020 set to take place on Saturday. The fixtures for these semi finals are as follows:\n\n\nSemi Final 1 - 10:40AM EET, Saturday 22nd of February 2020 - AISD vs ACS\n\nSemi Final 2 - 12:00PM EET, Saturday 22nd of February 2020 - TAISM vs AISC\n\n3rd Place Game - 04:00PM EET, Saturday 22nd of February 2020 - Loser of Semi Final 1 vs Loser of Semi Final 2\n\nChampionship Game - 05:20PM EET, Saturday 22nd of February 2020 - Winner of Semi Final 1 vs Winer of Semi Final 2\n\n\nKeep up with live scores, live streams and media from SAISA Boys Basketball 2020 hosted in Amman, Jordan via the SAISA Live App!\n\n*EET = +02:00 GMT'),(8,4,1,'SAISA Boys Basketball 2020 Photos Drive','https://drive.google.com/uc?id=1jum3T00ehTjpf8ECqyLk6g4HdcD8V5Kh','https://drive.google.com/drive/folders/1uWiURo7ELab2OaQ_G9R3C3xCIbN5Pd2I?usp=sharing',1,'1582356428',NULL),(9,5,1,'Day 3 - SAISA Girls Football 2020','http://142.93.212.170/covers/unnamed%20(2).jpg','https://saisa.taism.com/2019-20-saisa-girls-soccer/photos-girls-soccer/photo-girls-soccer-day-3',1,'1582369434',NULL),(10,5,3,'Home team TAISM comes out on top at SAISA Girls Football 2020','http://142.93.212.170/covers/cover.png',NULL,1,'1582377517','Despite taking a 2-0 lead early on in the first half, the home team came back strong in the end to defeat OSC 3-2 and take the SAISA Girls Football championship for 2020. Here are the final standings from the tournament.\n\n1. TAISM\n2. OSC\n3. ACS\n4. AISD\n5. AISC\n6. ASB\n7. LS\n8. KAS\n\nThank you for using the SAISA Live App to stay updated with SAISA Girls Football 2020.');
/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meets`
--

DROP TABLE IF EXISTS `meets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tournament_id` int(11) NOT NULL,
  `description` varchar(500) NOT NULL,
  `start_time` varchar(100) NOT NULL,
  `active_status` int(11) NOT NULL,
  `livestream_id` int(11) NOT NULL,
  `result_url` varchar(500) DEFAULT NULL,
  `p1team_id` int(11) DEFAULT NULL,
  `p2team_id` int(11) DEFAULT NULL,
  `p3team_id` int(11) DEFAULT NULL,
  `p1name` varchar(200) DEFAULT NULL,
  `p2name` varchar(200) DEFAULT NULL,
  `p3name` varchar(200) DEFAULT NULL,
  `p1result` varchar(100) DEFAULT NULL,
  `p2result` varchar(100) DEFAULT NULL,
  `p3result` varchar(100) DEFAULT NULL,
  `p1record` tinyint(4) DEFAULT NULL,
  `p2record` tinyint(4) DEFAULT NULL,
  `p3record` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meets`
--

LOCK TABLES `meets` WRITE;
/*!40000 ALTER TABLE `meets` DISABLE KEYS */;
INSERT INTO `meets` VALUES (1,6,'Girls (10-12) 1500 Meter Finals','1582162200',2,29,'https://www.aischennai.org/events/wp-content/uploads/2020/02/girls-10-12-1500mts.pdf',49,47,50,'Edelman, Liesl','Driscoll, Layla','Leibel, Rhys','5:41:22','5:58:39','5:58:39',0,0,0),(2,6,'Boys (10-12) 1500 Meter Finals','1582162200',2,29,'https://www.aischennai.org/events/wp-content/uploads/2020/02/boys-10-12-1500mts.pdf',50,45,50,'Makri, Ayoub','Faivre D\'Arcier, Gaspard','Kelly, Ryan','5:19:55','5:20:25','5:22:27',0,0,0),(3,6,'Girls (13-14) 1500 Meter Finals','1582162200',2,29,'https://www.aischennai.org/events/wp-content/uploads/2020/02/girls-13-14-1500mts.pdf#',49,49,47,'Lamade, Nayara','Palliyaguru, Lara','Masso Aiyar, Aanya','5:47:21','5:50:74','5:52:07',0,0,0),(4,6,'Boys (13-14) 1500 Meter Finals','1582162200',2,29,'https://www.aischennai.org/events/wp-content/uploads/2020/02/boys-13-14-1500mts.pdf',46,49,50,'Mbele, Nate','Bastin, Louie','Akomolede, Ola','4:39:45','4:40:69','5:10:44',0,0,0),(5,6,'Girls (15-19) 1500 Meter Finals','1582162200',2,29,'https://www.aischennai.org/events/wp-content/uploads/2020/02/girls-15-19-1500mts.pdf',50,49,45,'Moreno, Sofia','Wiberg, Lexi','Bridgeland, Chloe','5:38:73','5:47:59','5:52:35',0,0,0),(6,6,'Boys (15-19) 1500 Meter Finals','1582162200',2,29,'https://www.aischennai.org/events/wp-content/uploads/2020/02/boys-15-19-1500mts.pdf',44,44,49,'Bazbaz, Noorreddin','Allers, Christian ','Sinha, Tanay','4:51:61','5:01:04','5:09:87',0,0,0),(7,6,'Boys (13-14) Long Jump','1582182600',2,31,'https://www.aischennai.org/wp-content/uploads/2020/02/long-jump-boys13-14.pdf',49,45,45,'Goodland, George','Kaschura, Neel','Lee, Sihyeon','5.15 m','5.11 m','4.96 m',0,0,0),(8,6,'Boys (15-19) High Jump','1582182600',2,31,'https://www.aischennai.org/events/wp-content/uploads/2020/02/high-jump-boys-15-19.pdf',46,50,46,'Russell, Thitigarn','Al Musafir, Waleed','Bah, Abdulai','1.62m','1.59m','1.59m',0,0,0),(9,6,'Girls (10-12) Shot Put','1582182600',2,31,'https://www.aischennai.org/events/wp-content/uploads/2020/02/shot-put-girls-10-12.pdf',50,50,45,'Rauk, Marin','Leibel, Rhys','Penker, Shania','7.35 m','7.21 m','6.83 m',0,0,0),(10,6,'Girls (15-19) Discus','1582182600',2,31,'https://www.aischennai.org/events/wp-content/uploads/2020/02/discus-girls-15-19.pdf',50,49,50,'McWilliams, Kaylin','Shea, Talia','Moreno, Sofia','21.20 m','18.52 m','18.25 m',0,0,0),(11,6,'Girls (10-12) 100 Meter Finals','1582188600',2,29,'https://www.aischennai.org/events/wp-content/uploads/2020/02/100mts-girls-10-12.pdf',50,50,49,'Renjit, Hannah','Rauk, Marin','Watte, Charlotte','14:06','14:53','14:79',0,0,0),(12,6,'Boys  (10-12) 100 Meter Finals','1582188600',2,29,'https://www.aischennai.org/events/wp-content/uploads/2020/02/100mts-boys-10-12.pdf',44,47,46,'Henley, Ayden','Obasi, Toby','Khan, Junain','13:37','13:44','13:97',0,0,0),(13,6,'Girls  (13-14) 100 Meter Finals','1582188600',2,29,'https://www.aischennai.org/events/wp-content/uploads/2020/02/100mts-girls-13-14.pdf',47,46,50,'Obasi, Chinenve','Frankenberger, Melanie','Fugazzi, Taylor','13:04','13:33','13:90',0,0,0),(14,6,'Boys (13-14) 100 Meter Finals','1582188600',2,29,'https://www.aischennai.org/events/wp-content/uploads/2020/02/100mts-boys-13-14.pdf',49,50,50,'Jayasinghe, Praneeth','Macki, Aymen','Shabeer, Ashfaaq','12:56','12:68','12:73',0,0,0),(15,6,'Girls (15-19) 100 Meter Finals','1582188600',2,29,'https://www.aischennai.org/events/wp-content/uploads/2020/02/100mts-girls-15-19.pdf',50,46,46,'Al Mughairi, Aliva','Mir, Bushra','Bah, Fatmata','13:29','13:45','13:88',0,0,0),(16,6,'Boys (15-19) 100 Meter Finals','1582188600',2,29,'https://www.aischennai.org/events/wp-content/uploads/2020/02/100mts-boys-15-19.pdf',44,49,50,'Erickson, Jonathan','VanHorn, Jack','Martin Robles, Jorge','11:79','12:05','12:14',0,0,0),(17,6,'Girls (15-19) Long Jump','1582192800',2,30,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event-17-longjump.pdf',50,47,49,'Al Mughairi, Aliva','Deora, Naisha','Shea, Talia','5.20 m','4.19 m','4.11 m',1,0,0),(18,6,'Girls (13-14) High Jump','1582192800',2,30,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event18-highjump-girls-13-14.pdf',46,50,49,'Frankenberger, Melanie','Fugazzi, Taylor','Palliyaguru, Lara','1.58 m','1.36 m','1.33 m',1,0,0),(19,6,'Boys (10-12) Shot Put','1582192800',2,30,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event-19-shotput.pdf',49,46,48,'Guene, Mussa','Lee, Joonho','Shubash, Salem','12.09 m','8.99 m','8.61 m',1,0,0),(20,6,'Boys (13-14) Discus','1582192800',2,30,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event-20-discus.pdf',50,50,49,'Leewarner, Asher','Shabeer, Ashfaaq','Grandbois, Ethan','33.00 m','30.02 m','28.33 m',0,0,0),(21,6,'Girls (10-12) 4x400m Finals','1582198200',2,31,'https://www.aischennai.org/events/wp-content/uploads/2020/02/4x4-girls-10-12.pdf',45,48,46,'AISC','LS','AISD','5:34:19','5:41:00','5:43:12',0,0,0),(22,6,'Boys (10-12) 4x400m Finals','1582198200',2,31,'https://www.aischennai.org/events/wp-content/uploads/2020/02/4x4-boys-10-12.pdf',45,46,48,'AISC','AISD','LS','4:57:60','5:04:84','5:06:54',0,0,0),(23,6,'Girls (13-14) 4x400m Finals','1582198200',2,31,'https://www.aischennai.org/events/wp-content/uploads/2020/02/4x4-girls-13-14.pdf',49,45,47,'OSC','AISC','ASB','5:54.33','5:05.38','5:07.28',0,0,0),(24,6,'Boys (13-14) 4x400m Finals','1582198200',2,31,'https://www.aischennai.org/events/wp-content/uploads/2020/02/4x4-boys-13-14.pdf',49,46,50,'OSC','AISD','TAISM','4:14.02','4:18.20','4:21.48',0,0,0),(25,6,'Girls (15-19) 4x400m Finals','1582198200',2,31,'https://www.aischennai.org/events/wp-content/uploads/2020/02/4x4-girls-15-19.pdf',46,49,50,'AISD','OSC','TAISM','4:48.07','5:04.02','5:07.11',0,0,0),(26,6,'Boys (15-19) 4x400m Finals','1582198200',2,31,'https://www.aischennai.org/events/wp-content/uploads/2020/02/4x4-boys-15-19.pdf',44,46,49,'ACS','AISD','OSC','4:03.28','4:06.11','4:11.20',0,0,0),(27,6,'Girls 10-12 800m Finals','1582250400',2,35,'https://www.aischennai.org/events/wp-content/uploads/2020/02/800-Girls-10-12.pdf',47,49,50,'Driscoll, Layla','Edelman, Liesl','Leibel, Rhys','02:43.7','02:47.6','02:47.6',0,0,0),(28,6,'Boys 10-12 800m Finals','1582250400',2,35,'https://www.aischennai.org/events/wp-content/uploads/2020/02/800-boys-10-12.pdf',49,50,45,'Guene, Moussa','Kelly, Ryan','Favre D\'Arcier, Gaspard','02:28.4','02:40.4','02:41.3',0,0,0),(29,6,'Girls 13-14 800m Finals','1582250400',2,35,'https://www.aischennai.org/events/wp-content/uploads/2020/02/800-Girls-13-14.pdf',47,49,44,'Masso Aiyar, Aanya','Lamade, Nayara','Grube, Morgan','02:48.3','02:48.8','02:50.4',0,0,0),(30,6,'Boys 13-14 800m Finals','1582250400',2,35,'https://www.aischennai.org/events/wp-content/uploads/2020/02/800-boys-13-14.pdf',46,49,49,'Mbele, Nate','Bastin, Louie','Wigishoff, Valentin','02:15.9','02:19.2','02:20.3',0,0,0),(31,6,'Girls 15-19 800m Finals','1582250400',2,35,'https://www.aischennai.org/events/wp-content/uploads/2020/02/800-Girls-15-19.pdf',50,49,49,'Morena, Sofia','Wiberg, Lexi','Parr, Alexandra','02:41.1','02:47.7','02:49.7',0,0,0),(32,6,'Boys 15-19 800m Finals','1582250400',2,35,'https://www.aischennai.org/events/wp-content/uploads/2020/02/800-boys-15-19.pdf',44,44,44,'Bazbaz, Noorreddin','Boyd, Jeremy','Allers, Christian','00:02:22','02:23.7','02:24.5',0,0,0),(33,6,'Girls 10-12 Long Jump','1582261200',2,37,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event39-longjump-Girls-10-12.pdf',50,50,46,'Renjit, Hannah','Rauk, Marin','Stanislawska, Samia','4.16m','3.97m','3.92m',0,0,0),(34,6,'Boys 10-12 High Jump','1582261200',2,37,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event40-high-jump-boys-10-12.pdf',49,46,44,'Guene, Moussa','Goldberg, Yuta','Henley, Ayden','1.47m','1.40m','1.31m',0,0,0),(35,6,'Boys 15-19 Shot Put','1582261200',2,37,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event41-shot-put-boys-15-19.pdf',45,44,49,'Penker, Shahrukh','Flamant yotti, Nicholas','VanHorn, Jack','12.43m','11.29m','10.94m',0,0,0),(36,6,'Girls 13-14 Discus','1582261200',2,37,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event42-discus-Girls-13-14.pdf',50,49,45,'Moreno, Samara','Turner, Stephie','Penker, Shandaneh','21.32m','19.82m','19.66m',0,0,0),(37,6,'Girls 13-14 Long Jump','1582272000',2,35,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event43-Girls-longjump-13-14.pdf',46,47,45,'Frankenberger, Melanie','Obasi, Chinenye','Penker, Shandaneh','4.43m','4.41m','4.11m',0,0,0),(38,6,'Boys 13-14 High Jump','1582272000',2,35,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event44-boys-highjump-13-14.pdf',46,50,50,'Quintela, Luca','Aokomolede, Ola','Esho, Tobi','1.44m','1.44m','1.41m',0,0,0),(39,6,'Girls 15-19 Shot Put','1582272000',2,35,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event45-Girls-shotput-15-19.pdf',49,50,46,'Parr, Alexandra','McWilliams, Kaylin','Mir, Amara','7.87m','7.35m','6.87m',0,0,0),(40,6,'Boys 10-12 Discus','1582272000',2,35,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event46-boys-discus-10-12.pdf',49,46,47,'Guene, Moussa','Khan, Junain','Obasi, Toby','30.99m','18.91m','18.29m',1,0,0),(41,6,'Girls 10-12 200m Finals','1582279200',2,36,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event33-Girls-200-10-12.pdf',50,50,48,'Renjit, Hannah','Rauk, Marin','Madhav, Apurvaa','29.97','30.9','31.22',0,0,0),(42,6,'Boys 10-12 200m Finals','1582279200',2,36,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event34-boys-200-10-12.pdf',44,47,50,'Henley, Ayden','Obasi, Toby','Akomolede, Deolu','28.91','28.97','29.66',0,0,0),(43,6,'Girls 13-14 200m Finals','1582279200',2,36,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event35-Girls-200-13-14.pdf',47,46,50,'Obasi, Chinenye','Frankenberger, Melanie','Fugazzi, Taylor','28','28.3','30.5',0,0,0),(44,6,'Boys 13-14 200m Finals','1582279200',2,36,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event36-boys-200-13-14.pdf',49,50,50,'Jayasinghe, Praneeth','Macki, Aymen','Shabeer, Ashfaaq','25.9','26.1','26.55',0,0,0),(45,6,'Girls 15-19 200m Finals','1582279200',2,36,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event37-Girls-200-15-19.pdf',50,46,46,'Al Mughari, Aliya','Mir, Bushra','Mir, Amara','28.33','28.74','29.43',0,0,0),(46,6,'Boys 15-19 200m Finals','1582279200',2,36,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event38-boys-200-15-19.pdf',44,46,45,'Erickson, Jonathan','Bah, Abdulai','Lakhchine, Isa','24.8','25.21','25.29',0,0,0),(47,6,'Girls 10-12 3000m Finals','1582335000',2,47,'https://www.aischennai.org/events/wp-content/uploads/2020/02/3000-Girls-10-12.pdf',47,49,50,'Driscoll, Layla','Edelman, Liesl','Bharati, Aayushi','12:03.5','12:28.7','12:51.9',0,0,0),(48,6,'Girls 13-14 3000m Finals','1582335000',2,47,'https://www.aischennai.org/events/wp-content/uploads/2020/02/3000-Girls-13-14.pdf',44,49,47,'Grube, Morgan','Lamade, Nayara','Masso Aiyar, Aanya','12:41.4','12:52.3','13:08.8',1,1,1),(49,6,'Girls 15-19 3000m Finals','1582335000',2,47,'https://www.aischennai.org/events/wp-content/uploads/2020/02/3000-Girls-15-19.pdf',50,49,45,'Morena, Sofia','Pilapitiya, Sheruni','Bridgeland, Chloe','12:16.1','13:16.0','13:25.9',0,0,0),(50,6,'Boys 10-12 3000m Finals','1582335000',2,47,'https://www.aischennai.org/events/wp-content/uploads/2020/02/3000-boys-10-12.pdf',44,45,50,'Sharif, Farah','Favre D\'Arcier, Gaspard','Makri, Ayoub','13:44.0','12:05.0','13:46.8',1,1,1),(51,6,'Boys 13-14 3000m Finals','1582335000',2,47,'https://www.aischennai.org/events/wp-content/uploads/2020/02/3000-boys-13-14.pdf',49,50,50,'Bastin, Louie','Aokomolede, Ola','Makri, Jalil','10:49.3','11:07.0','11:30.5',0,0,0),(52,6,'Boys 15-19 3000m Finals','1582335000',2,47,'https://www.aischennai.org/events/wp-content/uploads/2020/02/3000-boys-15-19.pdf',44,44,48,'Bazbaz, Noorreddin','Allers, Christian','Pradhan, Shashwat','10:47.9','10:58.1','11:21.9',0,0,0),(53,6,'Girls 10-12 400m Finals','1582341000',2,47,'https://www.aischennai.org/events/wp-content/uploads/2020/02/400mts-Girls-10-12.pdf',50,49,47,'Renjit, Hannah','Watte, Charlotte','Besseling, Isabella','01:09.6','01:10.9','01:13.5',0,0,0),(54,6,'Boys 10-12 400m Finals','1582341000',2,47,'https://www.aischennai.org/events/wp-content/uploads/2020/02/400mts-boys-10-12.pdf',49,44,48,'Guene, Moussa','Henley, Ayden','Khadka, Biraj','01:01.5','01:04.9','01:08.1',0,0,0),(55,6,'Girls 13-14 400m Finals','1582341000',2,47,'https://www.aischennai.org/events/wp-content/uploads/2020/02/400mts-Girls-13-14.pdf',47,46,49,'Obasi, Chinenye','Frankenberger, Melanie','Lamade, Nayara','01:00.9','01:01.3','01:09.0',1,1,0),(56,6,'Boys 13-14 400m Finals','1582341000',2,47,'https://www.aischennai.org/events/wp-content/uploads/2020/02/400mts-boys-13-14.pdf',46,49,49,'Mbele, Nate','Jayasinghe, Praneeth','Wigishoff, Valentin','00:58.0','00:59.8','01:00.0',0,0,0),(57,6,'Girls 15-19 400m Finals','1582341000',2,47,'https://www.aischennai.org/events/wp-content/uploads/2020/02/400mts-Girlss-15-19.pdf',46,46,50,'Mir, Amara','Mir, Bushra','Al Mughariri, Aliya','01:06.8','01:07.1','01:07.2',0,0,0),(58,6,'Boys 15-19 400m Finals','1582341000',2,47,'https://www.aischennai.org/events/wp-content/uploads/2020/02/400mts-boys-15-19.pdf',46,44,46,'Bah, Abdulai','Flamant yotti, Nicholas','Inoue, Tomofumi','00:55.5','00:55.6','00:58.4',0,0,0),(59,6,'Boys 10-12 Long Jump','1582347600',2,48,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event59-longjump-boys.pdf',45,45,46,'Suzuki, Minato','Eto, Yuya','Lee, Joonho','4.28m','4.12m','4.00m',0,0,0),(60,6,'Girls 10-12 High Jump','1582347600',2,48,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event60-Girls-highjump-10-12.pdf',47,49,47,'Baumer, Linn','Watte, Charlotte','Hays, Alana','1.30m','1.25m','1.20m',0,0,0),(61,6,'Girls 13-14 Shot Put','1582347600',2,48,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event61-Girls-shot-put.pdf',48,47,50,'Shreshta, Ashwini','Driscoll, Brianna','Moreno, Samara','8.54m','8.41m','8.09m',0,0,0),(62,6,'Boys 15-19 Discus','1582347600',2,48,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event62-boys-shot-discus.pdf',49,45,47,'Vanhorn, Jack','Penker, Shahrukh','Dumbell, Toby','32.80m','32.17m','24.30m',0,0,0),(63,6,'Girls 15-19 Long Jump','1582358400',2,49,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event-63-boys-15-19-longjump.pdf',46,50,44,'Russell, Thitigarn','Martin Robles, Jorge','Flamant Yotti, Nicholas','5.76m','5.58m','5.53m',0,0,0),(64,6,'Boys 15-19 High Jump','1582358400',2,49,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event-64-Girls-15-19-HIGHjump.pdf',50,45,49,'Al Mughari, Aliya','Bridgeland, Chloe','Wiberg, Lexi','1.56m','1.33m','1.30m',1,0,0),(65,6,'Boys 13-14 Shot Put','1582358400',2,49,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event-65-boys-13-14-shotput.pdf',50,49,50,'Shabeer, Ashfaaq','Grandbois, Ethan','Leewarner, Asher','10.76m','10.41m','9.87m',0,0,0),(66,6,'Girls 10-12 Discus','1582358400',2,49,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event-66-boys-10-12-discus.pdf',50,45,45,'Leibel, Rhys','Penker, Shania','Dowling, Summer','15.97m','15.74m','15.14m',0,0,0),(67,6,'Girls 10-12 4x100m Relay','1582367400',2,47,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event67-Girls-4x100-10-12.pdf',50,47,49,'TAISM','ASB','OSC','00:58.4','01:00.8','01:01.8',0,0,0),(68,6,'Boys 10-12 4x100m Relay','1582367400',2,47,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event68-boys-4x100-10-12.pdf',49,45,50,'OSC','AISC','TAISM','00:57.4','00:59.5','00:59.6',0,0,0),(69,6,'Girls 13-14 4x100m Relay','1582367400',2,47,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event69-Girls-4x100-13-14.pdf',49,45,50,'OSC','AISC','TAISM','00:57.5','00:58.5','00:58.5',0,0,0),(70,6,'Boys 13-14 4x100m Relay','1582367400',2,47,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event70-boys-4x100-13-14.pdf',50,49,46,'TAISM','OSC','AISD','00:50.3','00:52.0','00:52.2',0,0,0),(71,6,'Girls 15-19 4x100m Relay','1582367400',2,47,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event71-Girls-4x100-15-19.pdf',46,47,50,'AISD','ASB','TAISM','00:53.8','00:56.7','00:58.1',0,0,0),(72,6,'Boys 15-19 4x100m Relay','1582367400',2,47,'https://www.aischennai.org/events/wp-content/uploads/2020/02/event72-boys-4x100-15-19.pdf',49,50,44,'OSC','TAISM','ACS','00:50.0','00:50.5','00:50.7',0,0,0);
/*!40000 ALTER TABLE `meets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participants`
--

DROP TABLE IF EXISTS `participants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) NOT NULL,
  `tournament_id` int(11) NOT NULL,
  `standing` int(11) NOT NULL,
  `wins` int(11) NOT NULL DEFAULT '0',
  `losses` int(11) NOT NULL DEFAULT '0',
  `ties` int(11) NOT NULL DEFAULT '0',
  `games` int(11) NOT NULL DEFAULT '0',
  `points` double NOT NULL DEFAULT '0',
  `pool` int(11) NOT NULL,
  `team_photo` varchar(300) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participants`
--

LOCK TABLES `participants` WRITE;
/*!40000 ALTER TABLE `participants` DISABLE KEYS */;
INSERT INTO `participants` VALUES (1,1,1,4,5,3,0,8,5,1,'http://osc.lk/saisa/assets/teams/OSC.jpg',1),(2,2,1,5,4,4,0,8,4,1,'http://osc.lk/saisa/assets/teams/AISD.jpg',1),(3,3,1,2,5,3,0,8,5,1,'http://osc.lk/saisa/assets/teams/ASB.jpg',1),(4,8,1,1,7,1,0,8,7,1,'http://osc.lk/saisa/assets/teams/ACS.jpg',1),(5,10,1,3,5,3,0,8,5,1,'http://osc.lk/saisa/assets/teams/KAS.jpg',1),(6,7,1,7,4,4,0,8,4,1,'http://osc.lk/saisa/assets/teams/LS.jpg',1),(7,11,1,8,2,6,0,8,2,1,'http://osc.lk/saisa/assets/teams/TAISM.jpg',1),(8,4,1,6,4,4,0,8,4,1,'http://osc.lk/saisa/assets/teams/AISC.jpg',1),(9,5,1,9,0,8,0,8,0,1,'http://osc.lk/saisa/assets/teams/ISOI.jpg',1),(10,3,2,3,7,2,0,9,7,1,'na',1),(11,4,2,7,3,6,0,9,3,1,'na',1),(12,11,2,2,8,1,0,9,8,1,'na',1),(13,2,2,1,8,1,0,9,8,1,'na',1),(14,10,2,5,5,4,0,9,5,1,'na',1),(15,7,2,9,1,8,0,9,1,1,'na',1),(16,1,2,4,6,3,0,9,6,1,'na',1),(17,8,2,8,2,7,0,9,2,1,'na',1),(18,5,2,6,4,5,0,9,4,1,'na',1),(19,6,2,10,1,8,0,9,1,1,'na',1),(20,11,3,1,0,0,0,0,1992,1,'taism',1),(21,8,3,2,0,0,0,0,1857,1,'acs',1),(22,1,3,3,0,0,0,0,1291,1,'na',1),(23,7,3,4,0,0,0,0,1265,1,'na',1),(24,3,3,5,0,0,0,0,916,1,'asb',1),(25,10,3,6,0,0,0,0,753,1,'na',1),(26,4,3,7,0,0,0,0,656,1,'na',1),(27,2,3,8,0,0,0,0,439,1,'na',1),(28,3,4,3,1,2,0,3,1,1,'https://drive.google.com/file/d/15nJY7chOyX1nIAD7mubs4UNejtiX6QHG/view',1),(29,2,4,1,3,0,0,3,3,1,'https://drive.google.com/file/d/15nJY7chOyX1nIAD7mubs4UNejtiX6QHG/view',1),(30,11,4,2,2,1,0,3,2,1,'https://drive.google.com/file/d/15nJY7chOyX1nIAD7mubs4UNejtiX6QHG/view',1),(31,9,4,4,0,3,0,3,0,1,'https://drive.google.com/file/d/15nJY7chOyX1nIAD7mubs4UNejtiX6QHG/view',1),(32,7,4,4,0,3,0,3,0,2,'https://drive.google.com/file/d/15nJY7chOyX1nIAD7mubs4UNejtiX6QHG/view',1),(33,4,4,1,3,0,0,3,3,2,'https://drive.google.com/file/d/15nJY7chOyX1nIAD7mubs4UNejtiX6QHG/view',1),(34,8,4,2,2,1,0,3,2,2,'https://drive.google.com/file/d/15nJY7chOyX1nIAD7mubs4UNejtiX6QHG/view',1),(35,1,4,3,1,2,0,3,1,2,'https://drive.google.com/file/d/15nJY7chOyX1nIAD7mubs4UNejtiX6QHG/view',1),(36,7,5,2,1,1,1,3,4,1,'na',1),(37,3,5,3,1,1,1,3,4,1,'na',1),(38,4,5,4,1,2,0,3,3,1,'na',1),(39,2,5,1,2,1,0,3,6,1,'na',1),(40,10,5,4,0,3,0,3,0,2,'na',1),(41,1,5,2,2,1,0,3,6,2,'na',1),(42,11,5,1,2,0,1,3,7,2,'na',1),(43,8,5,3,1,1,1,3,4,2,'na',1),(44,8,6,5,0,0,0,0,262.5,0,'na',1),(45,4,6,4,0,0,0,0,288.83,0,'na',1),(46,2,6,3,0,0,0,0,396.33,0,'na',1),(47,3,6,6,0,0,0,0,262,0,'na',1),(48,7,6,7,0,0,0,0,62,0,'na',1),(49,1,6,2,0,0,0,0,595.83,0,'na',1),(50,11,6,1,0,0,0,0,596.5,0,'na',1);
/*!40000 ALTER TABLE `participants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sports`
--

DROP TABLE IF EXISTS `sports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `is_indoor` tinyint(4) NOT NULL,
  `icon` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sports`
--

LOCK TABLES `sports` WRITE;
/*!40000 ALTER TABLE `sports` DISABLE KEYS */;
INSERT INTO `sports` VALUES (1,'Boys Volleyball',1,'http://127.0.0.1/sports/volleyball'),(2,'Girls Volleyball',1,'http://127.0.0.1/sports/volleyball'),(3,'Swimming',0,'http://127.0.0.1/sports/volleyball'),(4,'Boys Basketball',0,'http://127.0.0.1/sports/volleyball'),(5,'Girls Football',0,'http://127.0.0.1/sports/volleyball'),(6,'Track and Field',0,'http://127.0.0.1/sports/volleyball'),(7,'Girls Basketball',1,'http://127.0.0.1/sports/volleyball'),(8,'Boys Football',0,'http://127.0.0.1/sports/volleyball'),(9,'Badminton',1,'http://127.0.0.1/sports/volleyball');
/*!40000 ALTER TABLE `sports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `full_name` varchar(150) NOT NULL,
  `mascot` varchar(45) NOT NULL,
  `web_url` varchar(300) NOT NULL,
  `logo` varchar(300) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams`
--

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
INSERT INTO `teams` VALUES (1,'OSC','The Overseas School of Colombo','Geckos','https://osc.lk','http://142.93.212.170/logos/osc.png',1),(2,'AISD','The American International School of Dhaka','Tigers','https://aisdhaka.org','http://142.93.212.170/logos/aisd.png',1),(3,'ASB','The American School of Bombay','Eagles','https://ASB.org','http://142.93.212.170/logos/asb.png',1),(4,'AISC','The American International School of Chennai','Raptors','https://AISC.org','http://142.93.212.170/logos/aisc.png',1),(5,'ISOI','The International School of Islamabad','Cobras','https://ISOI.org','http://142.93.212.170/logos/isoi.png',1),(6,'LAS','The Lahore American School','Bulls','https://LAS.org','http://142.93.212.170/logos/las.png',1),(7,'LS','The Lincoln School, Kathmandu','Snow Leopards','https://LS.org','http://142.93.212.170/logos/ls.png',1),(8,'ACS','The American Community School, Jordan','Scorpions','https://ACS.org','http://142.93.212.170/logos/acs.png',1),(9,'SAISA','SAISA United Team','Saisa','https://saisaleague.com/','http://142.93.212.170/logos/saisa.png',1),(10,'KAS','The Karachi American School, Pakistan','Knights','http://www.kas.edu.pk/index.html','http://142.93.212.170/logos/kas.png',1),(11,'TAISM','The American International School in Muscat, Oman','Eagles','https://www.taism.com/','http://142.93.212.170/logos/taism.png',1);
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tournaments`
--

DROP TABLE IF EXISTS `tournaments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tournaments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `sport_id` int(11) NOT NULL,
  `url` varchar(350) NOT NULL,
  `logo` varchar(200) NOT NULL,
  `location` varchar(200) NOT NULL,
  `start_date` varchar(45) NOT NULL,
  `end_date` varchar(45) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `standings_active` tinyint(4) NOT NULL,
  `pools_active` tinyint(4) NOT NULL,
  `scores_active` tinyint(4) NOT NULL,
  `pool_quantity` int(11) NOT NULL,
  `meets_active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tournaments`
--

LOCK TABLES `tournaments` WRITE;
/*!40000 ALTER TABLE `tournaments` DISABLE KEYS */;
INSERT INTO `tournaments` VALUES (1,'SAISA Girls Volleyball 2018',2,'http://osc.lk/saisa/#!/home','http://osc.lk/saisa/assets/images/logo-img.png','Colombo, Sri Lanka','1540425600','1540598400',0,1,1,1,1,0),(2,'SAISA Boys Volleyball 2019',1,'https://saisa.taism.com/2019-20-saisa-boys-volleyball','https://drive.google.com/uc?id=1ncxPpoQuAV7MUDR9-s0Gkq9t0AtE0f6Q','Muscat, Oman','1571283000','1571484600',0,1,1,1,1,0),(3,'SAISA Swimming 2019',3,'https://www.lsnepal.com/saisa-swim-meet-2019/','https://drive.google.com/uc?id=1_FCKxWFBwwjffoRAwx_77EvjfN6wOsAF','Kathmandu, Nepal','1571270400','1571443200',0,1,1,0,1,0),(4,'SAISA Boys Basketball 2020',4,'https://www.acsamman.edu.jo/student-life/athletics/welcome-to-saisa/saisa-boys-basketball-2020','https://resources.finalsite.net/images/f_auto,q_auto/v1579685342/acsammanedujo/caz55azvn5a4g3oj3ck8/SAISABoysBasketballlogo-edit11.jpg','Amman, Jordan','1582156800','1582329600',0,1,1,1,2,0),(5,'SAISA Girls Football 2020',5,'https://saisa.taism.com/2019-20-saisa-girls-soccer','https://drive.google.com/thumbnail?id=1bdrUK4eyXXz-CTbsZ9orXVlPBnBkUl-j','Muscat, Oman','1582156800','1582329600',0,1,1,1,2,0),(6,'SAISA Track and Field 2020',6,'http://www.aischennai.org/events/','https://drive.google.com/thumbnail?id=1jc8bHAuc653Q-wPAowKYZM5s3NsyhKVI','Chennai, India','1582156800','1582329600',0,1,0,0,0,1);
/*!40000 ALTER TABLE `tournaments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device` varchar(100) NOT NULL,
  `first_login_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'iOS3','2020-05-17 09:03:32'),(2,'iOS3','2020-05-17 10:14:12'),(3,'iOS3','2020-05-17 10:47:02'),(4,'iOS3','2020-05-17 15:41:04'),(5,'iOS3','2020-05-18 11:50:56');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-11 18:27:23
