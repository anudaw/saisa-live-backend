package anuda.me.saisa_live.responses;

import anuda.me.saisa_live.entities.Teams;

public class ParticipantResponse {
    private int id;
    private Teams team;                 //the team object
    private int standing;               //The participant's standing in the tournament
    private int wins;                   //Number of games won
    private int losses;                 //Number of games lost
    private int ties;                   //Number of ties
    private int games;                  //Number of games played
    private double points;              //Number of points acquired in the tournament
    private int pool;                   //The participant's pool/group
    private String teamPhoto;           //Location of participant's team photo
    private boolean isActive;           //Is the participant active or deactivated

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Teams getTeam() {
        return team;
    }

    public void setTeam(Teams team) {
        this.team = team;
    }

    public int getStanding() {
        return standing;
    }

    public void setStanding(int standing) {
        this.standing = standing;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getLosses() {
        return losses;
    }

    public void setLosses(int losses) {
        this.losses = losses;
    }

    public int getTies() {
        return ties;
    }

    public void setTies(int ties) {
        this.ties = ties;
    }

    public int getGames() {
        return games;
    }

    public void setGames(int games) {
        this.games = games;
    }

    public double getPoints() {
        return points;
    }

    public void setPoints(double points) {
        this.points = points;
    }

    public int getPool() {
        return pool;
    }

    public void setPool(int pool) {
        this.pool = pool;
    }

    public String getTeamPhoto() {
        return teamPhoto;
    }

    public void setTeamPhoto(String teamPhoto) {
        this.teamPhoto = teamPhoto;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
