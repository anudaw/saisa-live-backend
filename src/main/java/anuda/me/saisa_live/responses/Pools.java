package anuda.me.saisa_live.responses;

import anuda.me.saisa_live.entities.Participants;

import java.util.List;

public class Pools {        //Pools response object

    private int pool;       //pool number
    private List<ParticipantResponse> participants;        //List of participants in this pool

    public List<ParticipantResponse> getParticipants() {       //Getter, and setter methods to work with the data in the object
        return participants;
    }

    public void setParticipants(List<ParticipantResponse> participants) {
        this.participants = participants;
    }

    public int getPool() {
        return pool;
    }

    public void setPool(int pool) {
        this.pool = pool;
    }
}
