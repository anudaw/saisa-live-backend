package anuda.me.saisa_live.responses;

import anuda.me.saisa_live.entities.Livestreams;
import anuda.me.saisa_live.entities.Tournaments;

public class MeetsResponse {

    private int id;
    private ParticipantResponse p1Team;//
    private ParticipantResponse p2Team;//
    private ParticipantResponse p3Team;//
    private int activeStatus;
    private Livestreams livestream;//
    private Tournaments tournament;//
    private String description;
    private String startTime;
    private String resultUrl;
    private String p1name;
    private String p2name;
    private String p3name;
    private String p1result;
    private String p2result;
    private String p3result;
    private boolean p1record;
    private boolean p2record;
    private boolean p3record;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ParticipantResponse getP1Team() {
        return p1Team;
    }

    public void setP1Team(ParticipantResponse p1Team) {
        this.p1Team = p1Team;
    }

    public ParticipantResponse getP2Team() {
        return p2Team;
    }

    public void setP2Team(ParticipantResponse p2Team) {
        this.p2Team = p2Team;
    }

    public ParticipantResponse getP3Team() {
        return p3Team;
    }

    public void setP3Team(ParticipantResponse p3Team) {
        this.p3Team = p3Team;
    }

    public int getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(int activeStatus) {
        this.activeStatus = activeStatus;
    }

    public Livestreams getLivestream() {
        return livestream;
    }

    public void setLivestream(Livestreams livestream) {
        this.livestream = livestream;
    }

    public Tournaments getTournament() {
        return tournament;
    }

    public void setTournament(Tournaments tournament) {
        this.tournament = tournament;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getResultUrl() {
        return resultUrl;
    }

    public void setResultUrl(String resultUrl) {
        this.resultUrl = resultUrl;
    }

    public String getP1name() {
        return p1name;
    }

    public void setP1name(String p1name) {
        this.p1name = p1name;
    }

    public String getP2name() {
        return p2name;
    }

    public void setP2name(String p2name) {
        this.p2name = p2name;
    }

    public String getP3name() {
        return p3name;
    }

    public void setP3name(String p3name) {
        this.p3name = p3name;
    }

    public String getP1result() {
        return p1result;
    }

    public void setP1result(String p1result) {
        this.p1result = p1result;
    }

    public String getP2result() {
        return p2result;
    }

    public void setP2result(String p2result) {
        this.p2result = p2result;
    }

    public String getP3result() {
        return p3result;
    }

    public void setP3result(String p3result) {
        this.p3result = p3result;
    }

    public boolean isP1record() {
        return p1record;
    }

    public void setP1record(boolean p1record) {
        this.p1record = p1record;
    }

    public boolean isP2record() {
        return p2record;
    }

    public void setP2record(boolean p2record) {
        this.p2record = p2record;
    }

    public boolean isP3record() {
        return p3record;
    }

    public void setP3record(boolean p3record) {
        this.p3record = p3record;
    }
}
