package anuda.me.saisa_live.responses;

import anuda.me.saisa_live.entities.Livestreams;
import anuda.me.saisa_live.entities.Tournaments;

public class GameResponse {

    private int id;
    private ParticipantResponse team1;
    private ParticipantResponse team2;
    private String team1Score;
    private String team2Score;
    private int result;
    private int activeStatus;
    private Livestreams livestream;
    private Tournaments tournament;
    private String location;
    private String gameDescription;
    private String startTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ParticipantResponse getTeam1() {
        return team1;
    }

    public void setTeam1(ParticipantResponse team1) {
        this.team1 = team1;
    }

    public ParticipantResponse getTeam2() {
        return team2;
    }

    public void setTeam2(ParticipantResponse team2) {
        this.team2 = team2;
    }

    public String getTeam1Score() {
        return team1Score;
    }

    public void setTeam1Score(String team1Score) {
        this.team1Score = team1Score;
    }

    public String getTeam2Score() {
        return team2Score;
    }

    public void setTeam2Score(String team2Score) {
        this.team2Score = team2Score;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public int getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(int activeStatus) {
        this.activeStatus = activeStatus;
    }

    public Livestreams getLivestream() {
        return livestream;
    }

    public void setLivestream(Livestreams livestream) {
        this.livestream = livestream;
    }

    public Tournaments getTournament() {
        return tournament;
    }

    public void setTournament(Tournaments tournament) {
        this.tournament = tournament;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getGameDescription() {
        return gameDescription;
    }

    public void setGameDescription(String gameDescription) {
        this.gameDescription = gameDescription;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
}
