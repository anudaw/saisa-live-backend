package anuda.me.saisa_live.responses;


import anuda.me.saisa_live.entities.Participants;

import java.util.List;

public class TournamentParticipantsResponse {   //Tournament participant response object to get participant details for a tournament

    private String name;                //Tournament Name
    private int sportId;                //Sport at tournament
    private String url;                 //URL to tournament webpage
    private String logo;                //Location of tournament artwork
    private String location;            //Tournament location
    private String startDate;           //tournament start date
    private String endDate;             //tournament end date
    private Boolean isActive;           //Is the tournament active
    private Boolean standingsActive;    //Are standings active for this tournament
    private Boolean scoresActive;       //Are scores active for this tournament
    private Boolean poolsActive;        //Are pools active for this tournament
    private int poolQuantity;           //How many pools are in this tournament
    private List<Pools> pools;          //List of pools objects that contain participants from each pool
    private Boolean meetsActive;

    public Boolean getMeetsActive() {
        return meetsActive;
    }

    public void setMeetsActive(Boolean meetsActive) {
        this.meetsActive = meetsActive;
    }

    public String getName() {           //getter and setter methods to work with the data in the object
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSportId() {
        return sportId;
    }

    public void setSportId(int sportId) {
        this.sportId = sportId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public Boolean getStandingsActive() {
        return standingsActive;
    }

    public void setStandingsActive(Boolean standingsActive) {
        this.standingsActive = standingsActive;
    }

    public Boolean getScoresActive() {
        return scoresActive;
    }

    public void setScoresActive(Boolean scoresActive) {
        this.scoresActive = scoresActive;
    }

    public Boolean getPoolsActive() {
        return poolsActive;
    }

    public void setPoolsActive(Boolean poolsActive) {
        this.poolsActive = poolsActive;
    }

    public int getPoolQuantity() {
        return poolQuantity;
    }

    public void setPoolQuantity(int poolQuantity) {
        this.poolQuantity = poolQuantity;
    }

    public List<Pools> getPools() {
        return pools;
    }

    public void setPools(List<Pools> pools) {
        this.pools = pools;
    }
}
