package anuda.me.saisa_live.responses;

import anuda.me.saisa_live.entities.Tournaments;

public class LivestreamsResponse {      //Livestreams response object

    private int id;                     //ID or primary key
    private Tournaments tournament;           //the tournaments object from the tournaments table
    private String url;                 //URL to the livestream
    private String description;         //Description of the livestream
    private boolean isActive;           //Is the livestream active and working?
    private boolean isLive;             //Is the livestream live now?

    public int getId() {        //Getter/Setter methods to work with data in object
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Tournaments getTournament() {
        return tournament;
    }

    public void setTournament(Tournaments tournament) {
        this.tournament = tournament;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isLive() {
        return isLive;
    }

    public void setLive(boolean live) {
        isLive = live;
    }
}
