package anuda.me.saisa_live.helpers;

import anuda.me.saisa_live.entities.*;
import anuda.me.saisa_live.requests.*;

public class ObjectSettingHelper {         //Helper class to create entity objects from request objects. This is to avoid a lot of duplicate code.

    public Games createGamesEntity(GameRequest request, Games gamesEntity){

        gamesEntity.setTeam1(request.getTeam1());
        gamesEntity.setTeam2(request.getTeam2());
        gamesEntity.setTeam1_score(request.getTeam1Score());
        gamesEntity.setTeam2_score(request.getTeam2Score());
        gamesEntity.setResult(request.getResult());
        gamesEntity.setActiveStatus(request.getActiveStatus());
        gamesEntity.setLivestreamId(request.getLivestreamId());
        gamesEntity.setTournamentId(request.getTournamentId());
        gamesEntity.setLocation(request.getLocation());
        gamesEntity.setGameDescription(request.getGameDescription());
        gamesEntity.setStartTime(request.getStartTime());

        return gamesEntity;

    }
    public Sports createSportsEntity(SportsRequest request, Sports sportsEntity){
        sportsEntity.setIcon(request.getIcon());
        sportsEntity.setIndoor(request.isIndoor());
        sportsEntity.setName(request.getName());

        return sportsEntity;
    }

    public Media createMediaEntity(MediaRequest request, Media mediaEntity){
        mediaEntity.setActive(request.isActive());
        mediaEntity.setContentUrl(request.getContentUrl());
        mediaEntity.setCoverImg(request.getCoverImg());
        mediaEntity.setTitle(request.getTitle());
        mediaEntity.setTournamentId(request.getTournamentId());
        mediaEntity.setType(request.getType());
        mediaEntity.setText(request.getText());

        return mediaEntity;
    }

    public Livestreams createLiveStreamsEntity(LivestreamRequest request, Livestreams livestreamsEntity){
        livestreamsEntity.setLive(request.isLive());
        livestreamsEntity.setActive(request.isActive());
        livestreamsEntity.setDescription(request.getDescription());
        livestreamsEntity.setTournamentId(request.getTournamentId());
        livestreamsEntity.setUrl(request.getUrl());

        return livestreamsEntity;
    }

    public Teams createTeamsEntity(TeamRequest request, Teams teamEntity){

        teamEntity.setFullName(request.getFullName());
        teamEntity.setLogo(request.getLogo());
        teamEntity.setMascot(request.getMascot());
        teamEntity.setName(request.getName());
        teamEntity.setWebUrl(request.getWebUrl());
        teamEntity.setActive(true);

        return teamEntity;
    }

    public Tournaments createTournamentsEntity(TournamentRequest request, Tournaments tournamentsEntity){

        tournamentsEntity.setEndDate(request.getEndDate());
        tournamentsEntity.setMeetsActive(request.isMeetsActive());
        tournamentsEntity.setLocation(request.getLocation());
        tournamentsEntity.setLogo(request.getLogo());
        tournamentsEntity.setName(request.getName());
        tournamentsEntity.setSportId(request.getSportId());
        tournamentsEntity.setStartDate(request.getStartDate());
        tournamentsEntity.setUrl(request.getUrl());
        tournamentsEntity.setActive(true);
        tournamentsEntity.setStandingsActive(request.getStandingsActive());
        tournamentsEntity.setScoresActive(request.getScoresActive());
        tournamentsEntity.setPoolQuantity(request.getPoolQuantity());
        tournamentsEntity.setPoolsActive(request.getPoolsActive());

        return tournamentsEntity;
    }

    public Participants createParticipantsEntity(ParticipantRequest request, Participants participantsEntity){
        participantsEntity.setActive(true);
        participantsEntity.setGames(request.getGames());
        participantsEntity.setLosses(request.getLosses());
        participantsEntity.setPoints(request.getPoints());
        participantsEntity.setPool(request.getPool());
        participantsEntity.setStanding(request.getStanding());
        participantsEntity.setTeamId(request.getTeamId());
        participantsEntity.setTeamPhoto(request.getTeamPhoto());
        participantsEntity.setTies(request.getTies());
        participantsEntity.setTournamentId(request.getTournamentId());
        participantsEntity.setWins(request.getWins());

        return participantsEntity;
    }

    public Meets createMeetsEntity(MeetsRequest request, Meets meetsEntity){

        meetsEntity.setActiveStatus(request.getActiveStatus());
        meetsEntity.setDescription(request.getDescription());
        meetsEntity.setLivestreamId(request.getLivestreamId());
        meetsEntity.setStartTime(request.getStartTime());
        meetsEntity.setTournamentId(request.getTournamentId());

        return meetsEntity;

    }
}
