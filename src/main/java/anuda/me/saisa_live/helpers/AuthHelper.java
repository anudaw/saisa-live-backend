package anuda.me.saisa_live.helpers;

import anuda.me.saisa_live.entities.Admins;
import anuda.me.saisa_live.repositories.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class AuthHelper {



    public Boolean checkCredentials(String uname, String password, boolean hasFullAccess, AdminRepository adminRepository){

        try{

            Admins adminUser = adminRepository.findByUsername(uname);

            if(adminUser==null){
                return false;
            }

            if(!adminUser.getPassword().equals(password)){
                return false;
            }

            if(!adminUser.isActive()){
                return false;
            }

            if(hasFullAccess){
                if(adminUser.getTournamentId()!=0){
                    return false;
                }
            }

            return true;

        }catch (Exception e){
            return false;
        }

    }

}
