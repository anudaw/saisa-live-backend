package anuda.me.saisa_live.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Tournaments {              //Tournament entity class that mirrors the tournaments table on the database

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;                     //ID or primary key
    private String name;                //Tournament Name
    private int sportId;                //Sport at tournament
    private String url;                 //URL to tournament webpage
    private String logo;                //Location of tournament artwork
    private String location;            //Tournament location
    private String startDate;           //tournament start date
    private String endDate;             //tournament end date
    private Boolean isActive;           //Is the tournament active
    private Boolean standingsActive;    //Are standings active for this tournament
    private Boolean scoresActive;       //Are scores active for this tournament
    private Boolean poolsActive;        //Are pools active for this tournament
    private int poolQuantity;           //How many pools are in this tournament
    private boolean meetsActive;

    public boolean isMeetsActive() {
        return meetsActive;
    }

    public void setMeetsActive(boolean meetsActive) {
        this.meetsActive = meetsActive;
    }

    public Boolean getScoresActive() {      //Getter,Setter methods to access data and store data in the object
        return scoresActive;
    }

    public void setScoresActive(Boolean scoresActive) {
        this.scoresActive = scoresActive;
    }

    public Boolean getPoolsActive() {
        return poolsActive;
    }

    public void setPoolsActive(Boolean poolsActive) {
        this.poolsActive = poolsActive;
    }

    public int getPoolQuantity() {
        return poolQuantity;
    }

    public void setPoolQuantity(int poolQuantity) {
        this.poolQuantity = poolQuantity;
    }

    public Boolean getStandingsActive() {
        return standingsActive;
    }

    public void setStandingsActive(Boolean standingsActive) {
        this.standingsActive = standingsActive;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSportId() {
        return sportId;
    }

    public void setSportId(int sportId) {
        this.sportId = sportId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
