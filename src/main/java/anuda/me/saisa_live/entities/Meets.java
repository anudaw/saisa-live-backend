package anuda.me.saisa_live.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Meets {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int tournamentId;
    private String description;
    private String startTime;
    private int activeStatus;
    private int livestreamId;
    private String resultUrl;
    private int p1teamId;
    private int p2teamId;
    private int p3teamId;
    private String p1name;
    private String p2name;
    private String p3name;
    private String p1result;
    private String p2result;
    private String p3result;
    private boolean p1record;
    private boolean p2record;
    private boolean p3record;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTournamentId() {
        return tournamentId;
    }

    public void setTournamentId(int tournamentId) {
        this.tournamentId = tournamentId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public int getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(int activeStatus) {
        this.activeStatus = activeStatus;
    }

    public int getLivestreamId() {
        return livestreamId;
    }

    public void setLivestreamId(int livestreamId) {
        this.livestreamId = livestreamId;
    }

    public String getResultUrl() {
        return resultUrl;
    }

    public void setResultUrl(String resultUrl) {
        this.resultUrl = resultUrl;
    }

    public int getP1teamId() {
        return p1teamId;
    }

    public void setP1teamId(int p1teamId) {
        this.p1teamId = p1teamId;
    }

    public int getP2teamId() {
        return p2teamId;
    }

    public void setP2teamId(int p2teamId) {
        this.p2teamId = p2teamId;
    }

    public int getP3teamId() {
        return p3teamId;
    }

    public void setP3teamId(int p3teamId) {
        this.p3teamId = p3teamId;
    }

    public String getP1name() {
        return p1name;
    }

    public void setP1name(String p1name) {
        this.p1name = p1name;
    }

    public String getP2name() {
        return p2name;
    }

    public void setP2name(String p2name) {
        this.p2name = p2name;
    }

    public String getP3name() {
        return p3name;
    }

    public void setP3name(String p3name) {
        this.p3name = p3name;
    }

    public String getP1result() {
        return p1result;
    }

    public void setP1result(String p1result) {
        this.p1result = p1result;
    }

    public String getP2result() {
        return p2result;
    }

    public void setP2result(String p2result) {
        this.p2result = p2result;
    }

    public String getP3result() {
        return p3result;
    }

    public void setP3result(String p3result) {
        this.p3result = p3result;
    }

    public boolean isP1record() {
        return p1record;
    }

    public void setP1record(boolean p1record) {
        this.p1record = p1record;
    }

    public boolean isP2record() {
        return p2record;
    }

    public void setP2record(boolean p2record) {
        this.p2record = p2record;
    }

    public boolean isP3record() {
        return p3record;
    }

    public void setP3record(boolean p3record) {
        this.p3record = p3record;
    }


}
