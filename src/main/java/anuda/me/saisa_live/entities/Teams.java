package anuda.me.saisa_live.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Teams {                //Teams entity class that mirrors the teams table on the database

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;                 //ID or primary key
    private String name;            //Team name
    private String fullName;        //Team full name
    private String logo;            //location of team logo
    private String mascot;          //Team mascot name
    private String webUrl;          //URL to team webpage
    private Boolean isActive;       //Is the team active or deactivated

    public Boolean getActive() {    //Getter,Setter methods to access data and store data in the object
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getMascot() {
        return mascot;
    }

    public void setMascot(String mascot) {
        this.mascot = mascot;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }
}
