package anuda.me.saisa_live.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Participants {             //Participants entity class that mirrors the participants table on the database

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;                     //ID or primary key
    private int teamId;                 //the team's ID from the teams table
    private int tournamentId;           //the tournaments ID from the tournaments table
    private int standing;               //The participant's standing in the tournament
    private int wins;                   //Number of games won
    private int losses;                 //Number of games lost
    private int ties;                   //Number of ties
    private int games;                  //Number of games played
    private double points;              //Number of points acquired in the tournament
    private int pool;                   //The participant's pool/group
    private String teamPhoto;           //Location of participant's team photo
    private boolean isActive;           //Is the participant active or deactivated

    public boolean isActive() {         //Getter,Setter methods to access data and store data in the object
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTeamId() {
        return teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    public int getTournamentId() {
        return tournamentId;
    }

    public void setTournamentId(int tournamentId) {
        this.tournamentId = tournamentId;
    }

    public int getStanding() {
        return standing;
    }

    public void setStanding(int standing) {
        this.standing = standing;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getLosses() {
        return losses;
    }

    public void setLosses(int losses) {
        this.losses = losses;
    }

    public int getTies() {
        return ties;
    }

    public void setTies(int ties) {
        this.ties = ties;
    }

    public int getGames() {
        return games;
    }

    public void setGames(int games) {
        this.games = games;
    }

    public double getPoints() {
        return points;
    }

    public void setPoints(double points) {
        this.points = points;
    }

    public int getPool() {
        return pool;
    }

    public void setPool(int pool) {
        this.pool = pool;
    }

    public String getTeamPhoto() {
        return teamPhoto;
    }

    public void setTeamPhoto(String teamPhoto) {
        this.teamPhoto = teamPhoto;
    }
}
