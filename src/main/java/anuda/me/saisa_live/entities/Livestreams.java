package anuda.me.saisa_live.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Livestreams {             //Livestreams entity class that mirrors the livestreams table on the database

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;                     //ID or primary key
    private int tournamentId;           //the tournaments ID from the tournaments table
    private String url;                 //URL to the livestream
    private String description;         //Description of the livestream
    private boolean isActive;           //Is the livestream active and working?
    private boolean isLive;             //Is the livestream live now?

    public int getId() {                //Getter,Setter methods to access data and store data in the object
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTournamentId() {
        return tournamentId;
    }

    public void setTournamentId(int tournamentId) {
        this.tournamentId = tournamentId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isLive() {
        return isLive;
    }

    public void setLive(boolean live) {
        isLive = live;
    }
}
