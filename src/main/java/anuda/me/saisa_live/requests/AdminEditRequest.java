package anuda.me.saisa_live.requests;

public class AdminEditRequest {

    private String creatorUsername;
    private String creatorPassword;
    private String username;
    private String password;
    private boolean isActive;
    private int tournamentId;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public int getTournamentId() {
        return tournamentId;
    }

    public void setTournamentId(int tournamentId) {
        this.tournamentId = tournamentId;
    }

    public String getCreatorUsername() {
        return creatorUsername;
    }

    public void setCreatorUsername(String creatorUsername) {
        this.creatorUsername = creatorUsername;
    }

    public String getCreatorPassword() {
        return creatorPassword;
    }

    public void setCreatorPassword(String creatorPassword) {
        this.creatorPassword = creatorPassword;
    }
}
