package anuda.me.saisa_live.requests.notifications;

import java.util.ArrayList;
import java.util.List;

public class CreateNotificationRequest {

    private String app_id;
    private List<String> include_external_user_ids;
    private NotificationHeadings headings;
    private NotificationContents contents;

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public List<String> getInclude_external_user_ids() {
        return include_external_user_ids;
    }

    public void setInclude_external_user_ids(List<String> include_external_user_ids) {
        this.include_external_user_ids = include_external_user_ids;
    }

    public NotificationHeadings getHeadings() {
        return headings;
    }

    public void setHeadings(NotificationHeadings headings) {
        this.headings = headings;
    }

    public NotificationContents getContents() {
        return contents;
    }

    public void setContents(NotificationContents contents) {
        this.contents = contents;
    }
}
