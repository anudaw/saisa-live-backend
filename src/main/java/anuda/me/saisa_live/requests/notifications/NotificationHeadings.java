package anuda.me.saisa_live.requests.notifications;

public class NotificationHeadings {

    private String en;

    public String getEn() {
        return en;
    }

    public void setEn(String en) {
        this.en = en;
    }
}
