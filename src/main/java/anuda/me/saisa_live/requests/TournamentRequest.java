package anuda.me.saisa_live.requests;

public class TournamentRequest {        //Request object sent when adding/editing and doing other work with tournaments

    private String name;                //Name of tournament. Example: SAISA Volleyball 2025
    private int sportId;                //Which sport is the tournament - id is from the sports table. Example: 2
    private String url;                 //URL to tournament webpage. Example: https://osc.lk/saisa
    private String logo;                //Location of tournament logo/artwork. Example: assets/images/logo.png
    private String location;            //Tournament Location. Example: Chennai
    private String startDate;           //tournament start date. Example: 21-12-2025
    private String endDate;             //tournament end date. Example: 24-12-2025
    private Boolean standingsActive;    //Are standings active for this tournament. Example: true
    private Boolean scoresActive;       //Are scores active for this tournament. Example: false
    private Boolean poolsActive;        //Are pools active for this tournament. Example: true
    private int poolQuantity;           //How many pools will be there. This will be 0, if poolsActive is false. Example: false
    private String username;
    private String password;
    private boolean meetsActive;

    public boolean isMeetsActive() {
        return meetsActive;
    }

    public void setMeetsActive(boolean meetsActive) {
        this.meetsActive = meetsActive;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getStandingsActive() {   //Getter and setter methods to work with this object
        return standingsActive;
    }

    public void setStandingsActive(Boolean standingsActive) {
        this.standingsActive = standingsActive;
    }

    public Boolean getScoresActive() {
        return scoresActive;
    }

    public void setScoresActive(Boolean scoresActive) {
        this.scoresActive = scoresActive;
    }

    public Boolean getPoolsActive() {
        return poolsActive;
    }

    public void setPoolsActive(Boolean poolsActive) {
        this.poolsActive = poolsActive;
    }

    public int getPoolQuantity() {
        return poolQuantity;
    }

    public void setPoolQuantity(int poolQuantity) {
        this.poolQuantity = poolQuantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSportId() {
        return sportId;
    }

    public void setSportId(int sportId) {
        this.sportId = sportId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
