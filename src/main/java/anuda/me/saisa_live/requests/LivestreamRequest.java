package anuda.me.saisa_live.requests;

public class LivestreamRequest {        //Request object sent when adding/editing and doing other work with Livestreams

    private int tournamentId;           //the tournaments ID from the tournaments table
    private String url;                 //URL to the livestream
    private String description;         //Description of the livestream
    private boolean isActive;           //Is the livestream active and working?
    private boolean isLive;             //Is the livestream live now?
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getTournamentId() {      //Getter,Setter methods to access data and store data in the object
        return tournamentId;
    }

    public void setTournamentId(int tournamentId) {
        this.tournamentId = tournamentId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isLive() {
        return isLive;
    }

    public void setLive(boolean live) {
        isLive = live;
    }
}
