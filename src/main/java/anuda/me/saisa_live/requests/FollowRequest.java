package anuda.me.saisa_live.requests;

import java.util.List;

public class FollowRequest {

    private int userId;
    private List<Integer> followingList;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public List<Integer> getFollowingList() {
        return followingList;
    }

    public void setFollowingList(List<Integer> followingList) {
        this.followingList = followingList;
    }
}
