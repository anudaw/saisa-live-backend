package anuda.me.saisa_live.requests;

public class ParticipantRequest {   //Request object sent when adding/editing and doing other work with participants of tournaments

    private int teamId;             //The team ID from the Teams table. Example: 23
    private int tournamentId;       //The tournament ID the team is participating in from tournaments table. Example: 2
    private int standing;           //The participant's standing/seed in the tournament. This will be 0, if standings have not been activated for the chosen tournament. Example:2
    private int wins;               //The number of games the participant has won. This will be 0, if the tournament has scores deactivated.
    private int losses;             //The number of games the participant has lost. This will be 0, if the tournament has scores deactivated.
    private int ties;               //The number of games the participant has tied. This will be 0, if the tournament has scores deactivated.
    private int games;              //The total number of games that the participant has played. This will be 0, if the tournament has scores deactivated.
    private double points;          //The total number of round-robin/tournament points the participant has won. This will be 0, if the tournament has scores deactivated.
    private int pool;               //Pool number the team has been allocated to. Pool A will be 1, and Pool B should be 2, and so on. If pools have been deactivated for the tournament, this can be 0.
    private String teamPhoto;       //Location of Team photo. Example: assets/images/logo.png
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getTeamId() {        //Getter and setter methods to work with this object
        return teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    public int getTournamentId() {
        return tournamentId;
    }

    public void setTournamentId(int tournamentId) {
        this.tournamentId = tournamentId;
    }

    public int getStanding() {
        return standing;
    }

    public void setStanding(int standing) {
        this.standing = standing;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getLosses() {
        return losses;
    }

    public void setLosses(int losses) {
        this.losses = losses;
    }

    public int getTies() {
        return ties;
    }

    public void setTies(int ties) {
        this.ties = ties;
    }

    public int getGames() {
        return games;
    }

    public void setGames(int games) {
        this.games = games;
    }

    public double getPoints() {
        return points;
    }

    public void setPoints(double points) {
        this.points = points;
    }

    public int getPool() {
        return pool;
    }

    public void setPool(int pool) {
        this.pool = pool;
    }

    public String getTeamPhoto() {
        return teamPhoto;
    }

    public void setTeamPhoto(String teamPhoto) {
        this.teamPhoto = teamPhoto;
    }
}
