package anuda.me.saisa_live.requests;

public class TeamRequest {          //Request object sent when adding/editing and doing other work with teams

    private String name;        //Team short name. Example: MIT.
    private String fullName;    //Team full name. Example: Massachusetts Institute of Technology
    private String mascot;      //Team mascot name. Example: Bears
    private String webUrl;      //URL to team website. Example: https://google.com
    private String logo;        //Location of Team Logo. Example: assets/images/logo.png
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {   //Getter and setter methods to work with this object
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMascot() {
        return mascot;
    }

    public void setMascot(String mascot) {
        this.mascot = mascot;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}
