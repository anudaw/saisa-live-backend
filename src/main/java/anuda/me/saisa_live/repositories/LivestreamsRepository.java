package anuda.me.saisa_live.repositories;

import anuda.me.saisa_live.entities.Livestreams;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface LivestreamsRepository extends CrudRepository<Livestreams, Long> {      //Repository used to access the Livestreams table on the database

    Livestreams findById(int id);           //Function to get records by ID
    List<Livestreams> findAllByTournamentId(int tournamentId);      //Function to get a list of all livestreams from a tournament
    List<Livestreams> findAll();        //Function to get a list of all livestreams
    List<Livestreams> findAllByIsLive(Boolean liveStatus);

}
