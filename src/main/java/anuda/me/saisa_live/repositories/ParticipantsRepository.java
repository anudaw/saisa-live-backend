package anuda.me.saisa_live.repositories;

import anuda.me.saisa_live.entities.Participants;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface ParticipantsRepository extends CrudRepository<Participants, Long> {      //Repository used to access the Participants table on the database

    Participants findByTournamentIdAndTeamId(int tournamentId, int teamId);         //Function to find participant records by tournamentId and teamId

    Participants findById(int id);      //Function to find participant records by ID

    Participants findByTeamIdAndTournamentId(int teamId, int tournamentId);

    List<Participants> findAllByTournamentId(int tournamentId);    //Function to find all participants from a given tournament

    List<Participants> findAllByTournamentIdAndPool(int tournamentId, int pool); //Function to find all participants in same pool from a given tournament
}
