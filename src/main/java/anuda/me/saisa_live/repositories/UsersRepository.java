package anuda.me.saisa_live.repositories;

import anuda.me.saisa_live.entities.Users;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface UsersRepository extends CrudRepository<Users, Long> {

    Users findById(int userId);
}
