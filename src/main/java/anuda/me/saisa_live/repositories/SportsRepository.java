package anuda.me.saisa_live.repositories;


import anuda.me.saisa_live.entities.Sports;
import anuda.me.saisa_live.entities.Tournaments;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface SportsRepository extends CrudRepository<Sports, Long> {

    List<Sports> findAll();
    Sports findById(int sportsId);

}
