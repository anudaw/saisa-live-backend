package anuda.me.saisa_live.repositories;

import anuda.me.saisa_live.entities.Admins;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface AdminRepository extends CrudRepository<Admins, Long> {

    Admins findByUsername(String username);

    List<Admins> findAll();
    List<Admins> findAllByTournamentId(int tournamentId);
    Admins findById(int id);

}
