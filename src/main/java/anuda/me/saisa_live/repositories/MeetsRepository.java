package anuda.me.saisa_live.repositories;

import anuda.me.saisa_live.entities.Meets;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface MeetsRepository extends CrudRepository<Meets, Long> {

    Meets findById(int id);
    List<Meets> findAllByActiveStatusAndTournamentId(int activeStatus, int tournamentId);
    List<Meets> findAllByTournamentId(int tournamentId);
    List<Meets> findAllByActiveStatus(int activeStatus);
    List<Meets> findAll();
}
