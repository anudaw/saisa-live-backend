package anuda.me.saisa_live.repositories;


import anuda.me.saisa_live.entities.Tournaments;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface TournamentsRepository extends CrudRepository<Tournaments, Long> {      //Repository used to access the Tournaments table on the database

    Tournaments findById(int id);                               //Function to find records by the ID
    List<Tournaments> findAll();
    List<Tournaments> findAllByIsActive(Boolean isActive);      //Function to find all active records and return a list.

}
