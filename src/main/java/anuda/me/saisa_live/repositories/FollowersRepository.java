package anuda.me.saisa_live.repositories;

import anuda.me.saisa_live.entities.Followers;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface FollowersRepository extends CrudRepository<Followers, Long> {

    List<Followers> findAllByTeamId(int teamId);
}
