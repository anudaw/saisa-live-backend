package anuda.me.saisa_live.repositories;

import anuda.me.saisa_live.entities.Games;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface GamesRepository extends CrudRepository<Games, Long> {

    Games findById(int id);
    List<Games> findAllByTournamentIdAndActiveStatus(int tournamentId, int activeStatus);
    List<Games> findAllByTournamentId(int tournamentId);
    List<Games> findAllByActiveStatus(int activeStatus);
    List<Games> findAll();
}
