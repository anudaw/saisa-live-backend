package anuda.me.saisa_live.repositories;

import anuda.me.saisa_live.entities.Teams;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface TeamsRepository extends CrudRepository<Teams, Long> {      //Repository used to access the Teams table on the database

    Teams findById(int id);                                     //Function to find records by the ID
    List<Teams> findAllByIsActive(boolean activeStatus);        //Function to find all active records and return a list.
}
