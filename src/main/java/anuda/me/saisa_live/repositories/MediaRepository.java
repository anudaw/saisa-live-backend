package anuda.me.saisa_live.repositories;

import anuda.me.saisa_live.entities.Media;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;


@Transactional
public interface MediaRepository extends CrudRepository<Media, Long> {

    Media findById(int mediaId);
    List<Media> findAllByTournamentId(int tournamentId);
    List<Media> findAllByType(int mediaType);
    List<Media> findAll();
    List<Media> findAllByTournamentIdAndType(int tournamentId, int type);


}
