package anuda.me.saisa_live.repositories;

import anuda.me.saisa_live.entities.Accesscodes;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface AccessRepository  extends CrudRepository<Accesscodes, Long> {

    List<Accesscodes> findAll();
    Accesscodes findByAccessCode(String accessCode);
    Accesscodes findById(int id);

}
