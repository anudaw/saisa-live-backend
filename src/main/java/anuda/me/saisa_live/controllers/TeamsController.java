package anuda.me.saisa_live.controllers;

import anuda.me.saisa_live.entities.Teams;
import anuda.me.saisa_live.helpers.AuthHelper;
import anuda.me.saisa_live.helpers.ObjectSettingHelper;
import anuda.me.saisa_live.repositories.AdminRepository;
import anuda.me.saisa_live.repositories.TeamsRepository;
import anuda.me.saisa_live.requests.TeamRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(value = "teams")            //Methods in this file will be at /teams endpoint
public class TeamsController {

    @Autowired
    private TeamsRepository teamsRepository;       //Connecting to repository to be able to access DB

    @Autowired
    private AdminRepository adminRepository;

    @RequestMapping(value = "new", method = RequestMethod.POST)         //POST method to create new team
    public ResponseEntity<?> addNewTeam(@RequestBody TeamRequest request) {      //This method request is the TeamRequest object

        try {
            Teams team = new Teams();       //Create new Teams object

            team = new ObjectSettingHelper().createTeamsEntity(request, team);      //Using ObjectSettingHelper class to populate the team entity with data from the request object

            teamsRepository.save(team);         //Save Teams Object

            return new ResponseEntity<>(HttpStatus.OK);     //Return success response

        } catch (Exception e) {

            return new ResponseEntity<Object>(e, HttpStatus.INTERNAL_SERVER_ERROR);     //Catch exceptions and return failure response with exception

        }

    }

    @RequestMapping(value = "edit", method = RequestMethod.POST)        //POST method to edit existing teams
    public ResponseEntity<?> editExistingTeam(@RequestParam int teamId, @RequestBody TeamRequest request) {     //This method requests a the TeamRequest object and the teamId as a URL parameter

        try {
            if(!(new AuthHelper().checkCredentials(request.getUsername(), request.getPassword(), false, adminRepository))){
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
            Teams team = teamsRepository.findById(teamId);       //Get teams object with same ID from DB

            if (team == null) {     //If team is null then, the team does not exist in the DB
                return new ResponseEntity(HttpStatus.NOT_FOUND);        //return not found status
            }
            team = new ObjectSettingHelper().createTeamsEntity(request, team);  //Using ObjectSettingHelper class to populate the team entity with data from the request object

            teamsRepository.save(team);         //Save Teams Object

            return new ResponseEntity<>(HttpStatus.OK);     //Return success response

        } catch (Exception e) {

            return new ResponseEntity<Object>(e, HttpStatus.INTERNAL_SERVER_ERROR);     //Catch exceptions and return failure response with exception

        }
    }

    @RequestMapping(value = "status", method = RequestMethod.POST)      //POST method to update team status
    public ResponseEntity<?> changeStatus(@RequestParam boolean newStatus, @RequestParam int teamId) {      //This method requests the new status and the teamId as URL parameters

        try {

            Teams team = teamsRepository.findById(teamId);       //Get teams object with same ID from DB
            if (team == null) {     //If team is null then, the team does not exist in the DB
                return new ResponseEntity(HttpStatus.NOT_FOUND);        //return not found status
            }
            team.setActive(newStatus);      //change the activeStatus on the team object to the new status
            teamsRepository.save(team);         //Save Teams Object

            return new ResponseEntity<>(HttpStatus.OK);     //Return success response

        } catch (Exception e) {

            return new ResponseEntity<Object>(e, HttpStatus.INTERNAL_SERVER_ERROR);     //Catch exceptions and return failure response with exception

        }
    }

    @RequestMapping(value = "", method = RequestMethod.GET)         //GET method to get teams data
    public ResponseEntity<?> getTeams(@RequestParam int teamId) {   //The teamId is sent as an URL parameter
        try {
            if (teamId == 0) {
                List<Teams> teamsList = teamsRepository.findAllByIsActive(true);        //If the teamId requested is 0, return a list of all active teams
                return new ResponseEntity<>(teamsList, HttpStatus.OK);
            } else {
                Teams team = teamsRepository.findById(teamId);      //Find the team with the required ID from the database
                if (team == null) {
                    return new ResponseEntity(HttpStatus.NOT_FOUND);        //If there is no record in DB with selected id, return not found response
                }
                return new ResponseEntity<>(team, HttpStatus.OK);   //Return team data with success response
            }
        } catch (Exception e) {

            return new ResponseEntity<Object>(e, HttpStatus.INTERNAL_SERVER_ERROR);     //Catch exceptions and return failure response with exception

        }
    }
}
