package anuda.me.saisa_live.controllers;


import anuda.me.saisa_live.entities.*;
import anuda.me.saisa_live.helpers.AuthHelper;
import anuda.me.saisa_live.helpers.ObjectSettingHelper;
import anuda.me.saisa_live.repositories.*;
import anuda.me.saisa_live.requests.AuthRequest;
import anuda.me.saisa_live.requests.GameRequest;
import anuda.me.saisa_live.responses.GameResponse;
import anuda.me.saisa_live.responses.ParticipantResponse;
import anuda.me.saisa_live.services.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping(value = "games")
public class GamesController {


    @Autowired
    private GamesRepository gamesRepository;

    @Autowired
    private TournamentsRepository tournamentsRepository;

    @Autowired
    private ParticipantsRepository participantsRepository;

    @Autowired
    private TeamsRepository teamsRepository;

    @Autowired
    private LivestreamsRepository livestreamsRepository;

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private FollowersRepository followersRepository;

    @RequestMapping(value = "new", method = RequestMethod.POST)
    public ResponseEntity<?> addNewGames(@RequestBody GameRequest request) {

        try {

            if (!(new AuthHelper().checkCredentials(request.getUsername(), request.getPassword(), false, adminRepository))) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }

            Participants team1 = participantsRepository.findById(request.getTeam1());
            Participants team2 = participantsRepository.findById(request.getTeam2());
            Tournaments tournament = tournamentsRepository.findById(request.getTournamentId());

            if (team1 == null || team2 == null) {
                return new ResponseEntity<Object>("These teams are not registered with this tournament.", HttpStatus.NOT_FOUND);
            }

            if (request.getTeam1() == request.getTeam2()) {

                return new ResponseEntity<Object>("Team 1 and Team 2 cannot be the same.", HttpStatus.BAD_REQUEST);

            }
            if (tournament == null) {
                return new ResponseEntity<Object>("Invalid tournament. Please try again", HttpStatus.NOT_FOUND);
            }

            if(!tournament.getScoresActive()){
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }

            if (request.getActiveStatus() != 0) {
                Livestreams livestream = livestreamsRepository.findById(request.getLivestreamId());
                if (livestream == null) {
                    return new ResponseEntity<Object>("Invalid Livestream. Please try again", HttpStatus.NOT_FOUND);
                }
            }


            Games game = new Games();
            game = new ObjectSettingHelper().createGamesEntity(request, game);

            gamesRepository.save(game);

            return new ResponseEntity(HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity(e, HttpStatus.INTERNAL_SERVER_ERROR);     //Catch exceptions and return failure response with exception
        }

    }

    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public ResponseEntity<?> editExistingGame(@RequestBody GameRequest request, @RequestParam Integer gameId) {

        try {
            if (!(new AuthHelper().checkCredentials(request.getUsername(), request.getPassword(), false, adminRepository))) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
            Games game = gamesRepository.findById(gameId);

            if (game == null) {
                return new ResponseEntity<Object>("This game does not exist", HttpStatus.NOT_FOUND);
            }

            Participants team1 = participantsRepository.findById(request.getTeam1());
            Participants team2 = participantsRepository.findById(request.getTeam2());
            Tournaments tournament = tournamentsRepository.findById(request.getTournamentId());
            Livestreams livestream = livestreamsRepository.findById(request.getLivestreamId());

            if (team1 == null || team2 == null) {
                return new ResponseEntity<Object>("These teams are not registered with this tournament.", HttpStatus.NOT_FOUND);
            }

            if (request.getTeam1() == request.getTeam2()) {

                return new ResponseEntity<Object>("Team 1 and Team 2 cannot be the same.", HttpStatus.BAD_REQUEST);

            }
            if (tournament == null) {
                return new ResponseEntity<Object>("Invalid tournament. Please try again", HttpStatus.NOT_FOUND);
            }

            if(!tournament.getScoresActive()){
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }

            if (livestream == null) {
                return new ResponseEntity<Object>("Invalid Livestream. Please try again", HttpStatus.NOT_FOUND);
            }

            game = new ObjectSettingHelper().createGamesEntity(request, game);

            gamesRepository.save(game);

            return new ResponseEntity(HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity(e, HttpStatus.INTERNAL_SERVER_ERROR);     //Catch exceptions and return failure response with exception
        }

    }

    @RequestMapping(value = "status", method = RequestMethod.POST)
    public ResponseEntity<?> changeGameStatus(@RequestParam int gameId, @RequestParam int newStatus, @RequestBody AuthRequest request) {

        try {
            if (!(new AuthHelper().checkCredentials(request.getUsername(), request.getPassword(), false, adminRepository))) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
            Games game = gamesRepository.findById(gameId);

            if (game == null) {
                return new ResponseEntity<Object>("Game not found!", HttpStatus.NOT_FOUND);
            }
            if (newStatus > 3) {
                return new ResponseEntity<Object>("Invalid Status", HttpStatus.BAD_REQUEST);

            }
            game.setActiveStatus(newStatus);

            gamesRepository.save(game);

            if(newStatus == 1){

                int t1Id = participantsRepository.findById(game.getTeam1()).getTeamId();
                int t2Id = participantsRepository.findById(game.getTeam2()).getTeamId();

                Teams t1 = teamsRepository.findById(t1Id);
                Teams t2 = teamsRepository.findById(t2Id);
                Tournaments tournament = tournamentsRepository.findById(game.getTournamentId());

                List<Followers> t1Followers = followersRepository.findAllByTeamId(t1Id);
                List<Followers> t2Followers = followersRepository.findAllByTeamId(t2Id);

                List<String> recipients = new ArrayList<>();

                for(int i=0; i<t1Followers.size();i++){

                    String userId = String.valueOf(t1Followers.get(i).getUserId());

                    recipients.add(userId);

                }
                for(int i=0; i<t2Followers.size();i++){

                    String userId = String.valueOf(t2Followers.get(i).getUserId());

                    recipients.add(userId);

                }

                Set<String> set = new HashSet<>(recipients);
                recipients.clear();
                recipients.addAll(set);

                String heading = t1.getName()+" vs "+t2.getName()+" is LIVE Now!";
                String message = "Catch the "+game.getGameDescription()+" at "+tournament.getName();

                boolean success = new NotificationService().sendNotification(message, heading, recipients);

            }

            return new ResponseEntity(HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity(e, HttpStatus.INTERNAL_SERVER_ERROR);     //Catch exceptions and return failure response with exception
        }
    }

    @RequestMapping(value = "result", method = RequestMethod.POST)
    public ResponseEntity<?> selectGameWinner(@RequestParam int gameId, @RequestParam int result, @RequestBody AuthRequest request) {

        try {
            if (!(new AuthHelper().checkCredentials(request.getUsername(), request.getPassword(), false, adminRepository))) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
            Games game = gamesRepository.findById(gameId);

            if (game == null) {
                return new ResponseEntity<Object>("Game not found!", HttpStatus.NOT_FOUND);
            }
            if (result > 3) {
                return new ResponseEntity<Object>("Invalid result. 1 = team 1 won, 2 = team 2 won, 3 = no result", HttpStatus.BAD_REQUEST);
            }

            game.setResult(result);
            gamesRepository.save(game);

            int t1Id = participantsRepository.findById(game.getTeam1()).getTeamId();
            int t2Id = participantsRepository.findById(game.getTeam2()).getTeamId();

            Teams t1 = teamsRepository.findById(t1Id);
            Teams t2 = teamsRepository.findById(t2Id);
            Tournaments tournament = tournamentsRepository.findById(game.getTournamentId());

            List<Followers> t1Followers = followersRepository.findAllByTeamId(t1Id);
            List<Followers> t2Followers = followersRepository.findAllByTeamId(t2Id);

            List<String> recipients = new ArrayList<>();

            for(int i=0; i<t1Followers.size();i++){

                String userId = String.valueOf(t1Followers.get(i).getUserId());

                recipients.add(userId);

            }
            for(int i=0; i<t2Followers.size();i++){

                String userId = String.valueOf(t2Followers.get(i).getUserId());

                recipients.add(userId);

            }

            Set<String> set = new HashSet<>(recipients);
            recipients.clear();
            recipients.addAll(set);


            String heading;
            String message = "See the full result details and catch the game coverage replay from "+tournament.getName();

            if(result==0){
                heading = game.getGameDescription()+" - "+t1.getName()+" vs "+t2.getName()+" ended in a Tie!";
            }else if(result ==2){
                heading = game.getGameDescription()+" - "+t2.getName()+" just beat "+t1.getName()+"!";
            }else{
                heading = game.getGameDescription()+" - "+t1.getName()+" just beat "+t2.getName()+"!";

            }

            boolean success = new NotificationService().sendNotification(message, heading, recipients);



            return new ResponseEntity(HttpStatus.OK);


        } catch (Exception e) {
            return new ResponseEntity(e, HttpStatus.INTERNAL_SERVER_ERROR);     //Catch exceptions and return failure response with exception
        }
    }

    @RequestMapping(value = "scores", method = RequestMethod.POST)
    public ResponseEntity<?> updateScores(@RequestParam int gameId, @RequestParam String t1Score, @RequestParam String t2Score, @RequestBody AuthRequest request) {


        try {
            if (!(new AuthHelper().checkCredentials(request.getUsername(), request.getPassword(), false, adminRepository))) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
            Games game = gamesRepository.findById(gameId);
            if (game == null) {
                return new ResponseEntity<Object>("Game not found!", HttpStatus.NOT_FOUND);
            }
            Tournaments tournament = tournamentsRepository.findById(game.getTournamentId());


            if (!tournament.getScoresActive()) {
                return new ResponseEntity<Object>("Tournament does not have scores active. You cannot update scores.", HttpStatus.BAD_REQUEST);
            }

            game.setTeam1_score(t1Score);
            game.setTeam2_score(t2Score);

            gamesRepository.save(game);

            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e, HttpStatus.INTERNAL_SERVER_ERROR);     //Catch exceptions and return failure response with exception
        }
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getGames(@RequestParam(required = false) Integer tournamentId, @RequestParam(required = false) Integer activeStatus, @RequestParam(required = false) Integer gameId) {

        try {
            List<Games> gamesRecords = new ArrayList<>();

            if (gameId != null) {
                Games game = gamesRepository.findById(gameId);

                if (game == null) {
                    return new ResponseEntity<Object>("Game not found!", HttpStatus.NOT_FOUND);
                }
                gamesRecords.add(game);
            } else if (tournamentId != null) {

                if (tournamentsRepository.findById(tournamentId) == null) {
                    return new ResponseEntity<Object>("Tournament not found!", HttpStatus.NOT_FOUND);
                }

                if (activeStatus != null) {
                    if (activeStatus > 3) {
                        return new ResponseEntity<Object>("Invalid Actives status", HttpStatus.BAD_REQUEST);
                    }
                    gamesRecords = gamesRepository.findAllByTournamentIdAndActiveStatus(tournamentId, activeStatus);
                } else {
                    gamesRecords = gamesRepository.findAllByTournamentId(tournamentId);
                }
            } else if (activeStatus != null) {

                if (activeStatus > 3) {
                    return new ResponseEntity<Object>("Invalid Actives status", HttpStatus.BAD_REQUEST);
                }

                gamesRecords = gamesRepository.findAllByActiveStatus(activeStatus);
            } else {
                gamesRecords = gamesRepository.findAll();
            }

            List<GameResponse> gameResponses = new ArrayList<>();

            for (int i = 0; i < gamesRecords.size(); i++) {

                Games gameRecord = gamesRecords.get(i);
                GameResponse gameResponse = new GameResponse();

                gameResponse.setId(gameRecord.getId());
                gameResponse.setTeam1Score(gameRecord.getTeam1_score());
                gameResponse.setTeam2Score(gameRecord.getTeam2_score());
                gameResponse.setResult(gameRecord.getResult());
                gameResponse.setActiveStatus(gameRecord.getActiveStatus());
                gameResponse.setLocation(gameRecord.getLocation());
                gameResponse.setGameDescription(gameRecord.getGameDescription());
                gameResponse.setStartTime(gameRecord.getStartTime());

                Livestreams livestream = new Livestreams();

                if (gameRecord.getLivestreamId() != 0) {

                    livestream = livestreamsRepository.findById(gameRecord.getLivestreamId());
                } else {
                    livestream.setUrl("");
                    livestream.setTournamentId(0);
                    livestream.setDescription("");
                    livestream.setActive(true);
                    livestream.setLive(true);
                    livestream.setId(0);
                }

                Tournaments tournament = tournamentsRepository.findById(gameRecord.getTournamentId());
                Participants team1Entity = participantsRepository.findById(gameRecord.getTeam1());
                ParticipantResponse team1 = new ParticipantResponse();

                Participants team2Entity = participantsRepository.findById(gameRecord.getTeam2());
                ParticipantResponse team2 = new ParticipantResponse();

                team1.setId(team1Entity.getId());
                team1.setTeam(teamsRepository.findById(team1Entity.getTeamId()));
                team1.setWins(team1Entity.getWins());
                team1.setTies(team1Entity.getTies());
                team1.setActive(team1Entity.isActive());
                team1.setTeamPhoto(team1Entity.getTeamPhoto());
                team1.setStanding(team1Entity.getStanding());
                team1.setPool(team1Entity.getPool());
                team1.setPoints(team1Entity.getPoints());
                team1.setLosses(team1Entity.getLosses());
                team1.setGames(team1Entity.getGames());

                team2.setId(team2Entity.getId());
                team2.setTeam(teamsRepository.findById(team2Entity.getTeamId()));
                team2.setWins(team2Entity.getWins());
                team2.setTies(team2Entity.getTies());
                team2.setActive(team2Entity.isActive());
                team2.setTeamPhoto(team2Entity.getTeamPhoto());
                team2.setStanding(team2Entity.getStanding());
                team2.setPool(team2Entity.getPool());
                team2.setPoints(team2Entity.getPoints());
                team2.setLosses(team2Entity.getLosses());
                team2.setGames(team2Entity.getGames());

                gameResponse.setLivestream(livestream);
                gameResponse.setTournament(tournament);
                gameResponse.setTeam1(team1);
                gameResponse.setTeam2(team2);

                gameResponses.add(gameResponse);
            }

            gameResponses.sort(Comparator.comparing(e -> new Long(e.getStartTime())));


            return new ResponseEntity<Object>(gameResponses, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity(e, HttpStatus.INTERNAL_SERVER_ERROR);     //Catch exceptions and return failure response with exception
        }

    }

//    @RequestMapping(value = "date-change", method = RequestMethod.GET)
//    public ResponseEntity<?> dateChange() {
//        try {
//
//            List<Games> games = gamesRepository.findAll();
//            for (int i = 0; i < 38; i++) {
//
//                DateFormat fmt = new SimpleDateFormat("hh:mma, EEEE dd MMMM yyyy", Locale.US);
//                String startTime = games.get(78+i).getStartTime().replaceAll(" of", "");
//                Date date = fmt.parse(startTime);
//
////                startTime = startTime.replace("th", "");
////                startTime = startTime.replace("st", "");
////                startTime = startTime.replace("rd", "");
////                startTime = startTime.replace("nd", "");
////                startTime = startTime.replace("Satuay", "Saturday");
//
//                System.out.println(startTime);
//
//
//                games.get(i+78).setStartTime(String.valueOf(date.getTime() / 1000L));
//                gamesRepository.save(games.get(i+78));
//
//            }
//            return new ResponseEntity(HttpStatus.OK);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            return new ResponseEntity(HttpStatus.OK);
//
//        }
//
//    }

}
