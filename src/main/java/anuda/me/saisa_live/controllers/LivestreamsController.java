package anuda.me.saisa_live.controllers;


import anuda.me.saisa_live.entities.Livestreams;
import anuda.me.saisa_live.entities.Tournaments;
import anuda.me.saisa_live.helpers.AuthHelper;
import anuda.me.saisa_live.helpers.ObjectSettingHelper;
import anuda.me.saisa_live.repositories.AdminRepository;
import anuda.me.saisa_live.repositories.LivestreamsRepository;
import anuda.me.saisa_live.repositories.TournamentsRepository;
import anuda.me.saisa_live.requests.LivestreamRequest;
import anuda.me.saisa_live.responses.LivestreamsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "livestreams")
public class LivestreamsController {

    @Autowired
    private TournamentsRepository tournamentsRepository;

    @Autowired
    private LivestreamsRepository livestreamsRepository;

    @Autowired
    private AdminRepository adminRepository;

    @RequestMapping(value = "new", method = RequestMethod.POST)
    public ResponseEntity<?> addNewLivestreams(@RequestBody LivestreamRequest request){

        try{
            if(!(new AuthHelper().checkCredentials(request.getUsername(), request.getPassword(), false, adminRepository))){
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
            Tournaments tournament = tournamentsRepository.findById(request.getTournamentId());

            if(tournament==null){
                return new ResponseEntity<Object>("Tournament not found",HttpStatus.NOT_FOUND);
            }

            Livestreams livestream = new Livestreams();

            String url = request.getUrl();
            url = url.replace("www.", "");
            url = url.replace("youtube.com", "youtu.be");
            url = url.replace("watch?v=", "");
            url = url.replace("&feature=youtu.be","");

            request.setUrl(url);

            livestream = new ObjectSettingHelper().createLiveStreamsEntity(request, livestream);

            livestreamsRepository.save(livestream);

            return new ResponseEntity(HttpStatus.OK);

        }catch (Exception e){
            return new ResponseEntity<Object>(e, HttpStatus.INTERNAL_SERVER_ERROR);      //Catch exceptions and return failure response with exception
        }
    }

    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public ResponseEntity<?> editExistingLivestreams(@RequestBody LivestreamRequest request, @RequestParam int livestreamId){

        try{
            if(!(new AuthHelper().checkCredentials(request.getUsername(), request.getPassword(), false, adminRepository))){
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
            Livestreams livestream = livestreamsRepository.findById(livestreamId);
            if(livestream==null){
                return new ResponseEntity<Object>("Livestream not found", HttpStatus.NOT_FOUND);
            }

            String url = request.getUrl();
            url = url.replace("www.", "");
            url = url.replace("youtube.com", "youtu.be");
            url = url.replace("watch?v=", "");
            url = url.replace("&feature=youtu.be","");

            request.setUrl(url);

            livestream = new ObjectSettingHelper().createLiveStreamsEntity(request, livestream);

            livestreamsRepository.save(livestream);

            return new ResponseEntity(HttpStatus.OK);

        }catch (Exception e){

            return new ResponseEntity<Object>(e, HttpStatus.INTERNAL_SERVER_ERROR);      //Catch exceptions and return failure response with exception

        }
    }

    @RequestMapping(value = "admin", method = RequestMethod.GET)
    public ResponseEntity<?> getLivestreamsAdmin(@RequestParam int tournamentId, @RequestParam(required = false) Integer liveNow){

        try{
            List<Livestreams> livestreamsList = new ArrayList<>();
            List<LivestreamsResponse> livestreamsResponses = new ArrayList<>();

            if(tournamentId==0){

                List<Livestreams> allStreams = new ArrayList<>();
                List<Livestreams> allLiveStreams = livestreamsRepository.findAll();

                if(liveNow==null) {

                    allStreams = allLiveStreams;
                }else{
                    if(liveNow==1){

                        for(int i=0;i<allLiveStreams.size(); i++){
                            if(allLiveStreams.get(i).isLive()){
                                allStreams.add(allLiveStreams.get(i));
                            }
                        }
                    }else{
                        for(int i=0;i<allLiveStreams.size(); i++){
                            if(allLiveStreams.get(i).isLive()==false){
                                allStreams.add(allLiveStreams.get(i));
                            }
                        }
                    }
                }

                for(int i= 0; i<allStreams.size(); i++){
                    livestreamsList.add(allStreams.get(i));
                }

            }else {

                Tournaments tournament = tournamentsRepository.findById(tournamentId);
                if (tournament == null) {
                    return new ResponseEntity<Object>("Tournament not found", HttpStatus.NOT_FOUND);
                }

                List<Livestreams> allFromTournament = livestreamsRepository.findAllByTournamentId(tournamentId);

                if(liveNow==null) {

                    livestreamsList = allFromTournament;
                }else if(liveNow==1){

                    for(int i=0; i<allFromTournament.size(); i++){
                        if(allFromTournament.get(i).isLive()){
                            livestreamsList.add(allFromTournament.get(i));
                        }
                    }
                }else if(liveNow==0){
                    for(int i=0; i<allFromTournament.size(); i++){
                        if(!allFromTournament.get(i).isLive()){
                            livestreamsList.add(allFromTournament.get(i));
                        }
                    }
                }
            }

            for(int i=0;i<livestreamsList.size();i++){
                LivestreamsResponse responseObject = new LivestreamsResponse();

                responseObject.setId(livestreamsList.get(i).getId());
                responseObject.setActive(livestreamsList.get(i).isActive());
                responseObject.setDescription(livestreamsList.get(i).getDescription());
                responseObject.setLive(livestreamsList.get(i).isLive());
                responseObject.setUrl(livestreamsList.get(i).getUrl());

                Tournaments tournaments = tournamentsRepository.findById(livestreamsList.get(i).getTournamentId());
                responseObject.setTournament(tournaments);

                livestreamsResponses.add(responseObject);
            }

            return new ResponseEntity<Object>(livestreamsResponses, HttpStatus.OK);


        }catch (Exception e){

            return new ResponseEntity<Object>(e, HttpStatus.INTERNAL_SERVER_ERROR);      //Catch exceptions and return failure response with exception

        }

    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getLivestreamsByTournament(@RequestParam int tournamentId, @RequestParam(required = false) Integer liveNow){

        try{
            List<Livestreams> livestreamsList = new ArrayList<>();
            List<LivestreamsResponse> livestreamsResponses = new ArrayList<>();

            if(tournamentId==0){

                List<Livestreams> allStreams = new ArrayList<>();
                List<Livestreams> allLiveStreams = livestreamsRepository.findAll();

                if(liveNow==null) {

                    for(int i=0;i<allLiveStreams.size(); i++){
                        if(allLiveStreams.get(i).isActive()){
                            allStreams.add(allLiveStreams.get(i));
                        }
                    }
                }else{
                    if(liveNow==1){

                        for(int i=0;i<allLiveStreams.size(); i++){
                            if(allLiveStreams.get(i).isLive()&&allLiveStreams.get(i).isActive()){
                                allStreams.add(allLiveStreams.get(i));
                            }
                        }
                    }else{
                        for(int i=0;i<allLiveStreams.size(); i++){
                            if(allLiveStreams.get(i).isLive()==false&&allLiveStreams.get(i).isActive()){
                                allStreams.add(allLiveStreams.get(i));
                            }
                        }
                    }
                }

                for(int i= 0; i<allStreams.size(); i++){
                        livestreamsList.add(allStreams.get(i));
                }

            }else {

                Tournaments tournament = tournamentsRepository.findById(tournamentId);
                if (tournament == null) {
                    return new ResponseEntity<Object>("Tournament not found", HttpStatus.NOT_FOUND);
                }

                List<Livestreams> allFromTournament = livestreamsRepository.findAllByTournamentId(tournamentId);

                if(liveNow==null) {

                    livestreamsList = allFromTournament;
                }else if(liveNow==1){

                    for(int i=0; i<allFromTournament.size(); i++){
                        if(allFromTournament.get(i).isLive()&&allFromTournament.get(i).isActive()){
                            livestreamsList.add(allFromTournament.get(i));
                        }
                    }
                }else if(liveNow==0){
                    for(int i=0; i<allFromTournament.size(); i++){
                        if(allFromTournament.get(i).isLive()==false&&allFromTournament.get(i).isActive()){
                            livestreamsList.add(allFromTournament.get(i));
                        }
                    }
                }
            }

            for(int i=0;i<livestreamsList.size();i++){
                LivestreamsResponse responseObject = new LivestreamsResponse();

                responseObject.setId(livestreamsList.get(i).getId());
                responseObject.setActive(livestreamsList.get(i).isActive());
                responseObject.setDescription(livestreamsList.get(i).getDescription());
                responseObject.setLive(livestreamsList.get(i).isLive());
                responseObject.setUrl(livestreamsList.get(i).getUrl());

                Tournaments tournaments = tournamentsRepository.findById(livestreamsList.get(i).getTournamentId());
                responseObject.setTournament(tournaments);

                livestreamsResponses.add(responseObject);
            }

            return new ResponseEntity<Object>(livestreamsResponses, HttpStatus.OK);


        }catch (Exception e){

            return new ResponseEntity<Object>(e, HttpStatus.INTERNAL_SERVER_ERROR);      //Catch exceptions and return failure response with exception

        }

    }


}
