package anuda.me.saisa_live.controllers;

import anuda.me.saisa_live.entities.Accesscodes;
import anuda.me.saisa_live.helpers.AuthHelper;
import anuda.me.saisa_live.repositories.AccessRepository;
import anuda.me.saisa_live.repositories.AdminRepository;
import anuda.me.saisa_live.requests.AccessCodeRequest;
import anuda.me.saisa_live.requests.AuthRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "access-codes")
public class AccessCodesController {

    @Autowired
    private AccessRepository accessRepository;

    @Autowired
    private AdminRepository adminRepository;

    @RequestMapping(value = "get", method = RequestMethod.POST)
    public ResponseEntity<?>getAccessCodes(@RequestBody AuthRequest request){

        if(!(new AuthHelper().checkCredentials(request.getUsername(), request.getPassword(), true, adminRepository))){
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

        List<Accesscodes> accessCodes = accessRepository.findAll();

        return new ResponseEntity<Object>(accessCodes, HttpStatus.OK);

    }

    @RequestMapping(value = "new", method = RequestMethod.POST)
    public ResponseEntity<?>createNewAccessCode(@RequestBody AccessCodeRequest request){
        if(!(new AuthHelper().checkCredentials(request.getUsername(), request.getPassword(), true, adminRepository))){
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

        if(accessRepository.findByAccessCode(request.getAccessCode())!=null){
            return new ResponseEntity<Object>("Access code already exists. Choose a different one.", HttpStatus.BAD_REQUEST);
        }

        Accesscodes accesscodesObject = new Accesscodes();

        accesscodesObject.setAccessCode(request.getAccessCode());
        accesscodesObject.setActive(request.isActive());
        accesscodesObject.setGroupName(request.getGroupName());

        accessRepository.save(accesscodesObject);

        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public ResponseEntity<?>editExistingAccessCode(@RequestBody AccessCodeRequest request, @RequestParam int id){
        if(!(new AuthHelper().checkCredentials(request.getUsername(), request.getPassword(), true, adminRepository))){
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

        Accesscodes accesscodesObject = accessRepository.findById(id);

        Accesscodes duplicate = accessRepository.findByAccessCode(request.getAccessCode());

        if(accesscodesObject ==null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        if(duplicate!=null){
            if(duplicate.getId()!=accesscodesObject.getId()) {
                return new ResponseEntity<Object>("Access code already exists. Choose a different one.", HttpStatus.BAD_REQUEST);
            }
        }
        accesscodesObject.setAccessCode(request.getAccessCode());
        accesscodesObject.setActive(request.isActive());
        accesscodesObject.setGroupName(request.getGroupName());

        accessRepository.save(accesscodesObject);

        return new ResponseEntity(HttpStatus.OK);
    }

}
