package anuda.me.saisa_live.controllers;


import anuda.me.saisa_live.entities.Followers;
import anuda.me.saisa_live.entities.Users;
import anuda.me.saisa_live.repositories.FollowersRepository;
import anuda.me.saisa_live.repositories.TeamsRepository;
import anuda.me.saisa_live.repositories.UsersRepository;
import anuda.me.saisa_live.requests.FollowRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "users")
public class UsersController {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private FollowersRepository followersRepository;

    @Autowired
    private TeamsRepository teamsRepository;

    @RequestMapping(value = "new", method = RequestMethod.GET)
    public ResponseEntity<?> addNewUser(@RequestParam String deviceName){

        try{

            Users newUser = new Users();

            newUser.setDevice(deviceName);

            newUser = usersRepository.save(newUser);

            return new ResponseEntity<Object>(newUser.getId(), HttpStatus.OK);


        } catch (Exception e) {
        return new ResponseEntity<Object>(e, HttpStatus.INTERNAL_SERVER_ERROR);      //Catch exceptions and return failure response with exception
        }

    }

    @RequestMapping(value = "follow", method = RequestMethod.POST)
    public ResponseEntity<?> followTeams(@RequestBody FollowRequest request){

        try{

            Users user = usersRepository.findById(request.getUserId());
            if(user==null){
                return new ResponseEntity<Object>("User not found.", HttpStatus.NOT_FOUND);
            }

            List<Integer> followingTeams = request.getFollowingList();

            for(int i = 0; i<followingTeams.size(); i++){
                Followers follower = new Followers();
                follower.setTeamId(followingTeams.get(i));
                follower.setUserId(request.getUserId());

                followersRepository.save(follower);
            }

            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Object>(e, HttpStatus.INTERNAL_SERVER_ERROR);      //Catch exceptions and return failure response with exception
        }
    }

}
