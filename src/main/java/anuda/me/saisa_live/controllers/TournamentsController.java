package anuda.me.saisa_live.controllers;


import anuda.me.saisa_live.entities.Participants;
import anuda.me.saisa_live.entities.Teams;
import anuda.me.saisa_live.entities.Tournaments;
import anuda.me.saisa_live.helpers.AuthHelper;
import anuda.me.saisa_live.helpers.ObjectSettingHelper;
import anuda.me.saisa_live.repositories.*;
import anuda.me.saisa_live.requests.EditParticipantRequest;
import anuda.me.saisa_live.requests.ParticipantRequest;
import anuda.me.saisa_live.requests.TournamentRequest;
import anuda.me.saisa_live.responses.ParticipantResponse;
import anuda.me.saisa_live.responses.Pools;
import anuda.me.saisa_live.responses.TournamentParticipantsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("tournaments")            //Methods in this file will be at /tournaments endpoint
public class TournamentsController {

    @Autowired
    private TournamentsRepository tournamentsRepository;       //Connecting to repository to be able to access Tournaments table

    @Autowired
    private ParticipantsRepository participantsRepository;       //Connecting to repository to be able to access participants table

    @Autowired
    private TeamsRepository teamsRepository;       //Connecting to repository to be able to access teams table

    @Autowired
    private SportsRepository sportsRepository;      //Connection to repository to be able to access sports table

    @Autowired
    private AdminRepository adminRepository;

    @RequestMapping(value = "", method = RequestMethod.GET)                  //GET method to get tournament data
    public ResponseEntity<?> getTournamentDetails(@RequestParam int tournamentId){      //This method requests the tournamentId as a URL parameter

        try{
            if(tournamentId == 0){
                List<Tournaments> tournamentList = tournamentsRepository.findAllByIsActive(true);        //If the tournamentId requested is 0, return a list of all active tournaments
                return new ResponseEntity(tournamentList, HttpStatus.OK);
            }else{
                Tournaments tournament = tournamentsRepository.findById(tournamentId);      //Find the tournament with the required ID from the database
                if(tournament==null){
                    return new ResponseEntity(HttpStatus.NOT_FOUND);        //If there is no record in DB with selected id, return not found response

                }else {
                    return new ResponseEntity(tournament, HttpStatus.OK);   //Return tournament data with success response
                }
            }
        }catch (Exception e){
            return new ResponseEntity(e, HttpStatus.INTERNAL_SERVER_ERROR);     //Catch exceptions and return failure response with exception
        }
    }

    @RequestMapping(value = "inactive", method = RequestMethod.GET)                  //GET method to get tournament data
    public ResponseEntity<?> getTournamentDetails(){      //This method requests the tournamentId as a URL parameter

        try{
                List<Tournaments> tournamentList = tournamentsRepository.findAllByIsActive(false);        //If the tournamentId requested is 0, return a list of all active tournaments
                return new ResponseEntity(tournamentList, HttpStatus.OK);

        }catch (Exception e){
            return new ResponseEntity(e, HttpStatus.INTERNAL_SERVER_ERROR);     //Catch exceptions and return failure response with exception
        }
    }

    @RequestMapping(value = "new", method = RequestMethod.POST)             //POST method to add new tournaments
    public ResponseEntity<?> createNewTournament(@RequestBody TournamentRequest request){       //This method requests a TournamentRequest object

        try{
            if(!(new AuthHelper().checkCredentials(request.getUsername(), request.getPassword(), false, adminRepository))){
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
            Tournaments newTournament = new Tournaments();              //Create new tournaments object

            if(sportsRepository.findById(request.getSportId())==null){
                return new ResponseEntity<Object>("Sport not found.", HttpStatus.NOT_FOUND);        //Check if sport with same ID exists in sports table and return not found error if it doesnt exist
            }

            newTournament = new ObjectSettingHelper().createTournamentsEntity(request, newTournament);      //Using ObjectSettingHelper class to populate the tournaments entity with data from the request object
            tournamentsRepository.save(newTournament);         //Save Tournaments Object to database
            return new ResponseEntity(HttpStatus.OK);          //return success response

        }catch(Exception e){
            return new ResponseEntity(e, HttpStatus.INTERNAL_SERVER_ERROR);     //Catch exceptions and return failure response with exception
        }

    }

    @RequestMapping(value = "edit", method = RequestMethod.POST)        //POST method to edit existing tournaments
    public ResponseEntity<?> editExistingTournament(@RequestBody TournamentRequest request, @RequestParam int tournamentId){     //This method requests a the TournamentRequest object and the tournamentId as a URL parameter

        try{
            if(!(new AuthHelper().checkCredentials(request.getUsername(), request.getPassword(), false, adminRepository))){
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
            Tournaments tournament = tournamentsRepository.findById(tournamentId);      //Get Tournaments object with same id from DB
            if(sportsRepository.findById(request.getSportId())==null){
                return new ResponseEntity<Object>("Sport not found.", HttpStatus.NOT_FOUND);        //Check if sport with same ID exists in sports table and return not found error if it doesnt exist
            }
            if(tournament==null){
                return new ResponseEntity(HttpStatus.NOT_FOUND);        //If tournament does not exist return a not found error
            }

            tournament = new ObjectSettingHelper().createTournamentsEntity(request, tournament);  //Using ObjectSettingHelper class to populate the tournaments entity with data from the request object

            tournamentsRepository.save(tournament);         //Save the tournament object to database
            return new ResponseEntity(HttpStatus.OK);       //return success response

        }catch (Exception e){
            return new ResponseEntity(e, HttpStatus.INTERNAL_SERVER_ERROR);     //Catch exceptions and return failure response with exception
        }

    }

    @RequestMapping(value = "status", method = RequestMethod.POST)          //POST method to change tournament status
    public ResponseEntity<?> changeTournamentStatus(@RequestParam int tournamentId, @RequestParam boolean newStatus){       //This method requests a tournamentId, and the newStatus as URL parameters

        try{

            Tournaments tournament = tournamentsRepository.findById(tournamentId);      //Get tournament object with the same ID from DB
            if(tournament==null){
                return new ResponseEntity(HttpStatus.NOT_FOUND);            //If tournament does not exist return a not found error
            }

            tournament.setActive(newStatus);        //Changing tournament active status to new status defined in the URL parameter

            tournamentsRepository.save(tournament);     //Save tournament in DB and return success response
            return new ResponseEntity(HttpStatus.OK);
        }catch(Exception e){

            return new ResponseEntity(e,HttpStatus.INTERNAL_SERVER_ERROR);      //Catch exceptions and return failure response with exception

        }
    }

    @RequestMapping(value = "participant/new", method = RequestMethod.POST)         //POST method to add participants to a tournament
    public ResponseEntity<?> addParticipantToExistingTournament(@RequestBody ParticipantRequest request){       //This method requests a ParticipantRequest object

        try{
            if(!(new AuthHelper().checkCredentials(request.getUsername(), request.getPassword(), false, adminRepository))){
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
            Participants existingParticipant = participantsRepository.findByTournamentIdAndTeamId(request.getTournamentId(), request.getTeamId());      //Check if there are any participants with the same team and tournament IDs
            if(existingParticipant!=null){
                if(existingParticipant.isActive()) {
                    return new ResponseEntity("This participant already exists in this tournament!", HttpStatus.BAD_REQUEST);       //If there is a participant and they are active, returns an error because cannot have duplicate participants.
                }
            }

            Tournaments tournament = tournamentsRepository.findById(request.getTournamentId());         //Gets tournament object with same tournament ID from DB
            Teams team = teamsRepository.findById(request.getTeamId());                                 //Gets team object with the same team ID from DB

            if(team == null||tournament==null){
                return new ResponseEntity("Team/Tournament not Found!", HttpStatus.NOT_FOUND);    //If team, or tournament is not found, return not found error
            }

            if(!tournament.getPoolsActive()){
                if(request.getPool()!=0){
                    return new ResponseEntity("Pools are not activated for this tournament", HttpStatus.BAD_REQUEST);       //Verifying that the pool is 0 if pools are not active for this tournament
                }
            }
            if(!tournament.getStandingsActive()){
                if(request.getStanding()!=0){
                    return new ResponseEntity("Standings are not activated for this tournament", HttpStatus.BAD_REQUEST);   //Verifying that the standing is 0 if standings are not active for this tournament
                }
            }
            if(!tournament.getScoresActive()){
                if(request.getLosses()!=0||request.getWins()!=0||request.getTies()!=0||request.getGames()!=0||request.getPoints()!=0){
                    return new ResponseEntity("Standings are not activated for this tournament", HttpStatus.BAD_REQUEST);      //Verifying that all score related fields are 0 if scores are inactive for this tournament
                }
            }

            if(request.getPool()>tournament.getPoolQuantity()){
                return new ResponseEntity("An invalid pool has been selected. There are only "+tournament.getPoolQuantity()+" pools setup for this tournament", HttpStatus.BAD_REQUEST);        //Verifying that the pool is valid based on the quantity of pools set for the tournament
            }

            Participants participant = new Participants();              //Creating new Participants object
            participant = new ObjectSettingHelper().createParticipantsEntity(request, participant);     //Using ObjectSettingHelper to populate a participant entity with data from the request

            participantsRepository.save(participant);
            return new ResponseEntity(HttpStatus.OK);                  //Save participant object to database and return success response
        }catch (Exception e){
            return new ResponseEntity(e, HttpStatus.INTERNAL_SERVER_ERROR);      //Catch exceptions and return failure response with exception
        }
    }

    @RequestMapping(value = "participant/edit", method = RequestMethod.POST)      //POST method to edit participants from tournament
    public ResponseEntity<?> changeParticipantStatus(@RequestParam int participantId, @RequestBody EditParticipantRequest request){     //This method requests the participantId as a URl parameter and the EditParticipantRequest in the request body

        try{
            if(!(new AuthHelper().checkCredentials(request.getUsername(), request.getPassword(), false, adminRepository))){
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
            Participants participant = participantsRepository.findById(participantId);              //Get participant object from DB with the same id

            if(participant==null){
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);      //Send not found if participant does not exist in DB
            }

            participant.setActive(request.isActive());      //Change active status to value in request

            Tournaments tournament = tournamentsRepository.findById(participant.getTournamentId());     //Get tournament record with same tournament id from DB

            if(tournament.getStandingsActive()) {       //If tournament has standings activated
                participant.setPoints(request.getPoints());
                participant.setStanding(request.getStanding());     //Change points and standing fields to new values in request
            }

            if(tournament.getScoresActive()) {       //If tournament has scores activated
                participant.setWins(request.getWins());
                participant.setGames(request.getGames());
                participant.setLosses(request.getLosses());
                participant.setTies(request.getTies());     //Change wins, games, losses, and ties fields to new values in request
            }

            participantsRepository.save(participant);
            return new ResponseEntity(HttpStatus.OK);       //Save edited participant object in DB and return success response

        }catch(Exception e){
            return new ResponseEntity<Object>(e, HttpStatus.INTERNAL_SERVER_ERROR);      //Catch exceptions and return failure response with exception

        }
    }

    @RequestMapping(value = "participants/", method = RequestMethod.GET)            //Get method to get the participants of a tournament
    public ResponseEntity<?> getParticipantsByTournament(@RequestParam(required = false) Integer tournamentId, @RequestParam(required = false) Integer participantId, @RequestParam(required = false) boolean active){       //This method requires the tournamentId as a URL parameter


        try{
            if(tournamentId==null&&participantId==null){
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }

            if(participantId!=null){
                Participants participantEntity = participantsRepository.findById(participantId);

                if(participantEntity==null){
                    return new ResponseEntity(HttpStatus.NOT_FOUND);
                }

                ParticipantResponse participantResponse = new ParticipantResponse();

                participantResponse.setId(participantEntity.getId());
                participantResponse.setActive(participantEntity.isActive());
                participantResponse.setGames(participantEntity.getGames());
                participantResponse.setLosses(participantEntity.getLosses());
                participantResponse.setPoints(participantEntity.getPoints());
                participantResponse.setPool(participantEntity.getPool());
                participantResponse.setStanding(participantEntity.getStanding());
                participantResponse.setTeamPhoto(participantEntity.getTeamPhoto());
                participantResponse.setTies(participantEntity.getTies());
                participantResponse.setWins(participantEntity.getWins());       //Populate data in participant response object from participant entity

                Teams team = teamsRepository.findById(participantEntity.getTeamId());
                participantResponse.setTeam(team);      //Get team data from the DB using the team ID and add to participant response object

                return new ResponseEntity<Object>(participantResponse, HttpStatus.OK);
            }
            Tournaments tournament = tournamentsRepository.findById(tournamentId);      //get Tournament record from FB using tournament id

            if(tournament == null){
                return new ResponseEntity(HttpStatus.NOT_FOUND);        //If tournament doesn't exist in DB returns not found error
            }

            TournamentParticipantsResponse participantsResponse = new TournamentParticipantsResponse();     //Create new response object

            participantsResponse.setActive(tournament.getActive());
            participantsResponse.setEndDate(tournament.getEndDate());
            participantsResponse.setLocation(tournament.getLocation());
            participantsResponse.setLogo(tournament.getLogo());
            participantsResponse.setName(tournament.getName());
            participantsResponse.setPoolQuantity(tournament.getPoolQuantity());
            participantsResponse.setPoolsActive(tournament.getPoolsActive());
            participantsResponse.setMeetsActive(tournament.isMeetsActive());
            participantsResponse.setScoresActive(tournament.getScoresActive());
            participantsResponse.setSportId(tournament.getSportId());
            participantsResponse.setStandingsActive(tournament.getStandingsActive());
            participantsResponse.setStartDate(tournament.getStartDate());
            participantsResponse.setUrl(tournament.getUrl());           //Add all tournament details to the response object from the tournament object

            List<Pools> poolsList = new ArrayList<Pools>();     //Create new list for pools

            if(!active) {
                if (!tournament.getPoolsActive()) {           //Check if tournament has pools deactivated

                    List<Participants> participantEntitiesList = participantsRepository.findAllByTournamentId(tournamentId);   //Get a list of all participants in the tournament from the database
                    List<ParticipantResponse> participantResponses = new ArrayList<>();     //Create new list of participant response objects
                    for (int j = 0; j < participantEntitiesList.size(); j++) {      //Loop through object in the participantEntities list
                        ParticipantResponse participantResponse = new ParticipantResponse();//create new participant response object
                        Participants participantEntity = participantEntitiesList.get(j);
                        participantResponse.setId(participantEntity.getId());
                        participantResponse.setActive(participantEntity.isActive());
                        participantResponse.setGames(participantEntity.getGames());
                        participantResponse.setLosses(participantEntity.getLosses());
                        participantResponse.setPoints(participantEntity.getPoints());
                        participantResponse.setPool(participantEntity.getPool());
                        participantResponse.setStanding(participantEntity.getStanding());
                        participantResponse.setTeamPhoto(participantEntity.getTeamPhoto());
                        participantResponse.setTies(participantEntity.getTies());
                        participantResponse.setWins(participantEntity.getWins());       //Populate data in participant response object from participant entity

                        Teams team = teamsRepository.findById(participantEntity.getTeamId());
                        participantResponse.setTeam(team);      //Get team data from the DB using the team ID and add to participant response object

                        participantResponses.add(participantResponse);      //Add participant response object to participant responses list
                    }
                    Pools pool = new Pools();       //Create new Pools object
                    pool.setPool(0);
                    pool.setParticipants(participantResponses);     //Since pools are inactive, set pool to 0 and add all tournament participants into the participants list
                    poolsList.add(pool);    //Add this pools object to the pools list
                }
                else {
                    int poolQuantity = tournament.getPoolQuantity();    //Get the number of pools from the tournament object

                    for (int i = 0; i < poolQuantity; i++) {        //for loop that will iterate once each for the number of pools
                        int pool = i + 1;     //Since i starts at 0, set the pool to a positive whole number i+1

                        List<Participants> participantEntitiesList = participantsRepository.findAllByTournamentIdAndPool(tournamentId, pool);      //get a list of all participants from the same tournament and pool from the DB
                        List<ParticipantResponse> participantResponses = new ArrayList<>();     //Create new list of participant response objects
                        for (int j = 0; j < participantEntitiesList.size(); j++) {      //Loop through object in the participantEntities list
                            ParticipantResponse participantResponse = new ParticipantResponse();        //create new participant response object
                            Participants participantEntity = participantEntitiesList.get(j);
                            participantResponse.setId(participantEntity.getId());

                            participantResponse.setActive(participantEntity.isActive());
                            participantResponse.setGames(participantEntity.getGames());
                            participantResponse.setLosses(participantEntity.getLosses());
                            participantResponse.setPoints(participantEntity.getPoints());
                            participantResponse.setPool(participantEntity.getPool());
                            participantResponse.setStanding(participantEntity.getStanding());
                            participantResponse.setTeamPhoto(participantEntity.getTeamPhoto());
                            participantResponse.setTies(participantEntity.getTies());
                            participantResponse.setWins(participantEntity.getWins());       //Populate data in participant response object from participant entity

                            Teams team = teamsRepository.findById(participantEntity.getTeamId());
                            participantResponse.setTeam(team);      //Get team data from the DB using the team ID and add to participant response object

                            participantResponses.add(participantResponse);      //Add participant response object to participant responses list
                        }
                        Pools poolObject = new Pools();     //Create new pools object

                        poolObject.setParticipants(participantResponses);
                        poolObject.setPool(pool);       //Add pool data to pools object
                        poolsList.add(poolObject);      //Add pool object to pool list
                    }
                }
            }else{
                if (!tournament.getPoolsActive()) {           //Check if tournament has pools deactivated

                    List<Participants> participantEntitiesList1 = participantsRepository.findAllByTournamentId(tournamentId);   //Get a list of all participants in the tournament from the database
                    List<Participants> participantEntitiesList=new ArrayList<>();

                    for(int k=0;k<participantEntitiesList1.size();k++){
                        if(participantEntitiesList1.get(k).isActive()){
                            participantEntitiesList.add(participantEntitiesList1.get(k));
                        }
                    }

                    List<ParticipantResponse> participantResponses = new ArrayList<>();     //Create new list of participant response objects
                    for (int j = 0; j < participantEntitiesList.size(); j++) {      //Loop through object in the participantEntities list
                        ParticipantResponse participantResponse = new ParticipantResponse();//create new participant response object
                        Participants participantEntity = participantEntitiesList.get(j);
                        participantResponse.setId(participantEntity.getId());
                        participantResponse.setActive(participantEntity.isActive());
                        participantResponse.setGames(participantEntity.getGames());
                        participantResponse.setLosses(participantEntity.getLosses());
                        participantResponse.setPoints(participantEntity.getPoints());
                        participantResponse.setPool(participantEntity.getPool());
                        participantResponse.setStanding(participantEntity.getStanding());
                        participantResponse.setTeamPhoto(participantEntity.getTeamPhoto());
                        participantResponse.setTies(participantEntity.getTies());
                        participantResponse.setWins(participantEntity.getWins());       //Populate data in participant response object from participant entity

                        Teams team = teamsRepository.findById(participantEntity.getTeamId());
                        participantResponse.setTeam(team);      //Get team data from the DB using the team ID and add to participant response object

                        participantResponses.add(participantResponse);      //Add participant response object to participant responses list
                    }
                    Pools pool = new Pools();       //Create new Pools object
                    pool.setPool(0);
                    pool.setParticipants(participantResponses);     //Since pools are inactive, set pool to 0 and add all tournament participants into the participants list
                    poolsList.add(pool);    //Add this pools object to the pools list
                }
                else {
                    int poolQuantity = tournament.getPoolQuantity();    //Get the number of pools from the tournament object

                    for (int i = 0; i < poolQuantity; i++) {        //for loop that will iterate once each for the number of pools
                        int pool = i + 1;     //Since i starts at 0, set the pool to a positive whole number i+1

                        List<Participants> participantEntitiesList1 = participantsRepository.findAllByTournamentIdAndPool(tournamentId, pool);      //get a list of all participants from the same tournament and pool from the DB
                        List<Participants> participantEntitiesList=new ArrayList<>();

                        for(int k=0;k<participantEntitiesList1.size();k++){
                            if(participantEntitiesList1.get(k).isActive()){
                                participantEntitiesList.add(participantEntitiesList1.get(k));
                            }
                        }

                        List<ParticipantResponse> participantResponses = new ArrayList<>();     //Create new list of participant response objects
                        for (int j = 0; j < participantEntitiesList.size(); j++) {      //Loop through object in the participantEntities list
                            ParticipantResponse participantResponse = new ParticipantResponse();        //create new participant response object
                            Participants participantEntity = participantEntitiesList.get(j);
                            participantResponse.setId(participantEntity.getId());

                            participantResponse.setActive(participantEntity.isActive());
                            participantResponse.setGames(participantEntity.getGames());
                            participantResponse.setLosses(participantEntity.getLosses());
                            participantResponse.setPoints(participantEntity.getPoints());
                            participantResponse.setPool(participantEntity.getPool());
                            participantResponse.setStanding(participantEntity.getStanding());
                            participantResponse.setTeamPhoto(participantEntity.getTeamPhoto());
                            participantResponse.setTies(participantEntity.getTies());
                            participantResponse.setWins(participantEntity.getWins());       //Populate data in participant response object from participant entity

                            Teams team = teamsRepository.findById(participantEntity.getTeamId());
                            participantResponse.setTeam(team);      //Get team data from the DB using the team ID and add to participant response object

                            participantResponses.add(participantResponse);      //Add participant response object to participant responses list
                        }
                        Pools poolObject = new Pools();     //Create new pools object

                        poolObject.setParticipants(participantResponses);
                        poolObject.setPool(pool);       //Add pool data to pools object
                        poolsList.add(poolObject);      //Add pool object to pool list
                    }
                }
            }
            participantsResponse.setPools(poolsList);       //Add pools list to the response object

            return new ResponseEntity<Object>(participantsResponse, HttpStatus.OK);     //return the response object with a success response

        }catch(Exception e){
            return new ResponseEntity<Object>(e, HttpStatus.INTERNAL_SERVER_ERROR);      //Catch exceptions and return failure response with exception
        }

    }

    @RequestMapping(value = "participants/minified", method = RequestMethod.GET)
    public ResponseEntity<?> getMinifiedParticipantByTournament(@RequestParam(required = false) Integer tournamentId){

        try{

            List<Participants> participantsList = participantsRepository.findAllByTournamentId(tournamentId);

            List<ParticipantResponse> participantResponses = new ArrayList<>();

            for(int i = 0; i<participantsList.size(); i++){

                ParticipantResponse participantResponse = new ParticipantResponse();
                Participants participantEntity = participantsList.get(i);

                participantResponse.setId(participantEntity.getId());
                participantResponse.setActive(participantEntity.isActive());
                participantResponse.setGames(participantEntity.getGames());
                participantResponse.setLosses(participantEntity.getLosses());
                participantResponse.setPoints(participantEntity.getPoints());
                participantResponse.setPool(participantEntity.getPool());
                participantResponse.setStanding(participantEntity.getStanding());
                participantResponse.setTeamPhoto(participantEntity.getTeamPhoto());
                participantResponse.setTies(participantEntity.getTies());
                participantResponse.setWins(participantEntity.getWins());       //Populate data in participant response object from participant entity

                Teams team = teamsRepository.findById(participantEntity.getTeamId());
                participantResponse.setTeam(team);      //Get team data from the DB using the team ID and add to participant response object

                participantResponses.add(participantResponse);
            }

            return new ResponseEntity<Object>(participantResponses, HttpStatus.OK);

        }catch(Exception e){
            return new ResponseEntity<Object>(e, HttpStatus.INTERNAL_SERVER_ERROR);      //Catch exceptions and return failure response with exception
        }

    }
}
