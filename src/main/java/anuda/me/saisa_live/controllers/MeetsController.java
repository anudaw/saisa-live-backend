package anuda.me.saisa_live.controllers;


import anuda.me.saisa_live.entities.Livestreams;
import anuda.me.saisa_live.entities.Meets;
import anuda.me.saisa_live.entities.Participants;
import anuda.me.saisa_live.entities.Tournaments;
import anuda.me.saisa_live.helpers.AuthHelper;
import anuda.me.saisa_live.helpers.ObjectSettingHelper;
import anuda.me.saisa_live.repositories.*;
import anuda.me.saisa_live.requests.AuthRequest;
import anuda.me.saisa_live.requests.MeetsRequest;
import anuda.me.saisa_live.requests.MeetsResultsRequest;
import anuda.me.saisa_live.responses.MeetsResponse;
import anuda.me.saisa_live.responses.ParticipantResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@RestController
@RequestMapping(value = "meets")
public class MeetsController {

    @Autowired
    private MeetsRepository meetsRepository;

    @Autowired
    private TournamentsRepository tournamentsRepository;

    @Autowired
    private ParticipantsRepository participantsRepository;

    @Autowired
    private LivestreamsRepository livestreamsRepository;

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private TeamsRepository teamsRepository;

    @RequestMapping(value = "new", method = RequestMethod.POST)
    public ResponseEntity<?> createNewMeetEvent(@RequestBody MeetsRequest request){


        try{

            if (!(new AuthHelper().checkCredentials(request.getUsername(), request.getPassword(), false, adminRepository))) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }

            Tournaments tournament = tournamentsRepository.findById(request.getTournamentId());
            if(tournament==null){
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }

            if(!tournament.isMeetsActive()){
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }

            if (request.getActiveStatus() != 0) {
                Livestreams livestream = livestreamsRepository.findById(request.getLivestreamId());
                if (livestream == null) {
                    return new ResponseEntity<Object>("Invalid Livestream. Please try again", HttpStatus.NOT_FOUND);
                }
            }

            if(request.getActiveStatus()>3){
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }

            Meets meets = new Meets();
            meets = new ObjectSettingHelper().createMeetsEntity(request, meets);

            meetsRepository.save(meets);

            return new ResponseEntity(HttpStatus.OK);


        }catch (Exception e){
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @RequestMapping(value ="edit", method = RequestMethod.POST)
    public ResponseEntity<?> editExistingMeetEvent(@RequestBody MeetsRequest request, @RequestParam int meetId){

        try{

            if (!(new AuthHelper().checkCredentials(request.getUsername(), request.getPassword(), false, adminRepository))) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }

            Meets meets = meetsRepository.findById(meetId);

            if(meets==null){
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }

            Tournaments tournament = tournamentsRepository.findById(request.getTournamentId());
            if(tournament==null){
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }

            if(!tournament.isMeetsActive()){
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }

            if (request.getActiveStatus() != 0) {
                Livestreams livestream = livestreamsRepository.findById(request.getLivestreamId());
                if (livestream == null) {
                    return new ResponseEntity<Object>("Invalid Livestream. Please try again", HttpStatus.NOT_FOUND);
                }
            }

            if(request.getActiveStatus()>3){
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }

            meets = new ObjectSettingHelper().createMeetsEntity(request, meets);

            meetsRepository.save(meets);

            return new ResponseEntity(HttpStatus.OK);


        }catch (Exception e){
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @RequestMapping(value = "status", method = RequestMethod.POST)
    public ResponseEntity<?> changeStatusOfMeetEvent(@RequestBody AuthRequest request, @RequestParam int meetId, @RequestParam int newStatus){

        try{

            if (!(new AuthHelper().checkCredentials(request.getUsername(), request.getPassword(), false, adminRepository))) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }

            Meets meets = meetsRepository.findById(meetId);

            if(meets==null){
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }

            if(newStatus==2){

                if(meets.getP1teamId()==0||meets.getP2teamId()==0||meets.getP3teamId()==0||meets.getP1result()==null||meets.getP2result()==null||meets.getP3result()==null||meets.getP1name()==null||meets.getP2name()==null||meets.getP3name()==null||meets.getResultUrl()==null){
                    return new ResponseEntity(HttpStatus.BAD_REQUEST);
                }

            }

            meets.setActiveStatus(newStatus);

            meetsRepository.save(meets);

            return new ResponseEntity(HttpStatus.OK);

        }catch (Exception e){
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }

    @RequestMapping(value = "results", method = RequestMethod.POST)
    public ResponseEntity<?> addResultsToMeetEvent(@RequestBody MeetsResultsRequest request){

        if (!(new AuthHelper().checkCredentials(request.getUsername(), request.getPassword(), false, adminRepository))) {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

        Meets meets = meetsRepository.findById(request.getMeetId());

        if(meets==null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        if(participantsRepository.findById(request.getP1teamId())==null||participantsRepository.findById(request.getP2teamId())==null||participantsRepository.findById(request.getP3teamId())==null){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        meets.setP1name(request.getP1name());
        meets.setP1record(request.isP1record());
        meets.setP1result(request.getP1result());
        meets.setP1teamId(request.getP1teamId());

        meets.setP2name(request.getP2name());
        meets.setP2record(request.isP2record());
        meets.setP2result(request.getP2result());
        meets.setP2teamId(request.getP2teamId());

        meets.setP3name(request.getP3name());
        meets.setP3record(request.isP3record());
        meets.setP3result(request.getP3result());
        meets.setP3teamId(request.getP3teamId());

        meets.setResultUrl(request.getResultUrl());

        if(meets.getActiveStatus()==1||meets.getActiveStatus()==0){
            meets.setActiveStatus(2);
        }

        meetsRepository.save(meets);

        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value="", method=RequestMethod.GET)
    public ResponseEntity<?> getMeetsEvents(@RequestParam(required = false)Integer tournamentId, @RequestParam(required = false)Integer activeStatus, @RequestParam(required = false)Integer meetId){

        try{

            List<Meets> meetsRecords = new ArrayList<>();

            if (meetId != null) {
                Meets meets = meetsRepository.findById(meetId);

                if (meets == null) {
                    return new ResponseEntity<Object>("Meet Event not found!", HttpStatus.NOT_FOUND);
                }
                meetsRecords.add(meets);
            } else if (tournamentId != null) {

                if (tournamentsRepository.findById(tournamentId) == null) {
                    return new ResponseEntity<Object>("Tournament not found!", HttpStatus.NOT_FOUND);
                }

                if (activeStatus != null) {
                    if (activeStatus > 3) {
                        return new ResponseEntity<Object>("Invalid Actives status", HttpStatus.BAD_REQUEST);
                    }
                    meetsRecords = meetsRepository.findAllByActiveStatusAndTournamentId(activeStatus, tournamentId);
                } else {
                    meetsRecords = meetsRepository.findAllByTournamentId(tournamentId);
                }
            } else if (activeStatus != null) {

                if (activeStatus > 3) {
                    return new ResponseEntity<Object>("Invalid Actives status", HttpStatus.BAD_REQUEST);
                }

                meetsRecords = meetsRepository.findAllByActiveStatus(activeStatus);
            } else {
                meetsRecords = meetsRepository.findAll();
            }

            List<MeetsResponse> meetsResponses = new ArrayList<>();

            for(int i=0;i<meetsRecords.size();i++){

                Meets meetRecord = meetsRecords.get(i);
                MeetsResponse meetsResponse = new MeetsResponse();

                meetsResponse.setId(meetRecord.getId());
                meetsResponse.setActiveStatus(meetRecord.getActiveStatus());
                meetsResponse.setDescription(meetRecord.getDescription());
                meetsResponse.setStartTime(meetRecord.getStartTime());

                if(meetsResponse.getActiveStatus()==2) {
                    meetsResponse.setResultUrl(meetRecord.getResultUrl());

                    meetsResponse.setP1name(meetRecord.getP1name());
                    meetsResponse.setP2name(meetRecord.getP2name());
                    meetsResponse.setP3name(meetRecord.getP3name());

                    meetsResponse.setP1result(meetRecord.getP1result());
                    meetsResponse.setP2result(meetRecord.getP3result());
                    meetsResponse.setP3result(meetRecord.getP3result());

                    meetsResponse.setP1record(meetRecord.isP1record());
                    meetsResponse.setP2record(meetRecord.isP2record());
                    meetsResponse.setP3record(meetRecord.isP3record());
                }


                Tournaments tournament = tournamentsRepository.findById(meetRecord.getTournamentId());
                meetsResponse.setTournament(tournament);

                Livestreams livestream = livestreamsRepository.findById(meetRecord.getLivestreamId());
                meetsResponse.setLivestream(livestream);

                if(meetsResponse.getActiveStatus()==2) {
                    ParticipantResponse p1Team = new ParticipantResponse();
                    Participants p1Entity = participantsRepository.findById(meetRecord.getP1teamId());

                    p1Team.setId(p1Entity.getId());
                    p1Team.setTeam(teamsRepository.findById(p1Entity.getTeamId()));
                    p1Team.setWins(p1Entity.getWins());
                    p1Team.setTies(p1Entity.getTies());
                    p1Team.setActive(p1Entity.isActive());
                    p1Team.setTeamPhoto(p1Entity.getTeamPhoto());
                    p1Team.setStanding(p1Entity.getStanding());
                    p1Team.setPool(p1Entity.getPool());
                    p1Team.setPoints(p1Entity.getPoints());
                    p1Team.setLosses(p1Entity.getLosses());
                    p1Team.setGames(p1Entity.getGames());

                    meetsResponse.setP1Team(p1Team);

                    ParticipantResponse p2Team = new ParticipantResponse();
                    Participants p2Entity = participantsRepository.findById(meetRecord.getP2teamId());

                    p2Team.setId(p2Entity.getId());
                    p2Team.setTeam(teamsRepository.findById(p2Entity.getTeamId()));
                    p2Team.setWins(p2Entity.getWins());
                    p2Team.setTies(p2Entity.getTies());
                    p2Team.setActive(p2Entity.isActive());
                    p2Team.setTeamPhoto(p2Entity.getTeamPhoto());
                    p2Team.setStanding(p2Entity.getStanding());
                    p2Team.setPool(p2Entity.getPool());
                    p2Team.setPoints(p2Entity.getPoints());
                    p2Team.setLosses(p2Entity.getLosses());
                    p2Team.setGames(p2Entity.getGames());

                    meetsResponse.setP2Team(p2Team);

                    ParticipantResponse p3Team = new ParticipantResponse();
                    Participants p3Entity = participantsRepository.findById(meetRecord.getP3teamId());

                    p3Team.setId(p3Entity.getId());
                    p3Team.setTeam(teamsRepository.findById(p3Entity.getTeamId()));
                    p3Team.setWins(p3Entity.getWins());
                    p3Team.setTies(p3Entity.getTies());
                    p3Team.setActive(p3Entity.isActive());
                    p3Team.setTeamPhoto(p3Entity.getTeamPhoto());
                    p3Team.setStanding(p3Entity.getStanding());
                    p3Team.setPool(p3Entity.getPool());
                    p3Team.setPoints(p3Entity.getPoints());
                    p3Team.setLosses(p3Entity.getLosses());
                    p3Team.setGames(p3Entity.getGames());

                    meetsResponse.setP3Team(p3Team);
                }


                meetsResponses.add(meetsResponse);
            }

            meetsResponses.sort(Comparator.comparing(e -> new Long(e.getStartTime())));


            return new ResponseEntity<Object>(meetsResponses, HttpStatus.OK);


        }catch(Exception e){
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
