package anuda.me.saisa_live.controllers;


import anuda.me.saisa_live.entities.Sports;
import anuda.me.saisa_live.helpers.AuthHelper;
import anuda.me.saisa_live.helpers.ObjectSettingHelper;
import anuda.me.saisa_live.repositories.AdminRepository;
import anuda.me.saisa_live.repositories.SportsRepository;
import anuda.me.saisa_live.requests.SportsRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "sports")
public class SportsController {

    @Autowired
    private SportsRepository sportsRepository;

    @Autowired
    private AdminRepository adminRepository;


    @RequestMapping(value = "new", method = RequestMethod.POST)
    public ResponseEntity<?> createNewSports(@RequestBody SportsRequest request){
        try{
            if(!(new AuthHelper().checkCredentials(request.getUsername(), request.getPassword(), false, adminRepository))){
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
            Sports sport = new Sports();
            sport = new ObjectSettingHelper().createSportsEntity(request, sport);

            sportsRepository.save(sport);
            return new ResponseEntity(HttpStatus.OK);

        }catch(Exception e){

            return new ResponseEntity(e, HttpStatus.INTERNAL_SERVER_ERROR);      //Catch exceptions and return failure response with exception

        }
    }

    @RequestMapping(value  = "edit", method = RequestMethod.POST)
    public ResponseEntity<?> editExistingSports(@RequestBody SportsRequest request, @RequestParam int sportId){

        try{
            if(!(new AuthHelper().checkCredentials(request.getUsername(), request.getPassword(), false, adminRepository))){
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
            Sports sport = sportsRepository.findById(sportId);

            if(sport==null){
                return new ResponseEntity<Object>("Sport not found in DB.", HttpStatus.NOT_FOUND);
            }

            sport = new ObjectSettingHelper().createSportsEntity(request, sport);

            sportsRepository.save(sport);
            return new ResponseEntity(HttpStatus.OK);

        }catch(Exception e){

            return new ResponseEntity(e, HttpStatus.INTERNAL_SERVER_ERROR);      //Catch exceptions and return failure response with exception

        }
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getSports(@RequestParam int sportId){

        try{

            if(sportId == 0){

                List<Sports> sportsList = sportsRepository.findAll();
                return new ResponseEntity<Object>( sportsList, HttpStatus.OK);

            }else{
                Sports sport = sportsRepository.findById(sportId);
                if(sport==null){
                    return new ResponseEntity<Object>("Sport not found in DB.", HttpStatus.NOT_FOUND);
                }
                return new ResponseEntity<Object>(sport, HttpStatus.OK);
            }

        }catch(Exception e) {

            return new ResponseEntity(e, HttpStatus.INTERNAL_SERVER_ERROR);      //Catch exceptions and return failure response with exception

        }

    }
}
