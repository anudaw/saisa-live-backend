package anuda.me.saisa_live.controllers;


import anuda.me.saisa_live.entities.Accesscodes;
import anuda.me.saisa_live.entities.Media;
import anuda.me.saisa_live.entities.Tournaments;
import anuda.me.saisa_live.helpers.AuthHelper;
import anuda.me.saisa_live.helpers.ObjectSettingHelper;
import anuda.me.saisa_live.repositories.AccessRepository;
import anuda.me.saisa_live.repositories.AdminRepository;
import anuda.me.saisa_live.repositories.MediaRepository;
import anuda.me.saisa_live.repositories.TournamentsRepository;
import anuda.me.saisa_live.requests.MediaRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "media")
public class MediaController {

    @Autowired
    private MediaRepository mediaRepository;

    @Autowired
    private TournamentsRepository tournamentsRepository;

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private AccessRepository accessRepository;

    @RequestMapping(value = "new", method = RequestMethod.POST)
    public ResponseEntity<?> addNewMedia(@RequestBody MediaRequest request) {

        try {
            if(!(new AuthHelper().checkCredentials(request.getUsername(), request.getPassword(), false, adminRepository))){
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
            Tournaments tournament = tournamentsRepository.findById(request.getTournamentId());

            if(tournament==null){
                return new ResponseEntity<Object>("Tournament not found",HttpStatus.NOT_FOUND);
            }

            Media media = new Media();

            media  = new ObjectSettingHelper().createMediaEntity(request, media);

            String unixTime = String.valueOf(System.currentTimeMillis() / 1000L);
            media.setTimestamp(unixTime);

            mediaRepository.save(media);
            return new ResponseEntity(HttpStatus.OK);


        } catch (Exception e) {
            return new ResponseEntity<Object>(e, HttpStatus.INTERNAL_SERVER_ERROR);      //Catch exceptions and return failure response with exception
        }

    }

    @RequestMapping(value = "admin", method = RequestMethod.GET)
    public ResponseEntity<?> getMediaAdmin(@RequestParam int tournamentId, @RequestParam int type, @RequestParam(required = false) Integer mediaId){

        if(mediaId!=null){
            Media media = mediaRepository.findById(mediaId);
            if(media==null){
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<Object>(media, HttpStatus.OK);
        }

        List<Media> mediaList = new ArrayList<>();
        List<Media> allMedia;
        if(tournamentId == 0){
            if(type == 0) {
                allMedia= mediaRepository.findAll();
            }else{
                allMedia = mediaRepository.findAllByType(type);
            }
            for(int i = 0; i<allMedia.size(); i++){

                if(allMedia.get(i).isActive()){
                    mediaList.add(allMedia.get(i));
                }
            }
        }else {

            Tournaments tournament = tournamentsRepository.findById(tournamentId);
            if (tournament == null) {
                return new ResponseEntity<Object>("Tournament not found", HttpStatus.NOT_FOUND);
            }

            if(type == 0){
                mediaList = mediaRepository.findAllByTournamentId(tournamentId);
            }else{
                mediaList = mediaRepository.findAllByTournamentIdAndType(tournamentId, type);
            }
        }
        return new ResponseEntity<Object>(mediaList, HttpStatus.OK);


    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getMedia(@RequestParam int tournamentId, @RequestParam int type, @RequestParam(required = false) Integer mediaId, @RequestParam(required = false) String accessCode){

        if(mediaId!=null){

            Media media = mediaRepository.findById(mediaId);
            if(media==null){
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<Object>(media, HttpStatus.OK);
        }

        List<Media> mediaList = new ArrayList<>();
        List<Media> allMedia;
        if(type==1){
            Accesscodes accesscodes = accessRepository.findByAccessCode(accessCode);
            if(accesscodes ==null){
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }

            if(!accesscodes.isActive()){
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);

            }
        }
        if(tournamentId == 0){
            if(type == 0) {
                allMedia= mediaRepository.findAll();
            }else{
                allMedia = mediaRepository.findAllByType(type);
            }

        }else {

            Tournaments tournament = tournamentsRepository.findById(tournamentId);
            if (tournament == null) {
                return new ResponseEntity<Object>("Tournament not found", HttpStatus.NOT_FOUND);
            }

            if(type == 0){
                allMedia = mediaRepository.findAllByTournamentId(tournamentId);
            }else{
                allMedia = mediaRepository.findAllByTournamentIdAndType(tournamentId, type);
            }
        }
        for(int i = 0; i<allMedia.size(); i++){

            if(allMedia.get(i).isActive()){
                mediaList.add(allMedia.get(i));
            }
        }
        return new ResponseEntity<Object>(mediaList, HttpStatus.OK);


    }

    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public ResponseEntity<?> editExistingMedia(@RequestBody MediaRequest request, @RequestParam int mediaId){
        try {
            if(!(new AuthHelper().checkCredentials(request.getUsername(), request.getPassword(), false, adminRepository))){
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
            Media media = mediaRepository.findById(mediaId);

            if(media == null){
                return new ResponseEntity<Object>("Media not found", HttpStatus.NOT_FOUND);
            }

            Tournaments tournament = tournamentsRepository.findById(request.getTournamentId());

            if(tournament==null){
                return new ResponseEntity<Object>("Tournament not found",HttpStatus.NOT_FOUND);
            }

            media = new ObjectSettingHelper().createMediaEntity(request, media);
            String unixTime = String.valueOf(System.currentTimeMillis() / 1000L);
            media.setTimestamp(unixTime);
            mediaRepository.save(media);
            return new ResponseEntity(HttpStatus.OK);


        } catch (Exception e) {
            return new ResponseEntity<Object>(e, HttpStatus.INTERNAL_SERVER_ERROR);      //Catch exceptions and return failure response with exception
        }
    }
}
