package anuda.me.saisa_live.controllers;

import anuda.me.saisa_live.entities.Admins;
import anuda.me.saisa_live.entities.Tournaments;
import anuda.me.saisa_live.helpers.AuthHelper;
import anuda.me.saisa_live.repositories.AdminRepository;
import anuda.me.saisa_live.repositories.TournamentsRepository;
import anuda.me.saisa_live.requests.AdminEditRequest;
import anuda.me.saisa_live.requests.AuthRequest;
import anuda.me.saisa_live.requests.LoginRequest;
import anuda.me.saisa_live.responses.AdminAccountResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "admin")
public class AdminController {

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private TournamentsRepository tournamentsRepository;

    @RequestMapping(value = "verify", method = RequestMethod.POST)
    public ResponseEntity<?> verifyAdminCredentials(@RequestBody LoginRequest request){

        try{
            Admins admin = adminRepository.findByUsername(request.getUsername());
            if(admin==null){
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);

            }
            if(admin.getPassword().equals(request.getPassword())&&admin.isActive()){

                Tournaments tournament = tournamentsRepository.findById(admin.getTournamentId());
                return new ResponseEntity<Object>(tournament, HttpStatus.OK);
            }else{
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }

        }catch (Exception e){
            return new ResponseEntity<Object>(e, HttpStatus.INTERNAL_SERVER_ERROR);      //Catch exceptions and return failure response with exception
        }

    }

    @RequestMapping(value = "new", method = RequestMethod.POST)
    public ResponseEntity<?> createNewAdminAccount(@RequestBody AdminEditRequest request){

        try{

            Admins creatorAccount = adminRepository.findByUsername(request.getCreatorUsername());

            if(creatorAccount==null){
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);

            }
            if(!creatorAccount.getPassword().equals(request.getCreatorPassword())||!creatorAccount.isActive()||creatorAccount.getTournamentId()!=0){
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
            if(adminRepository.findByUsername(request.getUsername())!=null){
                return new ResponseEntity<Object>("An account with this username already exists", HttpStatus.BAD_REQUEST);
            }
            if(tournamentsRepository.findById(request.getTournamentId())==null){
                return new ResponseEntity<Object>("There is no tournament with this ID. Create a new tournament before adding admin accounts", HttpStatus.NOT_FOUND);
            }
            Admins newAdmin = new Admins();

            newAdmin.setActive(request.isActive());
            newAdmin.setCreatorId(creatorAccount.getId());
            newAdmin.setPassword(request.getPassword());
            newAdmin.setTournamentId(request.getTournamentId());
            newAdmin.setUsername(request.getUsername());

            adminRepository.save(newAdmin);
            return new ResponseEntity(HttpStatus.OK);

        }catch (Exception e){
            return new ResponseEntity<Object>(e, HttpStatus.INTERNAL_SERVER_ERROR);      //Catch exceptions and return failure response with exception
        }
    }

    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public ResponseEntity<?> editExistingAdminAccount(@RequestBody AdminEditRequest request){

        try{

            Admins creatorAccount = adminRepository.findByUsername(request.getCreatorUsername());
            if(creatorAccount==null){
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);

            }
            if(!creatorAccount.getPassword().equals(request.getCreatorPassword())||!creatorAccount.isActive()||creatorAccount.getTournamentId()!=0){
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }

            Admins admin = adminRepository.findByUsername(request.getUsername());

            if(admin==null){
                return new ResponseEntity<Object>("There is no account with this username.", HttpStatus.NOT_FOUND);
            }

            if(tournamentsRepository.findById(request.getTournamentId())==null){
                return new ResponseEntity<Object>("There is no tournament with this ID. Create a new tournament before adding admin accounts", HttpStatus.NOT_FOUND);
            }

            admin.setActive(request.isActive());
            admin.setCreatorId(creatorAccount.getId());
            admin.setPassword(request.getPassword());
            admin.setTournamentId(request.getTournamentId());

            adminRepository.save(admin);
            return new ResponseEntity(HttpStatus.OK);

        }catch (Exception e){
            return new ResponseEntity<Object>(e, HttpStatus.INTERNAL_SERVER_ERROR);      //Catch exceptions and return failure response with exception
        }
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<?> getAdminAccountUserNames(@RequestParam int tournamentId, @RequestBody AuthRequest request){

        try{

            if(!(new AuthHelper().checkCredentials(request.getUsername(), request.getPassword(), true, adminRepository))){
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }

            List<Admins> adminEntities = new ArrayList<>();

            if(tournamentId == 0){
                adminEntities = adminRepository.findAll();
            }else{
                adminEntities = adminRepository.findAllByTournamentId(tournamentId);
            }

            List<AdminAccountResponse> adminResponsesList = new ArrayList<>();

            for(int i = 0; i<adminEntities.size(); i++){
                AdminAccountResponse adminResponse = new AdminAccountResponse();
                Admins adminEntity = adminEntities.get(i);

                adminResponse.setId(adminEntity.getId());
                adminResponse.setActive(adminEntity.isActive());
                adminResponse.setTournamentId(adminEntity.getTournamentId());
                adminResponse.setUsername(adminEntity.getUsername());

                adminResponsesList.add(adminResponse);
            }

            return new ResponseEntity<Object>(adminResponsesList, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<Object>(e, HttpStatus.INTERNAL_SERVER_ERROR);      //Catch exceptions and return failure response with exception
        }

    }
}
