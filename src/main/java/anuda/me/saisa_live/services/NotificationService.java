package anuda.me.saisa_live.services;

import anuda.me.saisa_live.entities.Followers;
import anuda.me.saisa_live.repositories.FollowersRepository;
import anuda.me.saisa_live.requests.notifications.CreateNotificationRequest;
import anuda.me.saisa_live.requests.notifications.NotificationContents;
import anuda.me.saisa_live.requests.notifications.NotificationHeadings;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.body.RequestBodyEntity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NotificationService {

    final static String app_id = "604461e0-965b-4e8a-b0fe-d1bf13f39dd9";
    final static String api_key = "Basic M2M3MWJmNTAtMmM1Yi00MTdmLTlkOTYtOGVlMmY2YjIxYzE1";

    public Boolean sendNotification(String message, String heading, List<String> recipients) {

        try {
            ObjectMapper mapper = new ObjectMapper();

            CreateNotificationRequest request = new CreateNotificationRequest();

            NotificationContents contents = new NotificationContents();
            contents.setEn(message);

            NotificationHeadings headings = new NotificationHeadings();
            headings.setEn(heading);

            request.setApp_id(app_id);
            request.setContents(contents);
            request.setHeadings(headings);
            request.setInclude_external_user_ids(recipients);

            String body = mapper.writeValueAsString(request);
            Map<String, String> maps = new HashMap<>();
            maps.put("Content-Type", "application/json");
            maps.put("Authorization", api_key);

            RequestBodyEntity jsonResponse = Unirest.post("https://onesignal.com/api/v1/notifications")
                    .headers(maps)
                    .body(body);

            HttpResponse<String> jsonResponse2 = jsonResponse.asString();

            return jsonResponse2.getStatus() == 200;


        } catch (Exception e) {
            return false;
        }
    }

}
