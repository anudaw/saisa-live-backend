-- MySQL dump 10.13  Distrib 5.7.21, for osx10.9 (x86_64)
--
-- Host: localhost    Database: saisa_live
-- ------------------------------------------------------
-- Server version	5.7.31-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `tournament_id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES (1,'aweerasinghe20@osc.lk','7HDhnssem8eUZi#%pLzPDKuCOrhFKm',0,1,1,'2020-02-08 15:05:59'),(2,'stturner@osc.lk','D0unu*zBvL*$y^FmhEc2GemO$t2^Pl',5,1,0,'2020-02-18 13:27:26'),(3,'boysBasketballJordan2020','VlLS1wDDB2#PU4yUz8RBAS8mb5MUvY',4,1,0,'2020-02-18 18:34:36'),(4,'trackChennai2020','aeZP6&Al9zX9axReJ^cC4XHSI$6TTa',6,1,0,'2020-02-18 18:40:21');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `followers`
--

DROP TABLE IF EXISTS `followers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `followers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `followers`
--

LOCK TABLES `followers` WRITE;
/*!40000 ALTER TABLE `followers` DISABLE KEYS */;
/*!40000 ALTER TABLE `followers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `games`
--

DROP TABLE IF EXISTS `games`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `games` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team1` int(11) NOT NULL,
  `team2` int(11) NOT NULL,
  `team1_score` varchar(100) DEFAULT '0',
  `team2_score` varchar(100) DEFAULT '0',
  `result` int(11) DEFAULT '0',
  `active_status` int(11) NOT NULL,
  `livestream_id` int(11) DEFAULT '0',
  `tournament_id` int(11) NOT NULL,
  `location` varchar(100) NOT NULL,
  `game_description` varchar(100) NOT NULL,
  `start_time` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `games`
--

LOCK TABLES `games` WRITE;
/*!40000 ALTER TABLE `games` DISABLE KEYS */;
INSERT INTO `games` VALUES (1,7,8,'6, 15','25, 25',2,2,2,1,'Court A, The Overseas School of Colombo','Round Robin','08:30AM, Thursday 25th of October 2018'),(2,6,5,'25, 25','19, 17',1,2,3,1,'Court B, The Overseas School of Colombo','Round Robin','08:30AM, Thursday 25th of October 2018'),(3,2,4,'19, 25,  8','25, 10,  15',2,2,2,1,'Court A, The Overseas School of Colombo','Round Robin','09:40AM, Thursday 25th of October 2018'),(4,3,1,'25, 25','22, 17',1,2,3,1,'Court B, The Overseas School of Colombo','Round Robin','09:40AM, Thursday 25th of October 2018'),(5,6,8,'22, 25','25, 27',2,2,2,1,'Court A, The Overseas School of Colombo','Round Robin','10:50AM, Thursday 25th of October 2018'),(6,5,9,'25, 25','11, 11',1,2,3,1,'Court B, The Overseas School of Colombo','Round Robin','10:50AM, Thursday 25th of October 2018'),(7,1,2,'25, 25','17, 16',1,2,2,1,'Court A, The Overseas School of Colombo','Round Robin','12:00PM, Thursday 25th of October 2018'),(8,4,7,'25, 25','12, 17',1,2,3,1,'Court B, The Overseas School of Colombo','Round Robin','12:00PM, Thursday 25th of October 2018'),(9,3,8,'25, 25','10, 16',1,2,2,1,'Court A, The Overseas School of Colombo','Round Robin','01:10PM, Thursday 25th of October 2018'),(10,9,6,'19, 16','25, 25',2,2,3,1,'Court B, The Overseas School of Colombo','Round Robin','01:10PM, Thursday 25th of October 2018'),(11,7,5,'12, 23','25, 25',2,2,2,1,'Court A, The Overseas School of Colombo','Round Robin','02:20PM, Thursday 25th of October 2018'),(12,1,4,'25, 22,  17','22, 25,  19',2,2,3,1,'Court B, The Overseas School of Colombo','Round Robin','02:20PM, Thursday 25th of October 2018'),(13,9,3,'10, 24','25, 26',2,2,2,1,'Court A, The Overseas School of Colombo','Round Robin','03:30PM, Thursday 25th of October 2018'),(14,8,2,'20, 20','25, 25',2,2,3,1,'Court B, The Overseas School of Colombo','Round Robin','03:30PM, Thursday 25th of October 2018'),(15,6,7,'25, 25','11, 17',1,2,2,1,'Court A, The Overseas School of Colombo','Round Robin','04:40PM, Thursday 25th of October 2018'),(16,1,5,'19, 25,  15','25, 23,  11',1,2,3,1,'Court B, The Overseas School of Colombo','Round Robin','04:40PM, Thursday 25th of October 2018'),(17,4,3,'25, 11,  15','19, 25,  9',1,2,2,1,'Court A, The Overseas School of Colombo','Round Robin','05:50PM, Thursday 25th of October 2018'),(18,2,9,'25, 25','12, 21',1,2,3,1,'Court B, The Overseas School of Colombo','Round Robin','05:50PM, Thursday 25th of October 2018'),(19,2,5,'25, 23,  13','23, 25,  15',2,2,4,1,'Court A, The Overseas School of Colombo','Round Robin','08:00AM, Friday 26th of October 2018'),(20,7,9,'25, 25','9, 12',1,2,5,1,'Court B, The Overseas School of Colombo','Round Robin','08:00AM, Friday 26th of October 2018'),(21,6,4,'25, 25','21, 14',1,2,4,1,'Court A, The Overseas School of Colombo','Round Robin','09:10AM, Friday 26th of October 2018'),(22,8,1,'25, 25,','18, 21',1,2,5,1,'Court B, The Overseas School of Colombo','Round Robin','09:10AM, Friday 26th of October 2018'),(23,5,3,'26, 21,  15','24, 25,  11',1,2,4,1,'Court A, The Overseas School of Colombo','Round Robin','10:20AM, Friday 26th of October 2018'),(24,7,2,'21, 25,  15','25, 22,  10',1,2,5,1,'Court B, The Overseas School of Colombo','Round Robin','10:20AM, Friday 26th of October 2018'),(25,9,1,'6, 16','25, 25,  0',2,2,4,1,'Court A, The Overseas School of Colombo','Round Robin','11:30AM, Friday 26th of October 2018'),(26,4,8,'23, 31,  15','25, 29,  7',1,2,5,1,'Court B, The Overseas School of Colombo','Round Robin','11:30AM, Friday 26th of October 2018'),(27,7,3,'23, 26,  13','25, 24,  15',2,2,4,1,'Court A, The Overseas School of Colombo','Round Robin','12:40PM, Friday 26th of October 2018'),(28,2,6,'25, 25','20, 19',1,2,5,1,'Court B, The Overseas School of Colombo','Round Robin','12:40PM, Friday 26th of October 2018'),(29,4,9,'25, 26','13, 24',1,2,4,1,'Court A, The Overseas School of Colombo','Round Robin','01:50PM, Friday 26th of October 2018'),(30,8,5,'13, 16','25, 25',2,2,5,1,'Court B, The Overseas School of Colombo','Round Robin','01:50PM, Friday 26th of October 2018'),(31,1,6,'25, 25','23, 17',1,2,4,1,'Court A, The Overseas School of Colombo','Round Robin','03:00PM, Friday 26th of October 2018'),(32,2,3,'27, 25','25, 21',1,2,5,1,'Court B, The Overseas School of Colombo','Round Robin','03:00PM, Friday 26th of October 2018'),(33,4,5,'26, 25','24, 17',1,2,4,1,'Court A, The Overseas School of Colombo','Round Robin','04:10PM, Friday 26th of October 2018'),(34,9,8,'14, 17','25, 25',2,2,5,1,'Court B, The Overseas School of Colombo','Round Robin','04:10PM, Friday 26th of October 2018'),(35,7,1,'18, 24','25, 26',2,2,4,1,'Court A, The Overseas School of Colombo','Round Robin','05:20PM, Friday 26th of October 2018'),(36,6,3,'23, 22','25, 25',2,2,5,1,'Court B, The Overseas School of Colombo','Round Robin','05:20PM, Friday 26th of October 2018'),(37,7,9,'25, 25','6, 15',1,2,6,1,'Court A, The Overseas School of Colombo','Playoffs','08:00AM, Saturday 27th of October 2018'),(38,3,6,'25, 25','20, 11',1,2,7,1,'Court B, The Overseas School of Colombo','Playoffs','08:00AM, Saturday 27th of October 2018'),(39,5,8,'25, 25','18, 18',1,2,6,1,'Court A, The Overseas School of Colombo','Playoffs','09:10AM, Saturday 27th of October 2018'),(40,1,2,'25, 19,  16','19, 25,  18',2,2,7,1,'Court B, The Overseas School of Colombo','Playoffs','09:10AM, Saturday 27th of October 2018'),(41,4,7,'25, 25','23, 18',1,2,6,1,'Court A, The Overseas School of Colombo','Playoffs','10:20AM, Saturday 27th of October 2018'),(42,3,5,'25, 21,  15','19, 25,  5',1,2,6,1,'Court A, The Overseas School of Colombo','Semi Final 1','11:30AM, Saturday 27th of October 2018'),(43,6,8,'25, 25','15, 15',1,2,7,1,'Court B, The Overseas School of Colombo','Playoffs','11:30AM, Saturday 27th of October 2018'),(44,1,7,'23, 15','25, 25',2,2,6,1,'Court A, The Overseas School of Colombo','Playoffs','12:40PM, Saturday 27th of October 2018'),(45,2,4,'21, 12','25, 25',2,2,7,1,'Court B, The Overseas School of Colombo','Semi Final 2','12:40PM, Saturday 27th of October 2018'),(46,8,1,'17, 25,  15','25, 18,  6',1,2,6,1,'Court A, The Overseas School of Colombo','7the Place Game','01:50PM, Saturday 27th of October 2018'),(47,6,7,'25, 23,  15','17, 25,  13',1,2,7,1,'Court B, The Overseas School of Colombo','5th Place Game','01:50PM, Saturday 27th of October 2018'),(48,5,2,'21, 18,  21','25, 25,  25',2,2,6,1,'Court A, The Overseas School of Colombo','3rd Place Game','03:00PM, Saturday 27th of October 2018'),(49,3,4,'21, 25, 25, 25','25, 9, 18, 17',1,2,6,1,'Court A, The Overseas School of Colombo','Championship Game','05:00PM, Saturday 27th of October 2018'),(50,28,29,'28','49',2,2,34,4,'American Community School, Amman','Pool A - Round Robin','09:00AM EET, Thursday 20th of February 2020'),(51,30,31,'44','14',1,2,34,4,'American Community School, Amman','Pool A - Round Robin','10:20AM EET, Thursday 20th of February 2020'),(52,32,33,'15','40',2,2,34,4,'American Community School, Amman','Pool B - Round Robin','11:40AM EET, Thursday 20th of February'),(53,34,35,'44','34',1,2,34,4,'American Community School, Amman','Pool B - Round Robin','01:00PM EET, Thursday 20th of February 2020'),(54,28,30,'52','59',2,2,34,4,'American Community School, Amman','Pool A - Round Robin','02:20PM EET, Thursday 20th of February 2020'),(55,29,31,'38','18',1,2,34,4,'American Community School, Amman','Pool A - Round Robin','03:40PM EET, Thursday 20th of February 2020'),(56,32,34,'32','44',2,2,34,4,'American Community School, Amman','Pool B - Round Robin','05:00PM EET, Thursday 20th of February 2020'),(57,33,35,'54','43',1,2,34,4,'American Community School, Amman','Pool B - Round Robin','06:20PM EET, Thursday 20th of February 2020'),(58,28,31,'38','15',1,2,38,4,'American Community School, Amman','Pool A - Round Robin','08:00AM EET, Friday 21st of February 2020'),(59,29,30,'54','28',1,2,38,4,'American Community School, Amman','Pool A - Round Robin','09:20AM EET, Friday 21st of February 2020'),(60,32,35,'43','45',2,2,38,4,'American Community School, Amman','Pool B - Round Robin','10:40AM EET, Friday 21st of February 2020'),(61,33,34,'66','51',1,2,38,4,'American Community School, Amman','Pool B - Round Robin','12:00PM EET, Friday 21st of February 2020'),(62,42,41,'1','0',1,2,17,5,'Field 1, The American International School in Muscat, Oman','Pool B - Round Robin','09:30AM EET, Thursday 20th of February 2020'),(63,40,43,'2','4',2,2,18,5,'Field 2, The American International School in Muscat, Oman','Pool B - Round Robin','09:30AM EET, Thursday 20th of February 2020'),(64,39,36,'0','1',2,2,19,5,'Field 1, The American International School in Muscat, Oman','Pool A - Round Robin','11:30AM GST, Thursday 20th of February 2020'),(65,38,37,'0','1',2,2,20,5,'Field 2, The American International School in Muscat, Oman','Pool A - Round Robin','11:30AM GST, Thursday 20th of February 2020'),(66,40,41,'0','5',2,2,21,5,'Field 1, The American International School in Muscat, Oman','Pool B - Round Robin','01:30PM GST, Thursday 20th of February 2020'),(67,42,43,'1','1',0,2,22,5,'Field 2, The American International School in Muscat, Oman','Pool B - Round Robin','01:30PM GST, Thursday 20th of February 2020'),(68,38,36,'1','0',1,2,23,5,'Field 1, The American International School in Muscat, Oman','Pool A - Round Robin','03:30PM GST, Thursday 20th of February 2020'),(69,39,37,'1','0',1,2,24,5,'Field 2, The American International School in Muscat, Oman','Pool A - Round Robin','03:30PM GST, Thursday 20th of February 2020'),(70,36,37,'0','0',0,2,25,5,'Field 1, The American International School in Muscat, Oman','Pool A - Round Robin','09:30AM GST, Friday 21st of February 2020'),(71,38,39,'0','3',2,2,26,5,'Field 2, The American International School in Muscat, Oman','Pool A - Round Robin','09:30AM GST, Friday 21st of February 2020'),(72,42,40,'3','0',1,2,27,5,'Field 1, The American International School in Muscat, Oman','Pool B - Round Robin','11:30AM GST, Friday 21st of February 2020'),(73,43,41,'1','2',2,2,28,5,'Field 2, The American International School in Muscat, Oman','Pool B - Round Robin','11:30AM GST, Friday 21st of February 2020'),(74,44,45,'','',0,3,29,6,'AISC','1500 Meter Finals','7:00 AM to 8:00 AM IST, Thursday 20th of February 2020'),(75,46,47,'','',0,3,30,6,'AISC','100 Meter Prelims','9:20 AM to 11:20 AM IST, Thursday 20 February 2020'),(76,48,49,'','',0,3,31,6,'AISC Field','Long Jump, High Jump, Shot Put, Discus','12:40 PM - 2:10 PM IST, Thursday 20 February 2020'),(77,45,50,'','',0,3,29,6,'AISC Field','100 Meter Finals','2:20 PM to 3:20 PM IST Thursday 20 Feb 2020'),(78,45,46,'','',0,3,30,6,'AISC','Long Jump, High Jump, Shot Put, Discus','3:30 PM - 4:50 PM IST , Thursday 20 Feb 2020'),(79,45,48,'','',0,3,31,6,'AISC','4 X 400 Meter Finals','5:00 PM to 6:30 PM IST, Thursday, 20 Feb 2020'),(80,45,44,'','',0,3,35,6,'AISC Field','800 Meter Finals','7:30 AM to 8:30 PM IST, 21st Feb 2020'),(81,46,47,'','',0,3,36,6,'AISC Track','200 Meter Prelims','8:40 AM to 10:10 AM IST, 21 Feb 2020'),(82,48,49,'','',0,3,37,6,'AISC Field','Long Jump, High Jump, Shot Put, Discus','10:30 - 11:50 AM IST, 21 Feb 2020'),(83,50,45,'','',0,3,35,6,'AISC Field','Long Jump, High Jump, Shot Put, Discus','1:30 - 3:00 PM IST, 21 Feb 2020'),(84,45,44,'','',0,3,36,6,'AISC Track','200 Meter Finals','3:30 PM to 4:10 PM IST, 21 Feb 2020'),(85,36,43,'0','1',2,2,39,5,'TAISM','Quarter Final 1','01:30PM GST, Friday 21st of February 2020'),(86,41,37,'3','0',1,2,40,5,'taism','Quarter Final 2','01:30PM GST, Thursday 21st of February 2020'),(87,39,40,'3','1',1,2,41,5,'taism','Quarter Final 3','03:30PM GST, Friday 21st of February 2020'),(88,42,38,'2','1',1,2,42,5,'taism','Quarter Final 4','03:30PM GST, Friday 21st of February 2020'),(89,30,35,'64','54',1,2,38,4,'ACS','Game 1 -  Quarter Finals','2:40PM EET, Friday 21st of February 2020'),(90,37,40,'2','0',1,2,43,5,'taism','Loser Game 14 vs Loser Game 15','09:30AM GST, Saturday 22nd of February 2020'),(91,36,38,'2','3',2,2,44,5,'taism','Loser Game 13 vs Loser Game 16','09:30AM GST, Saturday 22nd of February 2020'),(92,41,39,'1','0',1,2,45,5,'taism','Semi Final 1','11:00AM GST, Saturday 22nd of February 2020'),(93,43,42,'0','2',2,2,46,5,'taism','Semi Final 2','11:00AM GST, Saturday 22nd of February 2020'),(94,34,28,'','',0,3,38,4,'acs','Game 2 - Quarter Finals','04:00PM EET, Friday 22nd of February 2020'),(95,29,32,'','',0,3,38,4,'acs','Game 3 - Quarter Finals','05:20PM EET, Friday 22nd of February 2020'),(96,33,31,'43','12',1,2,38,4,'acs','Game 4 - Quarter Finals','06:20PM EET, Friday 22nd of February 2020'),(97,45,44,'','',0,3,47,6,'AISC Track','3000 Mts Final','7:00AM - 8:10 AM IST, Saturday 22nd Feb 2020'),(98,46,47,'','',0,3,47,6,'AISC Track','400 Mts Timed Finals','8:40AM - 10:00 AM IST, 22nd Feb 2020'),(99,48,49,'','',0,3,48,6,'Track','Long Jump, High Jump, Shot Put, Discus','10:30 AM to 11:50 AM IST, 22nd Feb 2020'),(100,50,45,'','',0,3,49,6,'AISC Field','Long Jump, High Jump, Shot Put, Discus','1:30 PM to 3:00 PM IST, 22nd Feb 2020'),(101,44,45,'','',0,3,47,6,'AISC Track','4x100 Mts Relay','4:00 PM to 5:00 PM IST, 22nd Feb 2020'),(102,34,28,'46','31',1,2,38,4,'ACS','Game 2 -  Quarter Finals','04:00PM EET, Friday 22nd of February 2020'),(103,29,32,'39','19',1,2,38,4,'ACS','Game 3 - Quarter Finals','05:20PM EET, Friday 22nd of February 2020'),(104,35,32,'','',0,3,50,4,'acs','Loser QF1 vs Loser QF 3','08:00AM EET, Saturday 22nd of February 2020'),(105,28,32,'40','30',1,2,50,4,'ACS','Loser QF2 vs Loser QF3','09:20AM EET, Saturday 22nd of February 2020'),(106,29,34,'57','44',1,2,50,4,'acs','Semi Final 1','10:40AM EET, Saturday 22nd of February 2020'),(107,30,33,'51','42',1,2,50,4,'acs','Semi Final 2','12:00PM EET, Saturday 22nd of February 2020'),(108,35,31,'35','28',1,2,50,4,'Court 1','Loser QF1 vs Loser QF 4','08:00AM EET, Saturday 22nd of February 2020'),(109,40,36,'FORFEIT','',2,2,51,5,'taism','7th Place Playoff Match','12:30PM GST, Saturday 22nd of February 2020'),(110,37,38,'0','1',2,2,52,5,'taism','5th Place Playoff Match','12:30PM GST, Saturday 22nd of February 2020'),(111,31,32,'8','30',2,2,50,4,'acs','7th Place Playoff Match','01:20PM EET, Saturday 22nd of February 2020'),(112,35,28,'21','35',2,2,50,4,'acs','5th Place Playoff Match','02:40PM EET, Thursday 22nd of February 2020'),(113,39,43,'3 (1/4)','3 (3/5)',2,2,53,5,'taism','3rd Place Playoff Match','02:00PM GST, Saturday 22nd of February 2020'),(114,41,42,'2','3',2,2,54,5,'taism','Championship Match','03:30PM GST, Saturday 22nd of February 2020'),(115,34,33,'46','44',1,2,50,4,'acs','3rd Place Playoff Match','04:00PM EET, Saturday 22nd of February 2020'),(116,29,30,'60','41',1,2,50,4,'acs','Championship Match','05:20PM EET, Saturday 22nd of February 2020');
/*!40000 ALTER TABLE `games` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `livestreams`
--

DROP TABLE IF EXISTS `livestreams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `livestreams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tournament_id` int(11) NOT NULL,
  `url` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `is_live` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `livestreams`
--

LOCK TABLES `livestreams` WRITE;
/*!40000 ALTER TABLE `livestreams` DISABLE KEYS */;
INSERT INTO `livestreams` VALUES (1,1,'https://youtu.be/sUwu2P2FLFY','Opening Ceremony',1,0),(2,1,'https://youtu.be/aSujN1fKouk','Day 1 - Court A',1,0),(3,1,'https://youtu.be/Es-wbYo-q2k','Day 1 - Court B',1,0),(4,1,'https://youtu.be/p1F2hp39y_Y','Day 2 - Court A',1,0),(5,1,'https://youtu.be/l89RfVieB8g','Day 2 - Court B',1,0),(6,1,'https://youtu.be/stqd2A3sw_A','Day 3 - Court A',1,0),(7,1,'https://youtu.be/1viuMBDm33A','Day 3 - Court B',1,0),(8,1,'https://youtu.be/-M54CN9hlXE','Closing Ceremony',1,0),(17,5,'https://youtu.be/_UMIajYPqEM','Game 1 - TAISM vs OSC',1,0),(18,5,'https://youtu.be/0e0NicAVP7A','Game 2 - KAS vs ACS',0,0),(19,5,'https://youtu.be/sB_H3wEgI4Y','Game 3 - AISD vs LS',1,0),(20,5,'https://youtu.be/UhZu4_9TGbE','Game 4 - AISC vs ASB',1,0),(21,5,'https://youtu.be/ForVCTvvItE','Game 5 - KAS vs OSC',1,0),(22,5,'https://youtu.be/FNc7Rz5cpDw','Game 6 - TAISM vs ACS',1,0),(23,5,'https://youtu.be/Z6ywtGiC3KU','Game 7 - AISC vs LS',1,0),(24,5,'https://youtu.be/9daPISqcYq0','Game 8 - AISD vs ASB',1,0),(25,5,'https://youtu.be/Ip9tuep6qAM','Game 9 - LS vs ASB',1,0),(26,5,'https://youtu.be/0dGH4IjPy6k','Game 10 - AISC vs AISD',1,0),(27,5,'https://youtu.be/HVqFtilMJ8U','Game 11 - TAISM vs KAS',1,0),(28,5,'https://youtu.be/PyZJvOhePUM','Game 12 - ACS vs OSC',1,0),(29,6,'https://youtu.be/iPHuZ_2FdRw','Camera 1 - Day 1',1,0),(30,6,'https://youtu.be/3H3IZRa1uZY','Camera 2 - Day 1',1,0),(31,6,'https://youtu.be/B-ErPVEVTHI','Camera 3 - Day 1',1,0),(32,6,'','',0,0),(33,6,'','',0,0),(34,4,'https://youtu.be/D5giwc4SNNk','Day 1',1,0),(35,6,'https://youtu.be/3Jj5nLuuTpc','Camera 1 - Day  2',1,0),(36,6,'https://youtu.be/EfKJT9aRwqo','Camera 2 - Day 2',1,0),(37,6,'https://youtu.be/T7cHLcMlxnY','Camera 3 - Day 2',1,0),(38,4,'https://youtu.be/6kgSscM9w48','Day 2',1,0),(39,5,'https://youtu.be/of48RF_yUtE','Game 13 - LS vs ACS',1,0),(40,5,'https://youtu.be/JsnUjQ6dy2Q','Game 14 - OSC vs ASB',1,0),(41,5,'https://youtu.be/_nkjHI8HlVs','Game 15 - AISD vs KAS',1,0),(42,5,'https://youtu.be/4GhoMVlUmmQ','Game 16 - TAISM vs AISC',1,0),(43,5,'https://youtu.be/O3kLF6-3Z0w','Game 17 - ASB vs KAS',1,0),(44,5,'https://youtu.be/rWDEgBGbG9M','Game 18 - LS vs AISC',1,0),(45,5,'https://youtu.be/lJViE7r2QJw','Semi Final 1 - OSC vs AISD',1,0),(46,5,'https://youtu.be/sWlJbbTQwMI','Semi Final 2 - ACS vs TAISM',1,0),(47,6,'https://youtu.be/qPDG8gzHcPo','Camera 1- Day 3',1,0),(48,6,'https://youtu.be/jXsjPLSKADQ','Camera 2 - Day 3',1,0),(49,6,'https://youtu.be/vJ5B2XR9Nl4','Camera 3 - Day 3',1,0),(50,4,'https://youtu.be/SmzOSUM6-zo','Day 3',1,0),(51,5,'https://youtu.be/w2jklRlbeIM','7th Place Playoff - KAS vs LS',0,0),(52,5,'https://youtu.be/4CFg1jDv_fY','5th Place Playoff - ASB vs AISC',1,0),(53,5,'https://youtu.be/-q6Pfdg16lI','3rd Place Playoff Match - AISD vs ACS',1,0),(54,5,'https://youtu.be/NnoOyvjNEds','Championship Match - OSC vs TAISM',1,0),(55,4,'https://youtu.be/j66iuPNB4Jg','Closing Ceremony',1,0);
/*!40000 ALTER TABLE `livestreams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tournament_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `cover_img` varchar(200) NOT NULL,
  `content_url` varchar(200) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL,
  `timestamp` varchar(200) NOT NULL,
  `text` varchar(10000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media`
--

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
INSERT INTO `media` VALUES (1,6,2,'SAISA Track and Field 2020 - Opening Ceremony','https://www.aischennai.org/wp-content/uploads/2020/02/SAISA-Track-Field-Banner-01.jpg','https://www.youtube.com/watch?v=oyVJFvs-9Ko',1,'1582177880',NULL),(2,1,1,'SAISA Girls Volleyball 2018','https://res.cloudinary.com/dr4uncu8z/image/upload/v1542877113/SAISA%20Girls%20Volleyball%20Website/49-min.jpg','https://drive.google.com/drive/folders/1pVTX2Uon1vNrS6m1FmuQuZSMsFuEfbZh?usp=sharing',0,'1582186475',NULL),(3,4,1,'Opening Ceremony - SAISA Boys Basketball 2020','https://resources.finalsite.net/images/f_auto,q_auto,t_image_size_4/v1582265859/acsammanedujo/fw5j6gpnneuux326y8jz/SAISABoysBB32.jpg','https://www.acsamman.edu.jo/student-life/athletics/welcome-to-saisa/saisa-boys-basketball-2020#photos',1,'1582282443',NULL),(4,5,1,'Day 1 - SAISA Girls Football 2020','http://142.93.212.170/covers/unnamed.jpg','https://saisa.taism.com/2019-20-saisa-girls-soccer/photos-girls-soccer/photo-girls-soccer-day-1',1,'1582293752',NULL),(5,5,1,'Day 2 - SAISA Girls Football 2020','http://142.93.212.170/covers/unnamed%20(1).jpg','https://saisa.taism.com/2019-20-saisa-girls-soccer/photos-girls-soccer/photo-girls-soccer-day-2',1,'1582293805',NULL),(6,5,3,'AISD, OSC, ACS and TAISM Qualify for Girls Football Semi Finals','http://142.93.212.170/covers/unnamed%20(1).jpg',NULL,1,'1582294600','After some thrilling games in the pool stages, and the quarter finals on Thursday and Friday, The American International School in Dhaka, The Overseas School of Colombo, The American Community School of Amman and The American International School of Muscat have qualified for the Semi Finals of SAISA Girls Football 2020 set to take place on Saturday. The fixtures for these semi finals are as follows:\n\n\nSemi Final 1 - 11:00AM GST, Saturday 22nd of February 2020 - OSC vs AISD\n\nSemi Final 2 - 11:00AM GST, Saturday 22nd of February 2020 - ACS vs TAISM\n\n3rd Place Game - 02:00PM GST, Saturday 22nd of February 2020 - Loser of Semi Final 1 vs Loser of Semi Final 2\n\nChampionship Game - 03:30PM GST, Saturday 22nd of February 2020 - Winner of Semi Final 1 vs Winer of Semi Final 2\n\n\nKeep up with live scores, live streams and media from SAISA Girls Football 2020 hosted in Muscat, Oman via the SAISA Live App!\n\n*GST = +04:00 GMT'),(7,4,3,'AISD, AISC, ACS and TAISM Qualify for Boys Basketball Semi Finals','https://resources.finalsite.net/images/f_auto,q_auto,t_image_size_4/v1582265845/acsammanedujo/lalo8i73wkcpgdtga4ha/SAISABoysBB21.jpg',NULL,1,'1582306921','After some thrilling games in the pool stages, and the quarter finals on Thursday and Friday, The American International School in Dhaka, The American International School in Chennai, The American Community School of Amman and The American International School of Muscat have qualified for the Semi Finals of SAISA Boys Basketball 2020 set to take place on Saturday. The fixtures for these semi finals are as follows:\n\n\nSemi Final 1 - 10:40AM EET, Saturday 22nd of February 2020 - AISD vs ACS\n\nSemi Final 2 - 12:00PM EET, Saturday 22nd of February 2020 - TAISM vs AISC\n\n3rd Place Game - 04:00PM EET, Saturday 22nd of February 2020 - Loser of Semi Final 1 vs Loser of Semi Final 2\n\nChampionship Game - 05:20PM EET, Saturday 22nd of February 2020 - Winner of Semi Final 1 vs Winer of Semi Final 2\n\n\nKeep up with live scores, live streams and media from SAISA Boys Basketball 2020 hosted in Amman, Jordan via the SAISA Live App!\n\n*EET = +02:00 GMT'),(8,4,1,'SAISA Boys Basketball 2020 Photos Drive','https://drive.google.com/uc?id=1jum3T00ehTjpf8ECqyLk6g4HdcD8V5Kh','https://drive.google.com/drive/folders/1uWiURo7ELab2OaQ_G9R3C3xCIbN5Pd2I?usp=sharing',1,'1582356428',NULL),(9,5,1,'Day 3 - SAISA Girls Football 2020','http://142.93.212.170/covers/unnamed%20(2).jpg','https://saisa.taism.com/2019-20-saisa-girls-soccer/photos-girls-soccer/photo-girls-soccer-day-3',1,'1582369434',NULL),(10,5,3,'Home team TAISM comes out on top at SAISA Girls Football 2020','http://142.93.212.170/covers/cover.png',NULL,1,'1582377517','Despite taking a 2-0 lead early on in the first half, the home team came back strong in the end to defeat OSC 3-2 and take the SAISA Girls Football championship for 2020. Here are the final standings from the tournament.\n\n1. TAISM\n2. OSC\n3. ACS\n4. AISD\n5. AISC\n6. ASB\n7. LS\n8. KAS\n\nThank you for using the SAISA Live App to stay updated with SAISA Girls Football 2020.');
/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participants`
--

DROP TABLE IF EXISTS `participants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id` int(11) NOT NULL,
  `tournament_id` int(11) NOT NULL,
  `standing` int(11) NOT NULL,
  `wins` int(11) NOT NULL DEFAULT '0',
  `losses` int(11) NOT NULL DEFAULT '0',
  `ties` int(11) NOT NULL DEFAULT '0',
  `games` int(11) NOT NULL DEFAULT '0',
  `points` double NOT NULL DEFAULT '0',
  `pool` int(11) NOT NULL,
  `team_photo` varchar(300) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participants`
--

LOCK TABLES `participants` WRITE;
/*!40000 ALTER TABLE `participants` DISABLE KEYS */;
INSERT INTO `participants` VALUES (1,1,1,4,5,3,0,8,5,1,'http://osc.lk/saisa/assets/teams/OSC.jpg',1),(2,2,1,5,4,4,0,8,4,1,'http://osc.lk/saisa/assets/teams/AISD.jpg',1),(3,3,1,2,5,3,0,8,5,1,'http://osc.lk/saisa/assets/teams/ASB.jpg',1),(4,8,1,1,7,1,0,8,7,1,'http://osc.lk/saisa/assets/teams/ACS.jpg',1),(5,10,1,3,5,3,0,8,5,1,'http://osc.lk/saisa/assets/teams/KAS.jpg',1),(6,7,1,7,4,4,0,8,4,1,'http://osc.lk/saisa/assets/teams/LS.jpg',1),(7,11,1,8,2,6,0,8,2,1,'http://osc.lk/saisa/assets/teams/TAISM.jpg',1),(8,4,1,6,4,4,0,8,4,1,'http://osc.lk/saisa/assets/teams/AISC.jpg',1),(9,5,1,9,0,8,0,8,0,1,'http://osc.lk/saisa/assets/teams/ISOI.jpg',1),(10,3,2,3,7,2,0,9,7,1,'na',1),(11,4,2,7,3,6,0,9,3,1,'na',1),(12,11,2,2,8,1,0,9,8,1,'na',1),(13,2,2,1,8,1,0,9,8,1,'na',1),(14,10,2,5,5,4,0,9,5,1,'na',1),(15,7,2,9,1,8,0,9,1,1,'na',1),(16,1,2,4,6,3,0,9,6,1,'na',1),(17,8,2,8,2,7,0,9,2,1,'na',1),(18,5,2,6,4,5,0,9,4,1,'na',1),(19,6,2,10,1,8,0,9,1,1,'na',1),(20,11,3,1,0,0,0,0,1992,1,'taism',1),(21,8,3,2,0,0,0,0,1857,1,'acs',1),(22,1,3,3,0,0,0,0,1291,1,'na',1),(23,7,3,4,0,0,0,0,1265,1,'na',1),(24,3,3,5,0,0,0,0,916,1,'asb',1),(25,10,3,6,0,0,0,0,753,1,'na',1),(26,4,3,7,0,0,0,0,656,1,'na',1),(27,2,3,8,0,0,0,0,439,1,'na',1),(28,3,4,3,1,2,0,3,1,1,'https://drive.google.com/file/d/15nJY7chOyX1nIAD7mubs4UNejtiX6QHG/view',1),(29,2,4,1,3,0,0,3,3,1,'https://drive.google.com/file/d/15nJY7chOyX1nIAD7mubs4UNejtiX6QHG/view',1),(30,11,4,2,2,1,0,3,2,1,'https://drive.google.com/file/d/15nJY7chOyX1nIAD7mubs4UNejtiX6QHG/view',1),(31,9,4,4,0,3,0,3,0,1,'https://drive.google.com/file/d/15nJY7chOyX1nIAD7mubs4UNejtiX6QHG/view',1),(32,7,4,4,0,3,0,3,0,2,'https://drive.google.com/file/d/15nJY7chOyX1nIAD7mubs4UNejtiX6QHG/view',1),(33,4,4,1,3,0,0,3,3,2,'https://drive.google.com/file/d/15nJY7chOyX1nIAD7mubs4UNejtiX6QHG/view',1),(34,8,4,2,2,1,0,3,2,2,'https://drive.google.com/file/d/15nJY7chOyX1nIAD7mubs4UNejtiX6QHG/view',1),(35,1,4,3,1,2,0,3,1,2,'https://drive.google.com/file/d/15nJY7chOyX1nIAD7mubs4UNejtiX6QHG/view',1),(36,7,5,2,1,1,1,3,4,1,'na',1),(37,3,5,3,1,1,1,3,4,1,'na',1),(38,4,5,4,1,2,0,3,3,1,'na',1),(39,2,5,1,2,1,0,3,6,1,'na',1),(40,10,5,4,0,3,0,3,0,2,'na',1),(41,1,5,2,2,1,0,3,6,2,'na',1),(42,11,5,1,2,0,1,3,7,2,'na',1),(43,8,5,3,1,1,1,3,4,2,'na',1),(44,8,6,5,0,0,0,0,262.5,1,'na',1),(45,4,6,4,0,0,0,0,288.83,1,'na',1),(46,2,6,3,0,0,0,0,396.33,1,'na',1),(47,3,6,6,0,0,0,0,262,1,'na',1),(48,7,6,7,0,0,0,0,62,1,'na',1),(49,1,6,2,0,0,0,0,595.83,1,'na',1),(50,11,6,1,0,0,0,0,596.5,1,'na',1);
/*!40000 ALTER TABLE `participants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sports`
--

DROP TABLE IF EXISTS `sports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `is_indoor` tinyint(4) NOT NULL,
  `icon` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sports`
--

LOCK TABLES `sports` WRITE;
/*!40000 ALTER TABLE `sports` DISABLE KEYS */;
INSERT INTO `sports` VALUES (1,'Boys Volleyball',1,'http://127.0.0.1/sports/volleyball'),(2,'Girls Volleyball',1,'http://127.0.0.1/sports/volleyball'),(3,'Swimming',0,'http://127.0.0.1/sports/volleyball'),(4,'Boys Basketball',0,'http://127.0.0.1/sports/volleyball'),(5,'Girls Football',0,'http://127.0.0.1/sports/volleyball'),(6,'Track and Field',0,'http://127.0.0.1/sports/volleyball'),(7,'Girls Basketball',1,'http://127.0.0.1/sports/volleyball'),(8,'Boys Football',0,'http://127.0.0.1/sports/volleyball'),(9,'Badminton',1,'http://127.0.0.1/sports/volleyball');
/*!40000 ALTER TABLE `sports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `full_name` varchar(150) NOT NULL,
  `mascot` varchar(45) NOT NULL,
  `web_url` varchar(300) NOT NULL,
  `logo` varchar(300) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams`
--

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
INSERT INTO `teams` VALUES (1,'OSC','The Overseas School of Colombo','Geckos','https://osc.lk','http://142.93.212.170/logos/osc.png',1),(2,'AISD','The American International School of Dhaka','Tigers','https://aisdhaka.org','http://142.93.212.170/logos/aisd.png',1),(3,'ASB','The American School of Bombay','Eagles','https://ASB.org','http://142.93.212.170/logos/asb.png',1),(4,'AISC','The American International School of Chennai','Raptors','https://AISC.org','http://142.93.212.170/logos/aisc.png',1),(5,'ISOI','The International School of Islamabad','Cobras','https://ISOI.org','http://142.93.212.170/logos/isoi.png',1),(6,'LAS','The Lahore American School','Bulls','https://LAS.org','http://142.93.212.170/logos/las.png',1),(7,'LS','The Lincoln School, Kathmandu','Snow Leopards','https://LS.org','http://142.93.212.170/logos/ls.png',1),(8,'ACS','The American Community School, Jordan','Scorpions','https://ACS.org','http://142.93.212.170/logos/acs.png',1),(9,'SAISA','SAISA United Team','Saisa','https://saisaleague.com/','http://142.93.212.170/logos/saisa.png',1),(10,'KAS','The Karachi American School, Pakistan','Knights','http://www.kas.edu.pk/index.html','http://142.93.212.170/logos/kas.png',1),(11,'TAISM','The American International School in Muscat, Oman','Eagles','https://www.taism.com/','http://142.93.212.170/logos/taism.png',1);
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tournaments`
--

DROP TABLE IF EXISTS `tournaments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tournaments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `sport_id` int(11) NOT NULL,
  `url` varchar(350) NOT NULL,
  `logo` varchar(200) NOT NULL,
  `location` varchar(200) NOT NULL,
  `start_date` varchar(45) NOT NULL,
  `end_date` varchar(45) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `standings_active` tinyint(4) NOT NULL,
  `pools_active` tinyint(4) NOT NULL,
  `scores_active` tinyint(4) NOT NULL,
  `pool_quantity` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tournaments`
--

LOCK TABLES `tournaments` WRITE;
/*!40000 ALTER TABLE `tournaments` DISABLE KEYS */;
INSERT INTO `tournaments` VALUES (1,'SAISA Girls Volleyball 2018',2,'http://osc.lk/saisa/#!/home','http://osc.lk/saisa/assets/images/logo-img.png','Colombo, Sri Lanka','25th of October 2018','27th of October 2018',0,1,1,1,1),(2,'SAISA Boys Volleyball 2019',1,'https://saisa.taism.com/2019-20-saisa-boys-volleyball','https://drive.google.com/uc?id=1ncxPpoQuAV7MUDR9-s0Gkq9t0AtE0f6Q','Muscat, Oman','17th of October 2019','19th of October 2019',0,1,1,1,1),(3,'SAISA Swimming 2019',3,'https://www.lsnepal.com/saisa-swim-meet-2019/','https://drive.google.com/uc?id=1_FCKxWFBwwjffoRAwx_77EvjfN6wOsAF','Kathmandu, Nepal','17th of October 2019','19th of October 2019',0,1,1,0,1),(4,'SAISA Boys Basketball 2020',4,'https://www.acsamman.edu.jo/student-life/athletics/welcome-to-saisa/saisa-boys-basketball-2020','https://resources.finalsite.net/images/f_auto,q_auto/v1579685342/acsammanedujo/caz55azvn5a4g3oj3ck8/SAISABoysBasketballlogo-edit11.jpg','Amman, Jordan','Thursday, 20th of February 2020','Saturday, 22nd of February 2020',0,1,1,1,2),(5,'SAISA Girls Football 2020',5,'https://saisa.taism.com/2019-20-saisa-girls-soccer','https://drive.google.com/thumbnail?id=1bdrUK4eyXXz-CTbsZ9orXVlPBnBkUl-j','Muscat, Oman','Thursday, 20th of February 2020','Saturday, 22nd of February 2020',0,1,1,1,2),(6,'SAISA Track and Field 2020',6,'http://www.aischennai.org/events/','https://drive.google.com/thumbnail?id=1jc8bHAuc653Q-wPAowKYZM5s3NsyhKVI','Chennai, India','Thursday, 20th of February 2020','Saturday, 22nd of February 2020',0,1,1,0,1);
/*!40000 ALTER TABLE `tournaments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device` varchar(100) NOT NULL,
  `first_login_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-11 18:27:37
